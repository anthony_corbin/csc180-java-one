package edu.neumont.acorbin.nubay.services;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;

public class ItemServiceTest extends TestCase {

    private static class ModelSet implements ItemService.Logger {
        private HashMap<Long,ItemDetailModel> items = new HashMap<>();

        //ModelSet(Iterable<ItemDetailModel> models) { for(ItemDetailModel m : models) { items.put(m.getId(),m); } }
        //ModelSet(HashMap<Long,ItemDetailModel> items) { this.items = items; }
        ModelSet(ItemDetailModel ... models) {
            for(ItemDetailModel m : models) {
                items.put(m.getId(),m);
            }
        }

        @Override
        public void CreateEvent(ItemDetailModel model) {
            items.put(model.getId(), model);
        }

        @Override
        public void DeleteEvent(long id) {
            items.remove(id);
        }

        @Override
        public void EditEvent(ItemDetailModel model) {
            items.remove(model.getId());
            items.put(model.getId(),model);
        }

        @Override
        public Iterable<ItemDetailModel> GetAll() {
            return items.values();
        }
    }

    public void testSearch() throws Exception {
        Date startDate = DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0);
        Date endDate = DateParser.getNowPlusDuration(0,30,0,0,0,0,0);
        ItemService service = new ItemService(new ModelSet(
                new ItemDetailModel(  0 ,"Toaster",         "Very Brave",            "The Brave Toaster has killed 50 foes",                                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
                new ItemDetailModel(  1 ,"Potato",          "Very Starchy",          "The Brave Potato has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.lib.umn.edu/huber195/psy1001spring12/imagespotato-face.jpg")),
                new ItemDetailModel(  2 ,"Panda",           "He will Kill you",      "The Scared Panda has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.boostability.com/wp-content/uploads/2014/09/Panda-Update.jpg")),
                new ItemDetailModel(  3 ,"Singer",          "Person, sounds decent", "Mr Greg has a long career of hurting friends and family's ears with his \"music\"", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://4vector.com/i/free-vector-man-with-a-microphone_099808_Man_with_a_microphone.png")),
                new ItemDetailModel(  4 ,"Desktop Mic",     "Kinda works",           "If you smack it a few times, it starts working",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.altoedge.com/microphones/images/pc-microphone-gn3_hi.jpg")),
                new ItemDetailModel(  5 ,"Mic",             "Unlock your singer",    "Please let the singer you have caged up go, take this mic instead",                 new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://fc09.deviantart.net/fs14/f/2007/004/2/3/Microphone_by_gregVent.jpg")),
                new ItemDetailModel(  6 ,"iPhone",          "$.$",                   "Do you like having money? I didn't think so",                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.telegraph.co.uk/multimedia/archive/02424/iphone-4s_2424784k.jpg")),
                new ItemDetailModel(  7 ,"Hose",            "not a gun",             "really it \"shoots\" water, but it isn't a gun",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://www.deere.com/common/media/images/product/home_and_workshop_products/air_tools/0076249_air_tools_942x458.jpg")),
                new ItemDetailModel(  8 ,"Hammer",          "hit things",            "Smack anything or anyone right into place with this shiny hammer",                  new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.popularmechanics.com/cm/popularmechanics/images/se/10-tools-for-kids-02-0513-lgn.jpg")),
                new ItemDetailModel(  9 ,"Toe Tail Clipper","For Men",               "For Toe nails made of steel",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.globalspec.com/ImageRepository/LearnMore/20129/ASTM_PEX_Crimp_Tool_3Ca64f4633735f4d9c93933bdd4bcdea53.png")),
                new ItemDetailModel( 10 ,"Plumber's crack", "for pipes and stuff",   "have a good excuse to not pull up your pants",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.johnnichollstrade.co.uk/assets/Rothenberger-tool-32.jpg")),
                new ItemDetailModel( 11 ,"Speaker",         "Makes noise",           "this one makes a lot of noise",                                                     new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://s3-us-west-1.amazonaws.com/static.brit.co/wp-content/uploads/2012/10/Wireless-Pill.jpg")),
                new ItemDetailModel( 12 ,"Alienware",       "Phone",                 "It doesn't really exist yet",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.blogcdn.com/www.engadget.com/media/2008/02/alienware-concept-phone.jpg")),
                new ItemDetailModel( 13 ,"Samsung Phone",   "Shiny and new",         "this one includes an app that can make calls",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://3.bp.blogspot.com/-jtiTpw22KZE/T8yDeavLDWI/AAAAAAAAAJg/Aoiq1uHEODw/s1600/best+new+samsung+android+smart+phone+with+3G+technology+at+MarketA2Z.com++%25286%2529.jpg")),
                new ItemDetailModel( 14 ,"Android",         "Android",               "Android",                                                                           new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
        ));

        Assert.assertEquals(1,service.search("very and (brave or phone) and not starchy").size());
        Assert.assertEquals(2,service.search("very and (brave or phone)").size());
        Assert.assertEquals(5,service.search("very and brave or phone").size());
        Assert.assertEquals(2,service.search("very and brave").size());
        Assert.assertEquals(0,service.search("very and brave and").size()); // invalid
    }

    public void testBid() throws Exception {
		/*
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0,  0,  0,  0,  0,  1);
		ItemService service = new ItemService(new ModelSet(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		/*/
        ItemService service = new ItemService();

        //*/
        Assert.assertEquals(null,service.getRandomInValidModel());
        ItemDetailModel m = service.getRandomValidModel();
        BigDecimal beforeBid = m.getCurrentBid();
        service.bid(m.getId(),10);
        BigDecimal after = m.getCurrentBid();
        Assert.assertEquals(10,after.subtract(beforeBid).doubleValue(),.001);
    }

    public void testExpiredBit() throws Exception {
        Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
        Date endDate   = DateParser.getNowPlusDuration(0,  0, -1,  0,  0,  0,  0);
        ItemService service = new ItemService(new ModelSet(
                new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
                new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
        ));
        Assert.assertEquals(null,service.getRandomValidModel());
        ItemDetailModel m = service.getRandomInValidModel();
        try {
            service.bid(m.getId(), 10);
            Assert.fail();
        } catch (Exception e) {

        }
    }

    public void testBidInvalid() throws Exception {
        ItemService service = new ItemService();
        Random rand = new Random();
        long invalidId = -1;
        while(service.getItemByID(invalidId) != null) {
            invalidId = rand.nextInt();
        }
        Assert.assertEquals(null,service.bid(invalidId,100));
    }

    public void testGetItemByID() throws Exception {
        ItemService service = new ItemService();
        Collection<ItemDetailModel> all = service.GetAll();
        for(ItemDetailModel m : all) {
            Assert.assertEquals(m,service.getItemByID(m.getId()));
        }
    }

    public void testGetRandomModel() throws Exception {
        ItemService service = new ItemService();
        Collection<ItemDetailModel> all = service.GetAll();
        for (int i = 0; i < 10; i++) {
            ItemDetailModel m = service.getRandomValidModel();
            Assert.assertTrue(all.contains(m));
        }
    }

    public void testCreateModelErr() throws Exception {
        ItemService service = new ItemService();
        ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
        stringy.setStartDate("Hello");
        Assert.assertTrue(stringy.getErrs().size() > 0);
        Assert.assertFalse(stringy.isValid());
        Assert.assertEquals(stringy.getModel(100),null);
        try {
            service.AddNewObject(stringy);
            Assert.fail();
        } catch (ItemService.ItemClientException e) {

        }
    }

    public void testCreateModel() throws Exception {
        ItemService service = new ItemService(new ModelSet());
        ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
        stringy.setStartDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0)));
        stringy.setImageUrl("");
        stringy.setName("Valid");
        stringy.setShortDescription("Good");
        stringy.setLongDescription("Goooooooooooooooooooooooooooooood");
        stringy.setCurrentBid("not good");
        Assert.assertFalse(stringy.isValid());
        Assert.assertTrue(stringy.getErrs().size() > 0);
        stringy.setCurrentBid(MoneyParser.format(100));
        stringy.setEndDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, 100, 0, 0, 0, 0)));
        Assert.assertTrue(stringy.isValid());
        service.AddNewObject(stringy);
    }
}