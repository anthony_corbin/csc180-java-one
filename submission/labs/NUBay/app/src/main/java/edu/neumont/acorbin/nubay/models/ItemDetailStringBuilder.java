package edu.neumont.acorbin.nubay.models;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;

/**
 * Created by Anthony on 2/9/2015.
 */
public class ItemDetailStringBuilder implements Serializable {
    interface GetErrors<T> {
        //returns "" if valid
        String getErrs(T val);
    }
    class NoErrors<T> implements GetErrors<T> { @Override public String getErrs(T val) { return ""; } }
    class NotNullErrors implements GetErrors<String> {
        private String name;
        NotNullErrors(String name) {
            this.name = name;
        }

        @Override public String getErrs(String val) {
            if(val == null || val.trim().equals("")) {
                return name+" Must have a value";
            }
            return "";
        }
    }
    class ValidField<T> {
        private T field;
        private Boolean isValid = null;

        public String getErr() {
            return err;
        }

        private String err = null;
        private GetErrors<T> validator;

        ValidField(GetErrors<T> validator, T defaultFieldValue) {
            this.validator = validator;
            field = defaultFieldValue;
        }

        T get() {return field;}
        void set(T toSet) { field = toSet; isValid = null; }
        boolean isValid() {
            return isValid == null ? (isValid = (err = validator.getErrs(field)).equals("")) : isValid;
        } // lazy init
    }

    private List<ValidField<String>> fields = new ArrayList<>();

    private ValidField<String> name = new ValidField<>(new NotNullErrors("Name"),null);
    private ValidField<String> shortDescription = new ValidField<>(new NotNullErrors("Short Desc"),null);
    private ValidField<String> longDescription = new ValidField<>(new NotNullErrors("Long Desc"),null);
    private ValidField<String> currentBid = new ValidField<>(new GetErrors<String>() {
        @Override
        public String getErrs(String val) {
            try{
                MoneyParser.parse(val);
            } catch (NumberFormatException e) {
                return "Invalid Money Syntax";
            }
            return "";
        }
    },".01");
    private ValidField<String> StartDate = new ValidField<>(new GetErrors<String>() {
        @Override
        public String getErrs(String val) {
            if(DateParser.parse(val) == null)
                return "Invalid Start Date";
            return "";
        }
    },DateParser.getNowStr());
    private ValidField<String> EndDate = new ValidField<>(new GetErrors<String>() {
        @Override
        public String getErrs(String val) {
            Date myResult = DateParser.parse(val);
            if(myResult == null) return "Invalid End Date";
            if(StartDate.isValid() && DateParser.parse(StartDate.get()).getTime() > myResult.getTime()) return "End date is before start date";
            return "";
        }
    },DateParser.format(DateParser.getNowPlusDuration(100,0,0,0,7,0,0)));
    private ValidField<String> imageUrl = new ValidField<>(new NoErrors<String>(),null);
    public ItemDetailStringBuilder() {
        fields.add(name);
        fields.add(shortDescription);
        fields.add(longDescription);
        fields.add(currentBid);
        fields.add(StartDate);
        fields.add(EndDate);
        fields.add(imageUrl);
    }

    public String getImageUrl() { return imageUrl.get(); }
    public void setImageUrl(String imageUrl) { this.imageUrl.set(imageUrl); }

    public String getEndDate() { return EndDate.get(); }
    public void setEndDate(String endDate) { EndDate.set(endDate); }

    public String getStartDate() { return StartDate.get(); }
    public void setStartDate(String startDate) { StartDate.set(startDate); }

    public String getCurrentBid() { return currentBid.get(); }
    public void setCurrentBid(String currentBid) { this.currentBid.set(currentBid); }

    public String getLongDescription() { return longDescription.get(); }
    public void setLongDescription(String longDescription) { this.longDescription.set(longDescription); }

    public String getShortDescription() { return shortDescription.get(); }
    public void setShortDescription(String shortDescription) { this.shortDescription.set(shortDescription); }

    public String getName() { return name.get(); }
    public void setName(String name) { this.name.set(name); }

    public boolean isValid() {
        return name.isValid()
                && shortDescription.isValid()
                && longDescription.isValid()
                && currentBid.isValid()
                && StartDate.isValid()
                && EndDate.isValid()
                && imageUrl.isValid();
    }
    public List<String> getErrs() {
        List<String> ret = new ArrayList<>();
        for(ValidField<String> field : fields) {
            if(!field.isValid()) {
                ret.add(field.getErr());
            }
        }
        return ret;
    }

    public ItemDetailModel getModel(long id) {
        if(isValid()) {
            return new ItemDetailModel(
                    id,
                    getName(),
                    getShortDescription(),
                    getLongDescription(),
                    MoneyParser.parse(getCurrentBid()),
                    DateParser.parse(getStartDate()),
                    DateParser.parse(getEndDate()),
                    getImageUrl()
            );
        }
        return null;
    }

    public void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeObject(this.getName());
        out.writeObject(this.getShortDescription());
        out.writeObject(this.getLongDescription());
        out.writeObject(this.getCurrentBid());
        out.writeObject(this.getStartDate());
        out.writeObject(this.getEndDate());
        out.writeObject(this.getImageUrl());
    }

    public void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.setName             ((String) in.readObject());
        this.setShortDescription ((String) in.readObject());
        this.setLongDescription  ((String) in.readObject());
        this.setCurrentBid       ((String) in.readObject());
        this.setStartDate        ((String) in.readObject());
        this.setEndDate          ((String) in.readObject());
        this.setImageUrl         ((String) in.readObject());
    }
}
