package edu.neumont.acorbin.nubay.utils;

/**
 * Created by Anthony on 2/9/2015.
 */
public class MathExtended {
    public static <T extends Comparable<T>> T Clamp(T src, T min, T max) {
        if(src.compareTo(min) < 0) return min;
        if(src.compareTo(max) > 0) return max;
        return src;
    }
    public static int Clamp(int num, int min, int max) {
        if(num < min) return min;
        if(num > max) return max;
        return num;
    }
    public static long Clamp(long num, long min, long max) {
        if(num < min) return min;
        if(num > max) return max;
        return num;
    }
    public static float Clamp(float num, float min, float max) {
        if(num < min) return min;
        if(num > max) return max;
        return num;
    }
    public static double Clamp(double num, double min, double max) {
        if(num < min) return min;
        if(num > max) return max;
        return num;
    }
}
