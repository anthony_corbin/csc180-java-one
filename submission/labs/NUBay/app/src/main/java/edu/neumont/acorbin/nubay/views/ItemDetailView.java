package edu.neumont.acorbin.nubay.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import junit.framework.Assert;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.Events.AntListener;
import edu.neumont.acorbin.nubay.utils.Events.SimpleObservable;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;

/**
 * Created by Anthony on 1/12/2015.
 */
public class ItemDetailView extends RelativeLayout {
    private TextView itemDesc;
    private TextView currentCost;
    private TextView timeLeft;
    private TextView itemNameLeft;
    private Button bidButton;
    private ItemDetailModel model;

    public ItemDetailView(Context context) {
        super(context);
    }

    public ItemDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemDetailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private AntListener<ItemDetailModel> listener = new AntListener<ItemDetailModel>() {
        @Override
        public void OnAntChange(ItemDetailModel obj) {
            if(obj == model) updateFromModel();
        }
    };

    public void setModel(ItemDetailModel toSet) {
        Assert.assertNotNull(toSet);
        if(model != null) {
            model.UnSubscribe(listener);
        }
        model = toSet;
        model.Subscribe(listener);
        updateFromModel();
    }

    private void updateFromModel() {
        itemDesc.setText(model.getLongDescription());
        currentCost.setText("Current Bid: " + MyStringUtils.formatMoney(model.getCurrentBid().toString()));
        itemNameLeft.setText(model.getName());
        updateTime();
    }
    public void updateTime() {
        if(model == null) return;
        if(model.valid() != bidButton.isEnabled()) {
            //bidButton.setVisibility(model.valid() ? Button.VISIBLE : Button.INVISIBLE);
            bidButton.setEnabled(model.valid());
        }
        if(model.valid())
            timeLeft.setText("Time Left: "+DateParser.formatTimeUntil(model.getEndDate()));
        else
            timeLeft.setText("Bid Time Over");
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //can now access XML
        itemDesc = (TextView) findViewById(R.id.textView_ItemDesc);
        currentCost = (TextView) findViewById(R.id.textView_CurrentCost);
        timeLeft = (TextView) findViewById(R.id.textView_timeLeft);
        bidButton = (Button) findViewById(R.id.button_Bid);
        itemNameLeft = (TextView)findViewById(R.id.textView_ItemName);

        bidButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == bidButton.getId() && modelListener != null) {
                    modelListener.BidButtonClicked(model);
                }
            }
        });

        itemDesc.setText("Herro Pie");
        currentCost.setText("Cost things");
        bidButton.setText("Bid ($5)");
    }

    ViewListener modelListener = null;
    public static interface ViewListener {
        void BidButtonClicked(ItemDetailModel model);
    };
    public void SetListener(ViewListener toSet) {
        modelListener = toSet;
    }
}
