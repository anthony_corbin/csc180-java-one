package edu.neumont.acorbin.nubay.models;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import edu.neumont.acorbin.nubay.utils.Compare;
import edu.neumont.acorbin.nubay.utils.Events.SimpleObservable;

/**
 * Created by Anthony on 1/12/2015.
 */
public class ItemDetailModel extends SimpleObservable<ItemDetailModel> implements Serializable {
    private static final long serialVersionUID = 0L;
    private long id;
    private String name;
    private String shortDescription;
    private String longDescription;
    private BigDecimal currentBid;
    private Date StartDate;
    private Date EndDate;
    private String imageUrl;

    public ItemDetailModel(long id, String name, String shortDescription, String longDescription, BigDecimal currentBid, Date startDate, Date endDate, String imageUrl) {
        this.id = id;
        this.name = name;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.currentBid = currentBid;
        StartDate = startDate;
        EndDate = endDate;
        this.imageUrl = imageUrl;
    }

    boolean wasValid = true;

    public boolean valid() {
        Date currentTime = Calendar.getInstance().getTime();
        boolean ret = (StartDate.getTime() <= currentTime.getTime() && currentTime.getTime() <= EndDate.getTime());
        if (wasValid != ret)
            wasValid = ret;
        return ret;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public BigDecimal getCurrentBid() {
        return currentBid;
    }

    public void setShortDescription(String shortDescription) {
        if (Compare.Compare(this.shortDescription, shortDescription)) {
            this.shortDescription = shortDescription;
            NotifyObservers(this);
        }
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setName(String name) {
        if (Compare.Compare(this.name, name)) {
            this.name = name;
            NotifyObservers(this);
        }
    }

    public void setLongDescription(String longDescription) {
        if (Compare.Compare(this.longDescription, longDescription)) {
            this.longDescription = longDescription;
            NotifyObservers(this);
        }
    }

    public void setCurrentBid(BigDecimal currentBid) {
        if (Compare.Compare(this.currentBid, currentBid)) {
            this.currentBid = currentBid;
            NotifyObservers(this);
        }
    }

    public void setImageUrl(String imageUrl) {
        if (Compare.Compare(this.imageUrl, imageUrl)) {
            this.imageUrl = imageUrl;
            NotifyObservers(this);
        }
    }


    public void increaseBid(BigDecimal amount) {
        currentBid = currentBid.add(amount);
        NotifyObservers(this);
    }

    @Override
    public String toString() {
        return "#" + id + " " + name + " (" + currentBid + ")";
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        if (Compare.Compare(this.EndDate, endDate)) {
            EndDate = endDate;
            NotifyObservers(this);
        }
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        if (Compare.Compare(this.StartDate, startDate)) {
            StartDate = startDate;
            NotifyObservers(this);
        }
    }

    public void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeObject(this.id);
        out.writeObject(this.name);
        out.writeObject(this.shortDescription);
        out.writeObject(this.longDescription);
        out.writeObject(this.currentBid);
        out.writeObject(this.StartDate);
        out.writeObject(this.EndDate);
        out.writeObject(this.imageUrl);
    }

    public void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.id               = in.readInt();
        this.name             = (String)     in.readObject();
        this.shortDescription = (String)     in.readObject();
        this.longDescription  = (String)     in.readObject();
        this.currentBid       = (BigDecimal) in.readObject();
        this.StartDate        = (Date)       in.readObject();
        this.EndDate          = (Date)       in.readObject();
        this.imageUrl         = (String)     in.readObject();
    }
}