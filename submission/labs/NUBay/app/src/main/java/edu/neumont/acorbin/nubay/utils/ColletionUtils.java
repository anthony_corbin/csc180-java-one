package edu.neumont.acorbin.nubay.utils;

import java.util.*;

import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;

/**
 * Created by Anthony on 1/20/2015.
 */
public class ColletionUtils {
    public interface Selector<IN,OUT> {
        OUT Select(IN obj);
    }
    public static <T> List<T> filter(List<T> elements, Predicate<T> theTest) {
        return filter(elements,theTest,new Selector<T, T>() {
            @Override public T Select(T obj) { return obj; }
        });
    }
    public static <DATA,PREDICATE> List<DATA> filter(List<DATA> elements, Predicate<PREDICATE> theTest, Selector<DATA,PREDICATE> selector) {
        List<DATA> filtered = new ArrayList<DATA>();
        for(DATA element : elements)
        {
            if(theTest.Invoke(selector.Select(element)))
            {
                filtered.add(element);
            }
        }
        return filtered;
    }
    public static <T> Set<T> filter(Collection<T> elements, Predicate<T> theTest) {
        return filter(elements,theTest,new Selector<T, T>() {
            @Override public T Select(T obj) { return obj; }
        });
    }
    public static <DATA,PREDICATE> Set<DATA> filter(Collection<DATA> elements, Predicate<PREDICATE> theTest, Selector<DATA,PREDICATE> selector) {
        Set<DATA> filtered = new HashSet<DATA>();
        for(DATA element : elements)
        {
            if(theTest.Invoke(selector.Select(element)))
            {
                filtered.add(element);
            }
        }
        return filtered;
    }

}
