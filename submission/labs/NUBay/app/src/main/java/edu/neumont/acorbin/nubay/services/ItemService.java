package edu.neumont.acorbin.nubay.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.utils.ColletionUtils;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;
import edu.neumont.acorbin.nubay.utils.Predicates.AndPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.NotPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.OrPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;
import edu.neumont.acorbin.nubay.utils.ReversePolish;
import edu.neumont.acorbin.nubay.utils.ShuntingYard;

/**
 * Created by Anthony on 1/21/2015.
 */
public class ItemService {
    public static class ItemClientException extends RuntimeException{
        public ItemClientException(String message) {
            super(message);
        }
        public ItemClientException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemClientException(Throwable cause) {
            super(cause);
        }
    }
    public static class ItemServiceException extends RuntimeException{
        public ItemServiceException(String message) {
            super(message);
        }
        public ItemServiceException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemServiceException(Throwable cause) {
            super(cause);
        }
    }
    public static class ItemNotFoundException extends RuntimeException {
        public ItemNotFoundException(String message) {
            super(message);
        }
        public ItemNotFoundException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemNotFoundException(Throwable cause) {
            super(cause);
        }
    }

    public static interface Logger {
        void CreateEvent(ItemDetailModel model);
        void DeleteEvent(long id);
        void EditEvent(ItemDetailModel model);
        Iterable<ItemDetailModel> GetAll(); // called when first loaded
    }

    private static class StaticModelSet implements Logger {
        private static HashMap<Long,ItemDetailModel> items;
        static {
            items = new HashMap<>();
            Date startDate = DateParser.getNowPlusDuration(0,0,-1,0,0,0,0);
            Date endDate = DateParser.getNowPlusDuration(0,30,0,0,0,0,0);
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Toaster",         "Very Brave",            "The Brave Toaster has killed 50 foes",                                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Potato",          "Very Starchy",          "The Brave Potato has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.lib.umn.edu/huber195/psy1001spring12/imagespotato-face.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Panda",           "He will Kill you",      "The Scared Panda has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.boostability.com/wp-content/uploads/2014/09/Panda-Update.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Singer",          "Person, sounds decent", "Mr Greg has a long career of hurting friends and family's ears with his \"music\"", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://4vector.com/i/free-vector-man-with-a-microphone_099808_Man_with_a_microphone.png")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Desktop Mic",     "Kinda works",           "If you smack it a few times, it starts working",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.altoedge.com/microphones/images/pc-microphone-gn3_hi.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Mic",             "Unlock your singer",    "Please let the singer you have caged up go, take this mic instead",                 new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://fc09.deviantart.net/fs14/f/2007/004/2/3/Microphone_by_gregVent.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"iPhone",          "$.$",                   "Do you like having money? I didn't think so",                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.telegraph.co.uk/multimedia/archive/02424/iphone-4s_2424784k.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Hose",            "not a gun",             "really it \"shoots\" water, but it isn't a gun",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://www.deere.com/common/media/images/product/home_and_workshop_products/air_tools/0076249_air_tools_942x458.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Hammer",          "hit things",            "Smack anything or anyone right into place with this shiny hammer",                  new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.popularmechanics.com/cm/popularmechanics/images/se/10-tools-for-kids-02-0513-lgn.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Toe Tail Clipper","For Men",               "For Toe nails made of steel",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.globalspec.com/ImageRepository/LearnMore/20129/ASTM_PEX_Crimp_Tool_3Ca64f4633735f4d9c93933bdd4bcdea53.png")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Plumber's crack", "for pipes and stuff",   "have a good excuse to not pull up your pants",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.johnnichollstrade.co.uk/assets/Rothenberger-tool-32.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Speaker",         "Makes noise",           "this one makes a lot of noise",                                                     new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://s3-us-west-1.amazonaws.com/static.brit.co/wp-content/uploads/2012/10/Wireless-Pill.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Alienware",       "Phone",                 "It doesn't really exist yet",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.blogcdn.com/www.engadget.com/media/2008/02/alienware-concept-phone.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Samsung Phone",   "Shiny and new",         "this one includes an app that can make calls",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://3.bp.blogspot.com/-jtiTpw22KZE/T8yDeavLDWI/AAAAAAAAAJg/Aoiq1uHEODw/s1600/best+new+samsung+android+smart+phone+with+3G+technology+at+MarketA2Z.com++%25286%2529.jpg")));
            ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Android",         "Android",               "Android",                                                                           new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")));
        }

        @Override
        public void CreateEvent(ItemDetailModel model) {
            items.put(model.getId(), model);
        }

        @Override
        public void DeleteEvent(long id) {
            items.remove(id);
        }

        @Override
        public void EditEvent(ItemDetailModel model) {
            items.remove(model.getId());
            items.put(model.getId(),model);
        }

        @Override
        public Iterable<ItemDetailModel> GetAll() {
            return items.values();
        }
    }

    private HashMap<Long,ItemDetailModel> itemsById;
    private static long ItemID = -1;

    private static ShuntingYard parser;
    static {
        Map<String, ShuntingYard.OperatorDetails> OPERATORS = new HashMap<>();
        OPERATORS.put("or",  new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("and", new ShuntingYard.OperatorDetails(1,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("not", new ShuntingYard.OperatorDetails(2,  ShuntingYard.Associativity.LEFT) );
        parser = new ShuntingYard(OPERATORS);
    }
    private static ReversePolish<String,Predicate<String>> eval;
    static {
        HashMap<String, ReversePolish.PolishOperator<Predicate<String>>> operators = new HashMap<>();
        operators.put("and", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new AndPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("or", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new OrPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("not", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 1; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new NotPredicate<>(data.pop()); }
        });



        eval = new ReversePolish<>(new ReversePolish.PolishValue<String, Predicate<String>>() {
            @Override
            public Predicate<String> eval(final String a) { // from search query
                return new Predicate<String>() {
                    @Override
                    public boolean Invoke(String val) {
                        return val.toLowerCase().contains(a.toLowerCase());
                    }
                };
            }
        }, operators);
    }

    private Logger logger;

    public ItemService() {
        logger = new StaticModelSet();
        itemsById = new HashMap<>();
        for(ItemDetailModel m : logger.GetAll()) {
            ItemID = Math.max(ItemID,m.getId()+1);
            itemsById.put(m.getId(),m);
        }
    }
    public ItemService(Logger logger) {
        this.logger = logger;
        itemsById = new HashMap<>();
        for(ItemDetailModel m : logger.GetAll()) {
            ItemID = Math.max(ItemID,m.getId()+1);
            itemsById.put(m.getId(),m);
        }
    }


    public  Collection<ItemDetailModel> GetAll(){
        return itemsById.values();
    }

    public Set<ItemDetailModel> search(String query){
        query = query.toLowerCase();
        Predicate<String> condition;
        Set<ItemDetailModel> ret;
        try {
            condition = eval.Eval(parser.infixToRPN(query));
            ret =  ColletionUtils.filter(
                    ColletionUtils.filter(itemsById.values(), new Predicate<ItemDetailModel>() {
                        @Override
                        public boolean Invoke(ItemDetailModel val) {
                            return val.valid();
                        }
                    })
                    ,condition, new ColletionUtils.Selector<ItemDetailModel, String>() {
                        @Override
                        public String Select(ItemDetailModel obj) {
                            return obj.getName() + " " + obj.getShortDescription() + " " + obj.getLongDescription();
                        }
                    });
        } catch(IllegalArgumentException e) {
            ret = new HashSet<>(); // empty set
        }
        return ret;
    }
    public ItemDetailModel bid(Long id, BigDecimal bidIncrease){
        if(itemsById.containsKey(id)) {
            ItemDetailModel model = itemsById.get(id);
            if(model.valid()) {
                model.increaseBid(bidIncrease);
                update(model);
            } else {
                throw new RuntimeException("Item Has Expired");
            }
            return model;
        }
        return null;
    }
    public ItemDetailModel bid(Long id, double bidIncrease){
        return bid(id,new BigDecimal(bidIncrease));
    }
    public ItemDetailModel getItemByID(long id) {
        if(itemsById.containsKey(id)) {
            return itemsById.get(id);
        }
        return null;
    }
    public ItemDetailModel getRandomValidModel() {
        int randomIndex = new Random().nextInt(itemsById.size());
        ItemDetailModel ret = null;
        int index = 0;
        for(ItemDetailModel i : itemsById.values()) {
            if(i.valid()) ret = i;
            if(ret != null && index >= randomIndex) return ret;
            index++;
        }
        return null;
    }
    public ItemDetailModel getRandomInValidModel() {
        int randomIndex = new Random().nextInt(itemsById.size());
        ItemDetailModel ret = null;
        int index = 0;
        for(ItemDetailModel i : itemsById.values()) {
            if(!i.valid()) ret = i;
            if(ret != null && index >= randomIndex) return ret;
            index++;
        }
        return null;
    }

    public ItemDetailModel update(ItemDetailModel model) {
        if(itemsById.containsKey(model.getId())) {

            try {
                logger.EditEvent(model);

                itemsById.get(model.getId()).setName(model.getName());
                itemsById.get(model.getId()).setShortDescription(model.getShortDescription());
                itemsById.get(model.getId()).setLongDescription(model.getLongDescription());
                itemsById.get(model.getId()).setCurrentBid(model.getCurrentBid());
                itemsById.get(model.getId()).setStartDate(model.getStartDate());
                itemsById.get(model.getId()).setEndDate(model.getEndDate());
                itemsById.get(model.getId()).setImageUrl(model.getImageUrl());
            } catch (Exception e) {
                throw new ItemServiceException("Something Broke", e);
            }
        } else {
            throw new ItemNotFoundException("update failed: "+model.getId()+" not in collection");
        }
        return null;
    }

    public void delete(long id) {
        if(itemsById.containsKey(id)) {
            try {
                logger.DeleteEvent(id);
                itemsById.remove(id);
            } catch (Exception e) {
                throw new ItemServiceException("Something Broke", e);
            }
        }
    }

    //returns ID of item created, null if item failed to create
    public Long AddNewObject(ItemDetailModel item) {
        try {
            logger.CreateEvent(item);
            itemsById.put(item.getId(), item);
        } catch (Exception e) {
            throw new ItemServiceException("Something Broke", e);
        }
        return item.getId();
    }
    public Long AddNewObject(ItemDetailStringBuilder item) {
        item.setImageUrl(MyStringUtils.javaStringLiteral("http://content.mycutegraphics.com/graphics/clothing/green-sock.png"));
        if(!item.isValid()) {
            throw new ItemClientException("Unable to add new model, it still invalid");
        }
        return AddNewObject(item.getModel(ItemID++));
    }
}
