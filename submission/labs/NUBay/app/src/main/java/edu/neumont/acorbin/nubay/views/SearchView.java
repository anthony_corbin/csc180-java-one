package edu.neumont.acorbin.nubay.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.SearchModel;
import edu.neumont.acorbin.nubay.services.AndroidDefaultService;
import edu.neumont.acorbin.nubay.services.ItemService;

/**
 * Created by Anthony on 1/26/2015.
 */
public class SearchView extends RelativeLayout {
    private TextView searchArea;
    private ListView searchResults;
    private SearchModel model = new SearchModel(getContext(),0,null);
    private ViewListener viewListener;

    public SearchView(Context context) {
        super(context);
    }
    public SearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        searchArea = (TextView)findViewById(R.id.editText_SearchBar);
        searchResults = (ListView)findViewById(R.id.listView_Results);
        ((Button)findViewById(R.id.button_updateList)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                updateListFromQuery();
            }
        });
        ((Button)findViewById(R.id.button_SearchCreateItem)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewListener != null) viewListener.onCreateItemClick();
            }
        });
        updateListFromQuery();
        searchArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (viewListener != null) viewListener.onTextChange(searchArea.getText().toString());
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void afterTextChanged(Editable s) { }
        });
        searchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (viewListener != null) viewListener.onTextChange(parent, view, position, id);
            }
        });
    }

    private void updateListFromQuery()
    {
        Set<ItemDetailModel> results = AndroidDefaultService.instance.search(searchArea.getText().toString());
        searchResults.setAdapter(model = new SearchModel(getContext(),0,new ArrayList<ItemDetailModel>(results)));
    }


    public void setListener(ViewListener listener)
    {
        viewListener = listener;
    }

    public void refresh() {
        updateListFromQuery();
    }

    public static interface ViewListener {
        public void onTextChange(AdapterView<?> parent, View view, int position, long id);
        public void onCreateItemClick();
        public void onTextChange(String newQuery);
    }
}