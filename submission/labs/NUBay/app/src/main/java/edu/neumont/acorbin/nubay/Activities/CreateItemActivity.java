package edu.neumont.acorbin.nubay.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.services.AndroidDefaultService;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.utils.FunctionInterfaces.Actions;
import edu.neumont.acorbin.nubay.utils.FunctionInterfaces.Functions;
import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;
import edu.neumont.acorbin.nubay.views.CreateItemView;

public class CreateItemActivity extends ActionBarActivity {

    CreateItemView view;

    HashMap<CreateItemView.ItemField,Actions.Action1<String>> updateFieldsMethods; // maps enum to correct method call

    ItemDetailStringBuilder stringModel = new ItemDetailStringBuilder();

    public CreateItemActivity() {
        updateFieldsMethods = new HashMap<>();
        updateFieldsMethods.put(CreateItemView.ItemField.Name,       new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setName(a);             } });
        updateFieldsMethods.put(CreateItemView.ItemField.ShortDesc,  new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setShortDescription(a); } });
        updateFieldsMethods.put(CreateItemView.ItemField.LongDesc,   new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setLongDescription(a);  } });
        updateFieldsMethods.put(CreateItemView.ItemField.StartPrice, new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setCurrentBid(a);       } });
        updateFieldsMethods.put(CreateItemView.ItemField.StartDate,  new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setStartDate(a);        } });
        updateFieldsMethods.put(CreateItemView.ItemField.EndDate,    new Actions.Action1<String>() { @Override public void Invoke(String a) { stringModel.setEndDate(a);          } });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = (CreateItemView) View.inflate(this, R.layout.activity_create_item, null);
        setContentView(view);
        view.SetListener(new CreateItemView.ViewListener() {
            @Override
            public void fieldChanged(CreateItemView.ItemField field, String newValue) {
                updateFieldsMethods.get(field).Invoke(newValue);
                if(stringModel.isValid()) {
                    view.enableItemCreation();
                    view.clearErrors();
                } else {
                    view.disableItemCreation();
                    view.setErrors(stringModel.getErrs());
                }
            }

            @Override
            public void CreateItemButtonPressed() {
                Long newId;
                if((newId = AndroidDefaultService.instance.AddNewObject(stringModel)) == null) {
                    Toast.makeText(view.getContext(),"Some error occured, please try re-entering model data",Toast.LENGTH_LONG).show();
                } else {
                    ItemDetailModel model = AndroidDefaultService.instance.getItemByID(newId);
                    Intent i = new Intent(getApplicationContext(), ItemDetailActivity.class);
                    i.putExtra("modelID", ""+model.getId());
                    startActivity(i);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_item, menu);
        return true;
    }
}
