package edu.neumont.acorbin.nubay.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import edu.neumont.acorbin.nubay.R;

/**
 * Created by Anthony on 2/9/2015.
 */
public class CreateItemView extends RelativeLayout {
    public enum ItemField {
        Name, ShortDesc, LongDesc, StartPrice, StartDate, EndDate
    }

    //region local vars =====================================================================================

    private TextView nameField, shortDescField, longDescField, startPriceField, startDateField, endDateField, errField;
    private Button createItemButton;

    private HashMap<ItemField,TextView> FieldToView = new HashMap<>();

    //endregion

    //region Init ===========================================================================================

    //region constructors ===================================================================================
    public CreateItemView(Context context) {
        super(context);
    }

    public CreateItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CreateItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //endregion

    private TextView initField(int id, final ItemField field) {
        final TextView ret = (TextView) findViewById(id);
        FieldToView.put(field,ret);
        ret.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(modelListener != null)
                    modelListener.fieldChanged(field,ret.getText().toString());
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void afterTextChanged(Editable s) { }
        });
        return ret;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //can now access XML
        nameField       = initField(R.id.editText_CreateName,      ItemField.Name       );
        shortDescField  = initField(R.id.editText_CreateShortDesc, ItemField.ShortDesc  );
        longDescField   = initField(R.id.editText_CreateLongDesc,  ItemField.LongDesc   );
        startPriceField = initField(R.id.editText_CreateBid,       ItemField.StartPrice );
        startDateField  = initField(R.id.editText_CreateStartDate, ItemField.StartDate  );
        endDateField    = initField(R.id.editText_CreateEndDate,   ItemField.EndDate    );
        errField = (TextView) findViewById(R.id.textView_createItemErrs);
        createItemButton = (Button) findViewById(R.id.button_createItem);

        createItemButton.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if(modelListener != null) modelListener.CreateItemButtonPressed();
            }
        });

        errField.setText("");
    }

    //endregion

    //region Public Methods =================================================================================

    public void clearErrors() {
        errField.setText("");
    }
    public void setErrors(Iterable<String> errors) {
        StringBuilder sb = new StringBuilder();
        for(String s : errors) {
            sb.append(s).append('\n');
        }
        sb.setLength(Math.max(sb.length() - 1, 0)); // remove last new line
        errField.setText(sb.toString());
    }
    public TextView getField(ItemField field) {
        return FieldToView.get(field);
    }
    public void setField(ItemField field, String toSet) {
        FieldToView.get(field).setText(toSet);
    }
    //aka invalid item
    public void disableItemCreation(){setCreateItemButtonState(false);}
    //aka valid item
    public void enableItemCreation(){setCreateItemButtonState(true);}
    public void setCreateItemButtonState(boolean enabled) {
        createItemButton.setEnabled(enabled);
    }

    //endregion

    //region Events =========================================================================================
    ViewListener modelListener = null;
    public static interface ViewListener {
        // will be null if no changes
        void fieldChanged(ItemField field, String newValue);
        void CreateItemButtonPressed();
    };
    public void SetListener(ViewListener toSet) {
        modelListener = toSet;
    }
    //endregion
}
