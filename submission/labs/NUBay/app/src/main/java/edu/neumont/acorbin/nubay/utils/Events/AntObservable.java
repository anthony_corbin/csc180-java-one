package edu.neumont.acorbin.nubay.utils.Events;

import java.util.ArrayList;

/**
 * Created by Anthony on 1/28/2015.
 */
public interface AntObservable<T> {
    void Subscribe(AntListener<T> toAdd);
    void UnSubscribe(AntListener<T> toRemove);
    void NotifyObservers(T obj);
}
