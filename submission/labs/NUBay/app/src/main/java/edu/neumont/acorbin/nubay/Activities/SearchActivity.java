package edu.neumont.acorbin.nubay.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.SearchModel;
import edu.neumont.acorbin.nubay.views.SearchView;


public class SearchActivity extends ActionBarActivity {
    private SearchView view;

    private SearchModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = (SearchView)View.inflate(getApplicationContext(),R.layout.activity_search, null);
        view.setListener(new SearchView.ViewListener() {
            @Override
            public void onTextChange(AdapterView<?> parent, View view, int position, long id) {
                ItemDetailModel model = (ItemDetailModel) parent.getItemAtPosition(position);
                Intent i = new Intent(getApplicationContext(), ItemDetailActivity.class);
                i.putExtra("modelID", ""+model.getId());
                startActivity(i);
            }

            @Override
            public void onCreateItemClick() {
                Intent i = new Intent(getApplicationContext(), CreateItemActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
            }

            @Override
            public void onTextChange(String newQuery) {
                view.refresh();
            }
        });
        setContentView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        view.refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
