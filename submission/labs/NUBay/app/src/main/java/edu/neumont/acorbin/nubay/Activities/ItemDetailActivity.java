package edu.neumont.acorbin.nubay.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.AndroidDefaultService;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.views.ItemDetailView;


public class ItemDetailActivity extends ActionBarActivity {
    private ItemDetailView view;

    ItemDetailModel model;

    DownloadImageTask downloadTask;

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String ...urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if(bmImage != null) {
                bmImage.setImageBitmap(result);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = (ItemDetailView) View.inflate(this, R.layout.activity_item_detail,null);
        setContentView(view);
        view.SetListener(new ItemDetailView.ViewListener() {
            @Override
            public void BidButtonClicked(ItemDetailModel model) {
                try {
                    AndroidDefaultService.instance.bid(model.getId(), 5);
                } catch (RuntimeException e) {
                    Toast.makeText(getApplicationContext(),"Bidding closed for item",Toast.LENGTH_LONG);
                }
            }
        });
        Intent i = getIntent();
        String modelID = i.getStringExtra("modelID");
        changeToModel(Long.valueOf(modelID).longValue());

        Thread t = new Thread(){
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                view.updateTime();
                            }
                        });
                    }
                } catch(Exception e) {}
            }
        };
        t.start();
    }

    private void changeToModel(long modelId) {
        model = AndroidDefaultService.instance.getItemByID(modelId);
        view.setModel(model);
        if(downloadTask != null) downloadTask.cancel(true);
        downloadTask = new DownloadImageTask((ImageView) findViewById(R.id.imageView));
        downloadTask.execute(model.getImageUrl());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
