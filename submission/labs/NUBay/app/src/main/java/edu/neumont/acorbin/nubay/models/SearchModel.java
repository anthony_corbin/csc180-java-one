package edu.neumont.acorbin.nubay.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.neumont.acorbin.nubay.R;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;

/**
 * Created by Anthony on 1/21/2015.
 */
public class SearchModel extends ArrayAdapter<ItemDetailModel> {
    Context context;
    int resource;
    List<ItemDetailModel> objects;

    public SearchModel(Context context, int resource, List<ItemDetailModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ItemDetailModel item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.single_item_display, parent, false);
        }
        // Lookup view for data population
        TextView itemName  = (TextView) convertView.findViewById(R.id.textView_Name);
        TextView itemPrice = (TextView) convertView.findViewById(R.id.textView_Price);
        TextView itemDesc  = (TextView) convertView.findViewById(R.id.textView_Desc);
        // Populate the data into the template view using the data object
        itemName.setText(item.getName());
        itemPrice.setText(MyStringUtils.formatMoney(item.getCurrentBid().toString()));
        itemDesc.setText(item.getShortDescription());
        // Return the completed view to render on screen
        return convertView;
    }
}