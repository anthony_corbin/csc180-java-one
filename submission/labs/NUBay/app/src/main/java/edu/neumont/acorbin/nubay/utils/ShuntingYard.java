package edu.neumont.acorbin.nubay.utils;

import java.util.*;

import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;

/**
 * Created by Anthony on 1/24/2015.
 */
public class ShuntingYard {
    public static enum Associativity {
        LEFT, RIGHT
    }

    public static class OperatorDetails {
        private int Weight;
        private Associativity ASSOC;

        public OperatorDetails(int weight, Associativity assoc) {
            Weight = weight;
            ASSOC = assoc;
        }

        public int getWeight() {
            return Weight;
        }

        public Associativity getASSOC() {
            return ASSOC;
        }
    }

    private Map<String, OperatorDetails> OPERATORS;
    private Set<String> LeftParenthesis;
    private Set<String> RightParenthesis;

    public ShuntingYard(Map<String, OperatorDetails> operators, Set<String> leftParenthesis, Set<String> rightParenthesis) {
        OPERATORS = operators;
        LeftParenthesis = leftParenthesis;
        RightParenthesis = rightParenthesis;
    }
    public ShuntingYard(Map<String, OperatorDetails> operators) {
        OPERATORS = operators;
        LeftParenthesis = new HashSet<>(Arrays.asList("( [".split(" ")));
        RightParenthesis = new HashSet<>(Arrays.asList(") ]".split(" ")));
    }


    public List<String> infixToRPN(String input) {
        return infixToRPN(input,true);
    }
    public List<String> infixToRPN(String input, boolean addSpacesBetweenOperators) {
        for(String P : LeftParenthesis) {
            input = input.replace(P," "+P+" ");
        }
        for(String P : RightParenthesis) {
            input = input.replace(P," "+P+" ");
        }
        if(addSpacesBetweenOperators) {
            for (String P : OPERATORS.keySet()) {
                input = input.replace(P, " " + P + " ");
            }
        }
        List<String> inputs = ColletionUtils.filter(Arrays.asList(input.split(" ")), new Predicate<String>() {
            @Override
            public boolean Invoke(String val) {
                return !val.equals("");
            }
        });
        return infixToRPN(inputs);
    }
    public List<String> infixToRPN(List<String> inputTokens) {
        return infixToRPN(inputTokens,OPERATORS,LeftParenthesis,RightParenthesis);
    }
    public static List<String> infixToRPN(String inputTokens, Map<String, OperatorDetails> OPERATORS) {
        return new ShuntingYard(OPERATORS).infixToRPN(inputTokens);
    }
    public static List<String> infixToRPN(List<String> inputTokens, Map<String, OperatorDetails> OPERATORS) {
        return new ShuntingYard(OPERATORS).infixToRPN(inputTokens);
    }
    public static <T> List<T> infixToRPN(List<T> inputTokens, Map<T, OperatorDetails> OPERATORS, Set<T> LeftParenthesis, Set<T> RightParenthesis) {
        ArrayList<T> ret = new ArrayList<>();
        Stack<T> stack = new Stack<>();
        for (T token : inputTokens) {
            if (OPERATORS.containsKey(token)) {
                OperatorDetails tokenDetails = OPERATORS.get(token);
                while ( !stack.empty() && OPERATORS.containsKey(stack.peek())) { // If token is an operator pop off operators in stack
                    OperatorDetails details = OPERATORS.get(stack.peek());
                    int precedence = tokenDetails.getWeight() - details.getWeight();
                    if (  (details.getASSOC() == Associativity.LEFT  && precedence <= 0)    // same or less weight for left operators
                            ||(details.getASSOC() == Associativity.RIGHT && precedence <  0)) { // only include right operator if higher priority
                        ret.add(stack.pop());
                        continue;
                    }
                    break;
                }
                // Push the new operator on the stack
                stack.push(token);
            } else if (LeftParenthesis.contains(token)) {
                stack.push(token);
            } else if (RightParenthesis.contains(token)) {
                // pop off until beginning '('
                while ( !stack.empty() && !LeftParenthesis.contains(stack.peek())) {
                    ret.add(stack.pop());
                }
                if(stack.empty()) {
                    throw new IllegalArgumentException("unmatched )");
                }
                stack.pop(); // discard '('
            } else {
                ret.add(token); // just another var
            }
        }
        while (!stack.empty()) {
            if(LeftParenthesis.contains(stack.peek())) {
                throw new IllegalArgumentException("unmatched (");
            }
            ret.add(stack.pop());
        }
        return ret;
    }
}