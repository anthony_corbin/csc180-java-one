package edu.neumont.acorbin.nubay.utils;


import java.util.HashMap;
import java.util.Stack;


/**
 * Created by Anthony on 1/21/2015.
 */
public class ReversePolish<inputType,operatorType> {
    public interface PolishOperator<RetType> {
        int getOpCount();
        RetType eval(Stack<RetType> data);
    }
    public interface PolishValue<T,RetType> {
        RetType eval(T a);
    }

    public static <T> T Eval(Iterable<T> inputs, HashMap<T,PolishOperator<T>> operators) {
        return Eval(inputs, new PolishValue<T, T>() {
            @Override
            public T eval(T a) {
                return a;
            }
        },operators);
    }

    enum PolishValidity { valid, tooManyValues, tooManyOperators };
    public static <T,O> PolishValidity Valid(Iterable<T> inputs,HashMap<T,PolishOperator<O>> operators) {
        if (inputs == null || operators == null) {
            throw new IllegalArgumentException("Null pass for argument");
        }
        int valueCount = 0;
        for (T input : inputs) {
            if (operators.containsKey(input)) {
                valueCount -= operators.get(input).getOpCount() - 1; // -1 because it adds 1 back in
                if (valueCount <= 0) {
                    return PolishValidity.tooManyOperators;
                }
            } else {
                valueCount++;
            }
        }
        if (valueCount != 1)
            return PolishValidity.tooManyValues;
        return PolishValidity.valid;
    }

    public static <inputType,operatorType> operatorType Eval(Iterable<inputType> inputs, PolishValue<inputType,operatorType> selector, HashMap<inputType,PolishOperator<operatorType>> operators) {
        if(Valid(inputs,operators) != PolishValidity.valid)
            throw new IllegalArgumentException(Valid(inputs,operators).toString());

        Stack<operatorType> values = new Stack<operatorType>();
        for(inputType input : inputs) {
            if(operators.containsKey(input)) {
                Stack<operatorType> data = new Stack<operatorType>();
                for (int i = 0; i < operators.get(input).getOpCount(); i++) {
                    data.push(values.pop());
                }
                values.push(operators.get(input).eval(data));
            } else {
                values.push(selector.eval(input));
            }
        }
        return values.pop();
    }

    HashMap<inputType,PolishOperator<operatorType>> operators;
    PolishValue<inputType,operatorType> selector;
    public ReversePolish(PolishValue<inputType,operatorType> selector, HashMap<inputType,PolishOperator<operatorType>> operators) {
        this.operators = operators;
        this.selector = selector;
    }

    public operatorType Eval(Iterable<inputType> inputs) {
        return Eval(inputs,selector,operators);
    }
}
