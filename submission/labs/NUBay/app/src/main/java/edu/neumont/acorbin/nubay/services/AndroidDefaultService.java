package edu.neumont.acorbin.nubay.services;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;

/**
 * Created by Anthony on 2/18/2015.
 */
public class AndroidDefaultService {
    public static final ItemService instance = new ItemService(new ItemService.Logger() {
        @Override
        public void CreateEvent(ItemDetailModel model) {

        }

        @Override
        public void DeleteEvent(long id) {

        }

        @Override
        public void EditEvent(ItemDetailModel model) {

        }

        @Override
        public Iterable<ItemDetailModel> GetAll() {
            return null;
        }
    });
}
