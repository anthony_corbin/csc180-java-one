import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

public class SpamDetectorTest {

	SpamDetector spammy;

	@Before
	public void setUp() throws Exception {
		File initialFile = new File("antispam-table.txt");
		InputStream stream = new FileInputStream(initialFile);
		spammy = new SpamDetector(stream);
	}

	@Test
	public void testIsSpam() throws Exception {
		Assert.assertTrue(spammy.isSpam(new Email("to", "from", "sub", "Hey Bob, I like cheese")));
	}
	@Test
	public void testIsNotSpam() throws Exception {
		Assert.assertTrue(spammy.isSpam(new Email("to","from","sub","responsible chains")));
	}
}