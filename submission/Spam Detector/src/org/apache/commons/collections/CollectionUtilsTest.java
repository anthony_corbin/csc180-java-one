package org.apache.commons.collections;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CollectionUtilsTest {

	@Test
	public void testeCreateNew() throws Exception {
		new CollectionUtils();
	}
	@Test
	public void testCardinality() throws Exception {
		ArrayList<Integer> myList = new ArrayList<Integer>();
		Assert.assertEquals(0, CollectionUtils.cardinality(10, myList));
		myList.add(10);
		Assert.assertEquals(1, CollectionUtils.cardinality(10, myList));
		myList.add(10);
		Assert.assertEquals(2, CollectionUtils.cardinality(10, myList));
		Assert.assertNotSame(3, CollectionUtils.cardinality(10, myList));
	}
	@Test
	public void testCardinalityNullCol() throws Exception {
		Assert.assertEquals(0, CollectionUtils.cardinality(null, null));
		Assert.assertEquals(0, CollectionUtils.cardinality(42, null));
	}
	@Test
	public void testCardinalityNull() throws Exception {
		ArrayList<Integer> myList = new ArrayList<Integer>();
		Assert.assertEquals(0, CollectionUtils.cardinality(null, myList));
		myList.add(110);
		myList.add(null);
		Assert.assertEquals(1, CollectionUtils.cardinality(null, myList));
		myList.add(100);
		Assert.assertEquals(1, CollectionUtils.cardinality(null, myList));
		myList.add(210);
		myList.add(null);
		myList.add(104);
		Assert.assertEquals(2, CollectionUtils.cardinality(null, myList));
	}
}