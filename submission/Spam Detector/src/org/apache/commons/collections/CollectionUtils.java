package org.apache.commons.collections;

/**
 * Created by Anthony on 1/6/2015.
 */
public class CollectionUtils {
	//Returns the number of occurrences of obj in col.
	public static int cardinality(java.lang.Object obj, java.util.Collection col) {
		int ret = 0;
		if(col != null) {
			for (Object o : col) {
				if (o == obj || o.equals(obj)) {
					ret++;
				}
			}
		}
		return ret;
	}
}
