import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class EmailTest {

	@Test
	public void testGettersSimple() throws Exception {
		String to, from, subject, content;
		to = "to@no.pie";
		from = "from@someone.pie";
		subject = "subject";
		content = "content";
		Email e = new Email(to, from, subject, content);
		Assert.assertEquals(to, e.getTo());
		Assert.assertEquals(from, e.getFrom());
		Assert.assertEquals(subject, e.getSubject());
		Assert.assertEquals(content, e.getContent());
	}

	@Test
	public void testGettersSpecial() throws Exception {
		String to, from, subject, content;
		to = "to with@spaces.hi";
		from = "Corbin@Anthony.com";
		subject = "this; is is sparta!!!";
		content = "@#%&*()(*$#@FGHJKLKJHFDFG lklkjdsfs  pou123";
		Email e = new Email(to, from, subject, content);
		Assert.assertEquals(to, e.getTo());
		Assert.assertEquals(from, e.getFrom());
		Assert.assertEquals(subject, e.getSubject());
		Assert.assertEquals(content, e.getContent());
	}

	@Test
	public void testGettersSimpleStream() throws Exception {
		String to, from, subject, content;
		to = "towith@spaces";
		from = "Corbin@Anthony";
		subject = "MySubject"; // having !! breaks
		content = "@#%&*()(*$#@FGHJKLKJHFDFG lklkjdsfs  pou123";
		InputStream input = new ByteArrayInputStream(("From:"+from+"\nTo:"+to+"\nSubject:"+subject+"\n"+content).getBytes(StandardCharsets.UTF_8));
		Email e = new Email(input);
		Assert.assertEquals(to, e.getTo());
		Assert.assertEquals(from, e.getFrom());
		Assert.assertEquals(subject, e.getSubject());
		Assert.assertEquals(content, e.getContent());
	}

	@Test
	public void testGettersStreamWithSpaces() throws Exception {
		String to, from, subject, content;
		to = "towith@spaces";
		from = "Corbin@Anthony";
		subject = "Subject_with_spaces";
		content = "@#%&*()(*$#@FGHJKLKJHFDFG lklkjdsfs  pou123";
		InputStream input = new ByteArrayInputStream(("From:"+from+"\nTo:"+to+"\nSubject:"+subject+"\n"+content).getBytes(StandardCharsets.UTF_8));
		Email e = new Email(input);
		Assert.assertEquals(to, e.getTo());
		Assert.assertEquals(from, e.getFrom());
		Assert.assertEquals(subject, e.getSubject());
		Assert.assertEquals(content, e.getContent());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGettersStreamComplexSubject() throws Exception {
		InputStream input = new ByteArrayInputStream(("This is invalid").getBytes(StandardCharsets.UTF_8));
		new Email(input);
	}
}