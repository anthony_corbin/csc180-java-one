import org.junit.Assert;
import org.junit.Test;

public class WordFeatureTest {

	@Test
	public void testGetters() throws Exception {
		String word = "word";
		int ham = 42;
		int spam = 24;
		WordFeature w = new WordFeature(word,ham,spam);
		Assert.assertEquals(word, w.getWord());
		Assert.assertEquals(ham,  (int)w.getHamCount());
		Assert.assertEquals(spam, (int)w.getSpamCount());
	}
}