package WeekOne;

import java.util.Observable;

/**
 * Created by Anthony on 1/7/2015.
 */
public class Flight extends Observable {
	private final String arrivalCity;
	private final String departureCity;
	private final Integer flightNumber;

	public Flight(String arrivalCity, String departureCity, Integer flightNumber) {
		this.arrivalCity = arrivalCity;
		this.departureCity = departureCity;
		this.flightNumber = flightNumber;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public Integer getFlightNumber() {
		return flightNumber;
	}


	@Override
	public String toString() {
		return "#"+flightNumber+" "+arrivalCity + "=>" + departureCity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Flight)) return false;
		return equals((Flight) o);
	}
	public boolean equals(Flight that) {
		boolean ret = false;
		if(that!=null) {
			ret = true
				&& CompareNullable(arrivalCity   , that.arrivalCity   )
				&& CompareNullable(departureCity , that.departureCity )
				&& CompareNullable(flightNumber  , that.flightNumber  )
			;
		}
		return ret;
	}

	private static <T> boolean CompareNullable(T a, T b) {
		return a == null ? b == null : a.equals(b);
	}

	@Override
	public int hashCode() {
		int result = arrivalCity != null ? arrivalCity.hashCode() : 0;
		result = 31 * result + (departureCity != null ? departureCity.hashCode() : 0);
		result = 31 * result + (flightNumber != null ? flightNumber.hashCode() : 0);
		return result;
	}
}
