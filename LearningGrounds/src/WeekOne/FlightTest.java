package WeekOne;

import org.junit.Assert;
import org.junit.Test;

public class FlightTest {

	@Test
	public void testGetFlightNumber() throws Exception {
		String arrive = "arrive";
		String depart = "depart";
		Integer flightNum = 192;
		Flight f = new Flight(arrive,depart,flightNum);
		Assert.assertEquals( arrive,    f.getArrivalCity()   );
		Assert.assertEquals( depart,    f.getDepartureCity() );
		Assert.assertEquals( flightNum, f.getFlightNumber()  );
	}

	@Test
	public void testToString() throws Exception {
		Flight f = new Flight("arrive","Depart",192);
		System.out.println(f);
		Assert.assertTrue(f != null);
	}

	@Test
	public void testEquals() throws Exception {
		Flight f = new Flight("arrive","Depart",192);
		Flight f2 = new Flight("arrive","Depart",192);
		Assert.assertEquals("ObjCall", f, f2);
		Assert.assertTrue("Obj Compare", f != f2);
		Assert.assertTrue("FlightCall", f.equals(f2));
	}

	@Test
	public void testEqualsNull() throws Exception {
		Flight f = new Flight(null,"Depart",192);
		Flight f2 = new Flight(null,"Depart",192);
		Assert.assertEquals("ObjCall", f, f2);
		Assert.assertTrue("Obj Compare", f != f2);
		Assert.assertTrue("FlightCall", f.equals(f2));
	}

	@Test
	public void testNotEqualsNull() throws Exception {
		Flight f = new Flight(null,"Depart",192);
		Flight f2 = new Flight("arrive","Depart",192);
		Assert.assertNotEquals("ObjCall", f, f2);
		Assert.assertTrue("Obj Compare", f != f2);
		Assert.assertTrue("FlightCall", !f.equals(f2));
	}
	@Test
	public void testNotEquals() throws Exception {
		Flight f = new Flight("no","Depart",192);
		Flight f2 = new Flight("arrive","Depart",192);
		Assert.assertNotEquals("ObjCall", f, f2);
		Assert.assertTrue("Obj Compare", f != f2);
		Assert.assertTrue("FlightCall", !f.equals(f2));
	}
}