package Week3;

/**
 * Created by Anthony on 1/28/2015.
 */
public class Compare {
	public static <T> boolean Compare(T a, T b) { return a == null ? b == null : a.equals(b); }
	public static boolean Compare(byte a,    byte b   ) { return a == b; }
	public static boolean Compare(int a,     int b    ) { return a == b; }
	public static boolean Compare(char a,    char b   ) { return a == b; }
	public static boolean Compare(long a,    long b   ) { return a == b; }
	public static boolean Compare(short a,   short b  ) { return a == b; }
	public static boolean Compare(float a,   float b  ) { return a == b; }
	public static boolean Compare(double a,  double b ) { return a == b; }
	public static boolean Compare(boolean a, boolean b) { return a == b; }
}
