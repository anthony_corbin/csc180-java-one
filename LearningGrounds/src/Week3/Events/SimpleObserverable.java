package Week3.Events;

import java.util.ArrayList;

/**
 * Created by Anthony on 1/27/2015.
 */
public interface SimpleObserverable<T> {
	ArrayList<SimpleListener> listeners = new ArrayList<>();
	default void Subscribe(SimpleListener<T> toAdd) {
		listeners.add(toAdd);
	}
	default void UnSubscribe(SimpleListener<T> toRemove) {
		listeners.remove(toRemove);
	}
	default void NotifyObservers(T obj) {
		for (SimpleListener<T> l : listeners) {
			l.OnAntChange(obj);
		}
	}
}
