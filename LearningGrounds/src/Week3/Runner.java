package Week3;

import java.util.ArrayList;
import java.util.List;
//import java.util.function.Predicate;

/**
 * Created by Anthony on 1/20/2015.
 */
public class Runner {


	public static String formatMoney(String amount) {
		String frontPart = amount.substring(0,amount.indexOf('.'));
		String Decimals = amount.substring(amount.indexOf('.'),amount.length()).substring(0,3);
		return "$"+frontPart + Decimals;
	}


	public static void main(String[]args) {
		/* the problem
		List<Object> d;// = new ArrayList<String>();
		d.add(3);
		//*/
		/* one solution
		List<? extends Object> d = new ArrayList<Object>();
		//d.add(3); // makes readonly
		//d.add("pie"); // makes readonly
		//*/


		String t = formatMoney("1234.5678");


		List<String> col = new ArrayList<String>() {{
			add("Bob");
			add("Bill");
			add("Tom");
		}};
		List<String> theB = ColletionUtils.filter(col, s -> s.contains("B"));
		List<String> theT = ColletionUtils.filter(col, s -> (s.contains("T")));
		System.out.println(theB.size());
		System.out.println(theT.size());
	}
}
