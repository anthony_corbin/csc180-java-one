package Week3.Predicates;

/**
 * Created by Anthony on 1/22/2015.
 */
@FunctionalInterface
public interface Predicate<T> {
	boolean Invoke(T val);
}
