package Formatting;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anthony on 1/29/2015.
 */
public class FormattingExperiments {

	private static final Pattern datePattern   = Pattern.compile("(\\w+|\\d{2}|\\d)[\\-\\.\\s/]+(?:(\\d{2}|\\d)?[\\-\\.\\s/,]+)?(\\d{2,4})");
	private static final Pattern socialPattern = Pattern.compile("(\\d{3})[-\\s]?(\\d{2})[-\\s]?(\\d{4})");
	//private static final Pattern moneyPattern  = Pattern.compile("\\$?(([0-9]+),?)*.?([0-9]+)?");
	private static final Pattern moneyPattern  = Pattern.compile("\\$?(((\\d+,?)*\\d+.?\\d+)|(.\\d+))");
	private static final HashMap<String,Integer> strToMonth;
	static {
		strToMonth = new HashMap<>();
		strToMonth.put("jan",  1);	strToMonth.put("january",   1);
		strToMonth.put("feb",  2);	strToMonth.put("february",  2);
		strToMonth.put("mar",  3);	strToMonth.put("march",     3);
		strToMonth.put("apr",  4);	strToMonth.put("april",     4);
		strToMonth.put("ma",   5);	strToMonth.put("may",       5);
		strToMonth.put("jun",  6);	strToMonth.put("june",      6);
		strToMonth.put("jul",  7);	strToMonth.put("july",      7);
		strToMonth.put("aug",  8);	strToMonth.put("august",    8);
		strToMonth.put("sept", 9);	strToMonth.put("september", 9);
		strToMonth.put("oct", 10);	strToMonth.put("october",  10);
		strToMonth.put("nov", 11);	strToMonth.put("november", 11);
		strToMonth.put("dec", 12);	strToMonth.put("december", 12);
	}

	@FunctionalInterface
	private static interface GetInput {
		String getString();
	}
	public static Matcher getFromUser(Pattern p, String err, GetInput inputer) {
		//Scanner input = new Scanner(System.in);
		while(true) {
			String user = inputer.getString();
			Matcher m = p.matcher(user);
			if(m.matches()) {
				return m;
				//return Double.parseDouble(m.group(1) + m.group(2) + m.group(3));
			} else {
				System.out.println(err);
			}
		}
	}
	public static Date getDateFromUser(GetInput inputer) {
		while(true) {
			try {
				Matcher date = getFromUser(datePattern, "Invalid date, please try again", inputer);
				int month = strToMonth.containsKey(date.group(1).toLowerCase()) ? strToMonth.get(date.group(1).toLowerCase()) : Integer.parseInt(date.group(1));
				int day = date.group(2) != null ? Integer.parseInt(date.group(2)) : 1;
				int year = Integer.parseInt(date.group(3));
				int ourBase = 2000;
				Calendar c = Calendar.getInstance(); // set to now
				if (year < 100) {
					if (year + ourBase > c.get(Calendar.YEAR)) { // before 2,000
						year += 1900;
					} else { // after 2,000
						year += 2000;
					}
				}
				c.set(Calendar.MONTH, month-1);
				c.set(Calendar.DATE, day);
				c.set(Calendar.YEAR, year);
				return c.getTime();
			} catch (Exception e) {
				System.out.println("Invalid month/day, please try again");
			}
		}
	}


	public static void Mafia() {
		GetInput scannerIn = new GetInput() {
			Scanner in = new Scanner(System.in);
			@Override
			public String getString() {
				return in.nextLine();
			}
		};


		System.out.println("Welcome to MafiaPlusPlus. Please enter your date of birth (dd/mm/yyyy)");
		Date birthday = getDateFromUser(scannerIn);

		System.out.println("Enter Social");
		Matcher social = getFromUser(socialPattern, "Invalid social number, please try again", scannerIn);
		System.out.println("Thank you. Please enter your bank account balance (no, we are not trying to steal your identity.  In all likelihood, we already have it):");
		double bankAmount = Double.parseDouble(getFromUser(moneyPattern, "Invalid money format, please try again", scannerIn).group(0).replaceAll("[$,]", ""));

		System.out.println("You were born on " + new SimpleDateFormat("MMMM dd, yyyy").format(birthday));
		System.out.print("Your social is:");
		char delim = ' ';
		for (int i = 1; i < social.groupCount()+1; i++) {
			System.out.print(delim);
			System.out.print(social.group(i));
			delim = '-';
		}
		System.out.println();
		System.out.println("Your bank account balance is: " + MoneyParser.format(bankAmount));
	}

	public static void main(String[]args) {
		Mafia();
	}
}
