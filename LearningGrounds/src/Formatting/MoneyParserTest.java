package Formatting;

import junit.framework.TestCase;
import org.junit.Assert;

import java.math.BigDecimal;

public class MoneyParserTest extends TestCase {

	class Tuple<T1,T2> {
		public T1 val1;
		public T2 val2;
		public Tuple(T1 val1, T2 val2) {
			this.val1 = val1;
			this.val2 = val2;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Tuple tuple = (Tuple) o;

			if (val1 != null ? !val1.equals(tuple.val1) : tuple.val1 != null) return false;
			if (val2 != null ? !val2.equals(tuple.val2) : tuple.val2 != null) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = val1 != null ? val1.hashCode() : 0;
			result = 31 * result + (val2 != null ? val2.hashCode() : 0);
			return result;
		}
	}

	public void testParse() throws Exception {
		Tuple<String,Double> tests[] = new Tuple[]{
				new Tuple<>("1", 1.0),
				new Tuple<>("1.00", 1.0),
				new Tuple<>("$1.00", 1.0),
				new Tuple<>("$1", 1.0),
				new Tuple<>("$1,000", 1000.0),
				new Tuple<>("$1,00 ", 100.0),
				new Tuple<>("1.0", 1.0),
				new Tuple<>("", .01),
		};
		for(Tuple<String,Double> set : tests) {
			double a = set.val2;
			double b = MoneyParser.parse(set.val1).doubleValue();
			Assert.assertEquals( a, b, .001);
		}
		try {
			MoneyParser.parse("potato");
			Assert.fail();
		} catch(NumberFormatException e) {

		}
	}

	public void testFormat() throws Exception {
		Assert.assertEquals("$5.00", MoneyParser.format(new BigDecimal(5)));
		Assert.assertEquals("$5.00", MoneyParser.format(5.0));
		Assert.assertEquals("$0.50", MoneyParser.format(.5));
		Assert.assertEquals("($1.50)", MoneyParser.format(-1.5));
	}

}