package FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Anthony on 2/10/2015.
 */
public class FileExperiments {

	static long getNumLength(long num) {
		long ret = 1;
		num = Math.abs(num);
		while(true)
		{
			if (num < Math.pow(10, ret)) {
				return ret;
			}
			ret++;
		}
	}

	public static void main(String[]args) throws FileNotFoundException {
		//System.out.println(new File(".").getAbsolutePath());
		//Scanner scanner = new Scanner(new File("helloWorld.txt"));
		//while(scanner.hasNext()) {
		main2();
		//*
		long num = 0;
		while(true) {
			long numLength = getNumLength(num);
			Scanner scanner = new Scanner(new File("helloWorld.txt"));
			if(num % Math.pow(10,Math.min(5,numLength-1)) == 0) {
				System.out.printf("%s %s\n", num, scanner.nextLine());
			}
			num++;
		}
		//*/
	}


	private static void main2() {
		while(true) {
			try(Scanner scanner = new Scanner(System.in)) {
				System.out.println(scanner.nextInt());
			} catch (InputMismatchException | NullPointerException e) {
				throw new RuntimeException(e);
			} finally {
				System.out.println("final");
			}
		}
	}
}
