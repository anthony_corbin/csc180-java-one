package FileIO;

import java.io.*;

/**
 * Created by Anthony on 2/11/2015.
 */
public class StreamVsWriter {
	public static void main(String[]args) throws IOException {

		InputStream is = new FileInputStream(new File("helloWorld.txt"));

		System.out.println((char)is.read());
		System.out.println(is.read());

		System.out.println("\n" +
				"+-------------+\n" +
				"| NEW SECTION |\n" +
				"+-------------+\n" +
				"\n");

		is = new FileInputStream(new File("helloWorld.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		System.out.println(br.readLine());
		System.out.println(br.readLine());

	}
}
