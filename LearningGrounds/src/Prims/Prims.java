package Prims;

import java.util.*;

/**
 * Created by Anthony on 3/1/2015.
 */
public class Prims {

	public interface Node<T>{
		public T getValue();
		public int getID();
		public List<Edge<T>> getNeighbors();
	}

	public static class Edge<T> implements Comparable<Edge<T>>{
		private Node<T> a, b;
		private double weight;

		public Edge(Node<T> a, Node<T> b, double weight){
			if(a == null)throw new IllegalArgumentException("a must not be null");
			if(b == null)throw new IllegalArgumentException("b must not be null");
			this.a = a;
			this.b = b;
			this.weight = weight;
		}

		public Node<T> getA(){
			return a;
		}

		public Node<T> getB(){
			return b;
		}

		public double getWeight(){
			return weight;
		}

		public void setWeight(double weight){
			this.weight = weight;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + a.getID() + b.getID();
			long temp;
			temp = Double.doubleToLongBits(weight);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@SuppressWarnings("unchecked")
		public boolean equals(Object o){
			if(this == o)return true;
			if (o == null)return false;
			if (getClass() != o.getClass())return false;
			Edge<T> e = (Edge<T>)o;
			if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(e.weight))return false;
			return ((a == e.a && b == e.b) || (a == e.b && b == e.a));
		}

		@Override
		public int compareTo(Edge<T> o) {
			return (int)(weight - o.weight);
		}

		@Override
		public String toString(){
			return a.getValue() + "," + b.getValue() + "," + weight;
		}
	}
	public static class IGraphWrapper implements IGraph{
		private Graph<Void> g;

		public IGraphWrapper(Graph<Void> g){
			this.g = g;
		}

		public int vertCount() {
			throw new UnsupportedOperationException();
		}

		public int first(int vert) {
			throw new UnsupportedOperationException();
		}

		public int next(int vert, int last) {
			throw new UnsupportedOperationException();
		}

		public void addEdge(int vertex, int neighbor, float weight) {
			g.addEdge(vertex, neighbor, weight);
		}

		public void removeEdge(int vertex, int neighbor) {
			throw new UnsupportedOperationException();
		}

		public boolean isEdge(int vertex, int neighbor) {
			throw new UnsupportedOperationException();
		}

		public Float getEdge(int vertex, int neighbor) {
			Edge<Void> e = g.getEdge(vertex, neighbor);
			if(e == null)return null;
			return (float)e.getWeight();
		}

		public List<Integer> getNeighbors(int vertex) {
			throw new UnsupportedOperationException();
		}

		public int degree(int vertex) {
			throw new UnsupportedOperationException();
		}

	}

	public static class Graph<T> {
		private int lastID = 0;
		private Map<Integer, Node<T>> nodes = new HashMap<>();
		private Set<Edge<T>> edges = new TreeSet<Edge<T>>();

		public int createNode() {
			NodeImpl node = new NodeImpl();
			nodes.put(node.getID(), node);
			return node.getID();
		}

		public int createNode(T value) {
			NodeImpl node = new NodeImpl(value);
			nodes.put(node.getID(), node);
			return node.getID();
		}

		public Node<T> getNode(int id) {
			return nodes.get(id);
		}

		public List<Edge<T>> getNeighbors(int id) {
			return nodes.get(id).getNeighbors();
		}

		public T getValue(int id) {
			return nodes.get(id).getValue();
		}

		public List<Node<T>> getAllNodes() {
			return new ArrayList<Node<T>>(nodes.values());
		}

		public List<Edge<T>> getAllEdges() {
			return new ArrayList<Edge<T>>(edges);
		}

		public int getNodeCount() {
			return nodes.size();
		}

		public int getEdgeCount() {
			return edges.size();
		}

		public Edge<T> getEdge(int aID, int bID) {
			Node<T> a = getNode(aID), b = getNode(bID);
			for (Edge<T> e : edges) {
				if ((e.getA() == a && e.getB() == b)) return e;
			}
			return null;
		}

		public Edge<T> addEdge(int aID, int bID) {
			return addEdge(aID, bID, 1);
		}

		/**
		 * If an edge already exists connecting the two nodes, then changes the weight. Otherwise, creates a connection between the two.
		 */
		public Edge<T> addEdge(int aID, int bID, double weight) {
			Node<T> a = getNode(aID), b = getNode(bID);
			Edge<T> e = getEdge(aID, bID);
			if (e != null) {
				e.setWeight(weight);
			} else {
				e = new Edge<T>(a, b, weight);
				edges.add(e);
				((NodeImpl) a).addNeighbor(b);
			}
			return e;
		}

		public Edge<T> removeEdge(int aID, int bID) {
			Node<T> a = getNode(aID), b = getNode(bID);
			Edge<T> e = getEdge(aID, bID);
			if (e != null) {
				edges.remove(e);
				((NodeImpl) a).removeNeighbor(b);
			}
			return e;
		}

		private class NodeImpl implements Node<T> {
			private int id = lastID++;
			private T value;
			private List<Node<T>> transitionList = new ArrayList<Node<T>>();

			public NodeImpl(T value) {
				this.value = value;
			}

			public NodeImpl() {
				this(null);
			}

			private void addNeighbor(Node<T> neighbor) {
				transitionList.add(neighbor);
			}

			private void removeNeighbor(Node<T> neighbor) {
				transitionList.remove(neighbor);
			}

			@Override
			public T getValue() {
				return value;
			}

			@Override
			public int getID() {
				return id;
			}

			@Override
			public List<Edge<T>> getNeighbors() {
				List<Edge<T>> edges = new ArrayList<Edge<T>>();
				for (Node<T> n : transitionList) {
					Edge<T> temp = getEdge(id, n.getID());
					edges.add(new Edge<T>(this, n, temp.getWeight()));
				}
				return edges;
			}
		}
	}



	static class PrimNode {
		Integer parent;
		int me;
		Float weight;
		boolean betterThan(PrimNode that) {
			if(this.weight == null) return false;
			if(that.weight == null) return true;
			return this.weight < that.weight;
		}
		boolean betterThan(float that) {
			if(this.weight == null) return false;
			return this.weight < that;
		}
		String getMe() {
			return ""+(char)(me+'a');
		}
		String getParent() {
			if(parent == null) return "null";
			return ""+(char)(parent+'a');
		}

		@Override
		public String toString() {
			return String.format("%s(%s - %s)\n",getMe(),getParent(),weight);
		}
	}


	public static void main(String[]args) {
		/*
		IGraph g = new ListGraph();
		/*/
		Graph<Void> tmp = new Graph<>();
		for (int i = 'a'; i <= 'f'; i++) {
			tmp.createNode();
		}
		IGraph g = new IGraphWrapper(tmp);
		//*/


		//*
		g.addEdge('a'-'a','b'-'a',6);
		g.addEdge('b'-'a','a'-'a',6);
		g.addEdge('a'-'a','f'-'a',1);
		g.addEdge('f'-'a','a'-'a',1);
		g.addEdge('a'-'a','c'-'a',9);
		g.addEdge('c'-'a','a'-'a',9);
		g.addEdge('f'-'a','e'-'a',4);
		g.addEdge('e'-'a','f'-'a',4);
		g.addEdge('b'-'a','e'-'a',5);
		g.addEdge('e'-'a','b'-'a',5);
		g.addEdge('e'-'a','c'-'a',7);
		g.addEdge('c'-'a','e'-'a',7);
		g.addEdge('e'-'a','d'-'a',8);
		g.addEdge('d'-'a','e'-'a',8);
		g.addEdge('d'-'a','c'-'a',13);
		g.addEdge('d'-'a','c'-'a',13);

		/*/

		g.addEdge('a'-'a','b'-'a',1);
		g.addEdge('b'-'a','a'-'a',1);
		g.addEdge('a'-'a','c'-'a',3);
		g.addEdge('c'-'a','a'-'a',3);
		g.addEdge('a'-'a','d'-'a',3);
		g.addEdge('d'-'a','a'-'a',3);
		g.addEdge('b'-'a','d'-'a',3);
		g.addEdge('d'-'a','b'-'a',3);
		g.addEdge('c'-'a','d'-'a',3);
		g.addEdge('d'-'a','c'-'a',3);
		g.addEdge('c'-'a','e'-'a',1);
		g.addEdge('e'-'a','c'-'a',1);
		g.addEdge('d'-'a','e'-'a',2);
		g.addEdge('e'-'a','d'-'a',2);
		g.addEdge('d'-'a','f'-'a',2);
		g.addEdge('f'-'a','d'-'a',2);
		g.addEdge('e'-'a','f'-'a',2);
		g.addEdge('f'-'a','e'-'a',2);

		//*/


		//a(null - null)			a(null - null)
		//f(a - 1.0)				f(a - 1.0)
		//e(f - 4.0)				e(f - 4.0)
		//b(e - 5.0)				b(a - 6.0)
		//c(e - 7.0)				c(e - 7.0)
		//d(e - 8.0)				d(e - 8.0)







		List<PrimNode> unprocessed = new LinkedList<>();
		List<PrimNode> processed = new ArrayList<>();

		//init unprocessed to all nodes
		for (int i = 0; i < 6; i++) {
			PrimNode toAdd = new PrimNode();
			toAdd.me = i;
			unprocessed.add(toAdd);
		}
		//do prims
		while(unprocessed.size() > 0) {
			//select node
			PrimNode toUse = null;
			for(PrimNode possible : unprocessed) {
				if(toUse == null || possible.betterThan(toUse))
					toUse = possible;
			}

			//update unprocessed with new pointers
			for(PrimNode possible : unprocessed) {
				Float edge = g.getEdge(toUse.me,possible.me);
				if(edge != null) { // has edge?
					if(!possible.betterThan(edge)) {
						possible.weight = edge;
						possible.parent = toUse.me;
					}
				}
			}

			//move selected node to processed
			unprocessed.remove(toUse);
			processed.add(toUse);
		}


		for(PrimNode n : processed) {
			System.out.printf("%s(%s - %s)\n",n.getMe(),n.getParent(),n.weight);
		}

	}
}
