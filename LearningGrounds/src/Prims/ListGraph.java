package Prims;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Anthony on 3/1/2015.
 */
public class ListGraph implements IGraph {
	class Node {
		class NodeAndWeight {
			public final int node;
			public final float weight;

			public NodeAndWeight(int node, float weight) {
				this.node = node;
				this.weight = weight;
			}
		}
		int myIndex;
		List<NodeAndWeight> neighbors = new ArrayList<>();

		public void addNeighbor(int neighbor, float weight) {
			neighbors.add(new NodeAndWeight(neighbor,weight));
		}
		public void removeNeighbor(int neighbor) {
			for(NodeAndWeight tmp : neighbors) {
				if(tmp.node == neighbor) {
					neighbors.remove(tmp);
					return;
				}
			}
		}

		public Node(int myIndex) {
			this.myIndex = myIndex;
		}
	}
	List<Node> myNodes = new ArrayList<>();

	public int vertCount() {
		return myNodes.size();
	}

	public int first(int vert) {
		return myNodes.get(vert).neighbors.size() > 0 ? myNodes.get(vert).neighbors.get(0).node : -1;
	}

	public int next(int vert, int last) {
		boolean found = false;
		for(Node.NodeAndWeight pair : myNodes.get(vert).neighbors) {
			if(found) return pair.node;
			found = pair.node == last;
		}
		return -1;
	}

	public void addEdge(int vertex, int neighbor, float weight) {
		while(myNodes.size() <= vertex)
			myNodes.add(new Node(myNodes.size()));

		myNodes.get(vertex).addNeighbor(neighbor,weight);
	}

	public void removeEdge(int vertex, int neighbor) {
		myNodes.get(vertex).removeNeighbor(neighbor);
	}

	public boolean isEdge(int vertex, int neighbor) {
		return getEdge(vertex,neighbor) != null;
	}

	public Float getEdge(int vertex, int neighbor) {
		for(Node.NodeAndWeight pair : myNodes.get(vertex).neighbors) {
			if(pair.node == neighbor) return pair.weight;
		}
		return null;
	}

	public int degree(int vertex) {
		return myNodes.get(vertex).neighbors.size();
	}
}

class BiDirectionalListGraph extends ListGraph {

	@Override
	public void addEdge(int vertex, int neighbor, float weight) {
		super.addEdge(vertex,neighbor,weight);
		super.addEdge(neighbor,vertex,weight);
	}
	@Override
	public void removeEdge(int vertex, int neighbor) {
		super.removeEdge(vertex, neighbor);
		super.removeEdge(neighbor, vertex);
	}
}
