package Prims;

import java.util.ArrayList;
import java.util.List;

public interface IGraph {
	//public int vertCount();

	//int first(int vert);
	//int next(int vert, int last);
	void addEdge(int vertex, int neighbor, float weight);
	//void removeEdge(int vertex, int neighbor);
	//boolean isEdge(int vertex, int neighbor);
	Float getEdge(int vertex, int neighbor);
	//default List<Integer> getNeighbors(int vertex) {
	//	List<Integer> ret = new ArrayList<>(vertCount());
	//	for(int i=first(vertex);i<vertCount();i = next(vertex,i)) {
	//		ret.add(i);
	//	}
	//	return ret;
	//}
	//num neighbors
	//int degree(int vertex);
}
