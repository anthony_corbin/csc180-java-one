package Prims;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anthony on 3/1/2015.
 */
public class MatrixGraph implements IGraph {
	private Float[][]matrix; // for tracking edges
	private int[]marks; // for tracking verts

	public MatrixGraph(int n) {
		matrix = new Float[n][n];
		marks = new int[n];
	}

	public int vertCount() {
		return marks.length;
	}

	//runs in theta(V)
	public int first(int vert){
		return next(vert, -1);
	}
	//runs in theta(V)
	public int next(int vert,int last) {
		for(int i=last+1;i<vertCount();i++) {
			if(matrix[vert][i] != 0) return i;
		}
		return vertCount();
	}
	public void addEdge(int vertex, int neighbor, float weight) {
		//if adding weight 0 complain
		if(weight == 0) throw new IllegalArgumentException("Weight cannot be 0");
		matrix[vertex][neighbor] = weight;
	}
	public void removeEdge(int vertex, int neighbor) {
		matrix[vertex][neighbor] = null;
	}
	public boolean isEdge(int vertex, int neighbor) {
		return matrix[vertex][neighbor] != 0;
	}
	public Float getEdge(int vertex, int neighbor) {
		return matrix[vertex][neighbor];
	}
	public List<Integer> getNeighbors(int vertex) {
		List<Integer> ret = new ArrayList<Integer>(vertCount());
		for(int i=first(vertex);i<vertCount();i = next(vertex,i)) {
			ret.add(i);
		}
		return ret;
	}
	public int degree(int vertex) {
		int ret = 0;
		for(int i=first(vertex);i<vertCount();i = next(vertex,i)) {
			ret++;
		}
		return ret;
	}
	public int getMark(int vertex) {
		return marks[vertex];
	}
	public void setMark(int vertex, int mark) {
		marks[vertex] = mark;
	}
	public void setAllMarks(int mark) {
		for(int i=0;i<marks.length;i++) {
			marks[i] = mark;
		}
	}
}

class BiDirectionalMatrixGraph extends MatrixGraph {
	public BiDirectionalMatrixGraph(int n) { super(n); }

	@Override
	public void addEdge(int vertex, int neighbor, float weight) {
		super.addEdge(vertex,neighbor,weight);
		super.addEdge(neighbor,vertex,weight);
	}
	@Override
	public void removeEdge(int vertex, int neighbor) {
		super.removeEdge(vertex, neighbor);
		super.removeEdge(neighbor, vertex);
	}
}
