package edu.neumont.acorbin.nubay.utils.FunctionInterfaces;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ActionEventsTest {
	@Test
	public void testSimpleEvent() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent simpleEvent = new ActionEvents.ActionEvent();

		simpleEvent.Subscribe(() -> hitCount[0]++);
		simpleEvent.Subscribe(() -> hitCount[0]++);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke();
		Assert.assertEquals(2, hitCount[0]);
	}
	@Test
	public void testEvent1() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent1<Integer> simpleEvent = new ActionEvents.ActionEvent1<>();

		simpleEvent.Subscribe((a) -> {
			hitCount[0]+= a;
		});
		simpleEvent.Subscribe((a) -> {
			hitCount[0]+= a;
		});

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2);
		Assert.assertEquals(4, hitCount[0]);
	}
	@Test
	public void testEvent2() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent2<Integer,Integer> simpleEvent = new ActionEvents.ActionEvent2<>();

		simpleEvent.Subscribe((a,b) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b) -> hitCount[0]+= a);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2);
		Assert.assertEquals(2 * 2, hitCount[0]);
	}
	@Test
	public void testEvent3() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent3<Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent3<>();

		simpleEvent.Subscribe((a,b,c) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c) -> hitCount[0]+= c);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2);
		Assert.assertEquals(2 * 3, hitCount[0]);
	}
	@Test
	public void testEvent4() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent4<Integer,Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent4<>();

		simpleEvent.Subscribe((a,b,c,d) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c,d) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c,d) -> hitCount[0]+= c);
		simpleEvent.Subscribe((a,b,c,d) -> hitCount[0]+= d);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2, 2);
		Assert.assertEquals(2 * 4, hitCount[0]);
	}
	@Test
	public void testEvent5() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent5<Integer,Integer,Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent5<>();

		simpleEvent.Subscribe((a,b,c,d,e) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c,d,e) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c,d,e) -> hitCount[0]+= c);
		simpleEvent.Subscribe((a,b,c,d,e) -> hitCount[0]+= d);
		simpleEvent.Subscribe((a,b,c,d,e) -> hitCount[0]+= e);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2, 2, 2);
		Assert.assertEquals(2 * 5, hitCount[0]);
	}
	@Test
	public void testEvent6() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent6<Integer,Integer,Integer,Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent6<>();

		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= c);
		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= d);
		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= e);
		simpleEvent.Subscribe((a,b,c,d,e,f) -> hitCount[0]+= f);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2, 2, 2, 2);
		Assert.assertEquals(2 * 6, hitCount[0]);
	}
	@Test
	public void testEvent7() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent7<Integer,Integer,Integer,Integer,Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent7<>();

		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= c);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= d);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= e);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= f);
		simpleEvent.Subscribe((a,b,c,d,e,f,g) -> hitCount[0]+= g);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2, 2, 2, 2, 2);
		Assert.assertEquals(2 * 7, hitCount[0]);
	}
	@Test
	public void testEvent8() {
		final int[] hitCount = {0};
		ActionEvents.ActionEvent8<Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer> simpleEvent = new ActionEvents.ActionEvent8<>();

		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= a);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= b);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= c);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= d);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= e);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= f);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= g);
		simpleEvent.Subscribe((a,b,c,d,e,f,g,h) -> hitCount[0]+= h);

		Assert.assertEquals(0, hitCount[0]);
		simpleEvent.Invoke(2, 2, 2, 2, 2, 2, 2, 2);
		Assert.assertEquals(2 * 8, hitCount[0]);
	}
}