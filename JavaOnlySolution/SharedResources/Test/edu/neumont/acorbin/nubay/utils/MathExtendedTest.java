package edu.neumont.acorbin.nubay.utils;

import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MathExtendedTest {

	@Test
	public void testClamp1() throws Exception {
		Assert.assertEquals(  0, MathExtended.Clamp(-5, 0, 10));
		Assert.assertEquals( 10, MathExtended.Clamp(50, 0, 10));
		Assert.assertEquals(  3, MathExtended.Clamp( 3, 0, 10));
	}

	@Test
	public void testClamp2() throws Exception {
		Assert.assertEquals(  0L, MathExtended.Clamp(-5L, 0L, 10L));
		Assert.assertEquals( 10L, MathExtended.Clamp(50L, 0L, 10L));
		Assert.assertEquals(  3L, MathExtended.Clamp( 3L, 0L, 10L));
	}

	@Test
	public void testClamp3() throws Exception {
		Assert.assertEquals(  0.0, MathExtended.Clamp(-5.0, 0.0, 10.0),0);
		Assert.assertEquals( 10.0, MathExtended.Clamp(50.0, 0.0, 10.0),0);
		Assert.assertEquals(  3.0, MathExtended.Clamp( 3.0, 0.0, 10.0),0);
	}

	@Test
	public void testClamp() throws Exception {
		Assert.assertEquals(  0.0f, MathExtended.Clamp(-5.0f, 0.0f, 10.0f),0);
		Assert.assertEquals( 10.0f, MathExtended.Clamp(50.0f, 0.0f, 10.0f),0);
		Assert.assertEquals(  3.0f, MathExtended.Clamp( 3.0f, 0.0f, 10.0f),0);
	}
}