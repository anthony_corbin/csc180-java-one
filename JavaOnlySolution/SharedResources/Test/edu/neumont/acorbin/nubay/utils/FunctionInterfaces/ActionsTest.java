package edu.neumont.acorbin.nubay.utils.FunctionInterfaces;

import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ActionsTest {
	@Test
	public void testAction() {
		final int[] num = {0};

		Actions.Action fun = () -> num[0] = 2;

		Assert.assertEquals(0, num[0]);
		fun.Invoke();
		Assert.assertEquals(2, num[0]);
	}

}