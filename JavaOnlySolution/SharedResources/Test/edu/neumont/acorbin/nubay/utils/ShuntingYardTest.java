package edu.neumont.acorbin.nubay.utils;

import edu.neumont.acorbin.nubay.utils.Predicates.AndPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.NotPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.OrPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.util.*;

public class ShuntingYardTest extends TestCase {



    //region test exceptions
    public void testTooManyOperators() {
        try {
            MakeMathParser().infixToRPN("1++1"); // too many operators
        } catch(IllegalArgumentException e) {
            Assert.fail("Parser shouldn't throw exception");
        }

        try {
            MakeEvalInstance().Eval(MakeMathParser().infixToRPN("1++1")); // too many operators
            Assert.fail("should have thrown exception");
        } catch(IllegalArgumentException e) { }
    }

    public void testTooManyParentheses() {
        try {
            MakeMathParser().infixToRPN("(1+1"); // extra '('
            Assert.fail("should have thrown exception");
        } catch(IllegalArgumentException e) { }

        try {
            MakeMathParser().infixToRPN("1+1)"); // extra ')'
            Assert.fail("should have thrown exception");
        } catch(IllegalArgumentException e) { }
    }
    //endregion


    //region Basic Tests

    // Math
    public void testParseAndEval() {
        testParseAndEval("(1+2)*(3/4)^(5+6)", 0.1267054080963134765625);
        testParseAndEval("5+((1+2)*4)-3", 14);
        testParseAndEval("(4+2*5)/(1+3*2)", 2);
        testParseAndEval("(2*5+4)/(3*2+1)", 2);
        testParseAndEval("2^2",4);
        testParseAndEval("5+2^3", 13);
        testParseAndEval("[5+8*sin(2*15)]/[2+tan(45)]", 3);
        testParseAndEval("[sin(2*15)*8+5]/[tan(45)+2]", 3);
    }
    public void testParseAndEval(String infix, double result) {
        List<String> RPN = MakeMathParser().infixToRPN(infix);
        double ret = MakeEvalInstance().Eval(RPN);
        Assert.assertEquals(result, ret, .0001);
    }


    //Boolean
    public void testParseBoolAndEval() {
        testParseBoolAndEval(false,"a", "b");
        testParseBoolAndEval(true, "a", "a");

        testParseBoolAndEval(false,"a and b", "a");
        testParseBoolAndEval(false,"a and b", "b");
        testParseBoolAndEval(false,"a and b", "d");
        testParseBoolAndEval(true, "a and b", "abc");

        testParseBoolAndEval(false,"a or b", "c");
        testParseBoolAndEval(true, "a or b", "a");
        testParseBoolAndEval(true, "a or b", "b");
        testParseBoolAndEval(true, "a or b", "ab");

        testParseBoolAndEval(true, "a and not b", "a");
        testParseBoolAndEval(false,"a and not b", "b");
        testParseBoolAndEval(true, "a and not b", "ac");

        testParseBoolAndEval(true, "not b", "a");
        testParseBoolAndEval(false,"not b", "b");

        testParseBoolAndEval(true, "a and b or c and d", "abcd");
        testParseBoolAndEval(true, "a and b or c and d", "ab");
        testParseBoolAndEval(true, "a and b or c and d", "cd");
        testParseBoolAndEval(false,"a and b or c and d", "ac");
        testParseBoolAndEval(false,"a and b or c and d", "ad");
        testParseBoolAndEval(false,"a and b or c and d", "bc");
        testParseBoolAndEval(false,"a and b or c and d", "bd");
        testParseBoolAndEval(false,"a and b or c and d", "wut");

        testParseBoolAndEval(true, "(a and b) or (c and d)", "abcd");
        testParseBoolAndEval(true, "(a and b) or (c and d)", "ab");
        testParseBoolAndEval(true, "(a and b) or (c and d)", "cd");
        testParseBoolAndEval(false,"(a and b) or (c and d)", "ac");
        testParseBoolAndEval(false,"(a and b) or (c and d)", "ad");
        testParseBoolAndEval(false,"(a and b) or (c and d)", "bc");
        testParseBoolAndEval(false,"(a and b) or (c and d)", "bd");
        testParseBoolAndEval(false,"(a and b) or (c and d)", "wut");

        testParseBoolAndEval(true, "a and (b or c) and d", "abcd");
        testParseBoolAndEval(true, "a and (b or c) and d", "abd");
        testParseBoolAndEval(true, "a and (b or c) and d", "acd");
        testParseBoolAndEval(false,"a and (b or c) and d", "ad");
        testParseBoolAndEval(false,"a and (b or c) and d", "bc");
        testParseBoolAndEval(false,"a and (b or c) and d", "abc");
        testParseBoolAndEval(false,"a and (b or c) and d", "bcd");
        testParseBoolAndEval(false,"a and (b or c) and d", "wut");

        testParseBoolAndEval(false,"a and (b or c)", "a");
        testParseBoolAndEval(false,"a and (b or c)", "b");
        testParseBoolAndEval(false,"a and (b or c)", "bc");
        testParseBoolAndEval(true, "a and (b or c)", "abc");
        testParseBoolAndEval(true, "a and (b or c)", "ac");
        testParseBoolAndEval(true, "a and (b or c)", "ab");
        testParseBoolAndEval(false,"a and (b or c)", "wut");

        testParseBoolAndEval(true, "a and b or c", "abc");
        testParseBoolAndEval(true, "a and b or c", "ab");
        testParseBoolAndEval(true, "a and b or c", "c");
        testParseBoolAndEval(true, "a and b or c", "ac");
    }
    public void testParseBoolAndEval(boolean result, String query,String input) {
        List<String> RPN = MakeBoolParser().infixToRPN(query,false);
        Predicate<String> condition = MakePredicateEval().Eval(RPN);
        boolean ret = condition.Invoke(input);
        Assert.assertEquals(result, ret);
    }


    //endregion

    //region testing Constructor and method overloads
    public void testAltConstructor() {
        Map<String, ShuntingYard.OperatorDetails> OPERATORS = new HashMap<>();
        OPERATORS.put("+", new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("-", new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("*", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("/", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("%", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("^", new ShuntingYard.OperatorDetails(10, ShuntingYard.Associativity.RIGHT));
        OPERATORS.put("sin", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("cos", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("tan", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );

        ShuntingYard parser = new ShuntingYard(OPERATORS,new HashSet<>(Arrays.asList("( (".split(" "))),new HashSet<>(Arrays.asList(") )".split(" "))));

        Assert.assertEquals(9, MakeEvalInstance().Eval(parser.infixToRPN("(1+2)*3")),.0001);
        Assert.assertEquals(9, MakeEvalInstance().Eval(ShuntingYard.infixToRPN("(1+2)*3",OPERATORS)),.0001);
        Assert.assertEquals(9, MakeEvalInstance().Eval(ShuntingYard.infixToRPN(Arrays.asList("( 1 + 2 ) * 3".split(" ")),OPERATORS)),.0001);
    }
    //endregion

    //region Parse/Eval Makers
    private ReversePolish<String,Predicate<String>> MakePredicateEval() {
        HashMap<String, ReversePolish.PolishOperator<Predicate<String>>> operators = new HashMap<>();
        operators.put("and", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new AndPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("or", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new OrPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("not", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 1; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new NotPredicate<>(data.pop()); }
        });



        return new ReversePolish<>(new ReversePolish.PolishValue<String, Predicate<String>>() {
            @Override
            public Predicate<String> eval(final String a) { // from search query
                return new Predicate<String>() {
                    @Override
                    public boolean Invoke(String val) {
                        return val.toLowerCase().contains(a.toLowerCase());
                    }
                };
            }
        }, operators);
    }
    private ShuntingYard MakeBoolParser() {
        Map<String, ShuntingYard.OperatorDetails> OPERATORS = new HashMap<>();
        OPERATORS.put("or",  new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("and", new ShuntingYard.OperatorDetails(1,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("not", new ShuntingYard.OperatorDetails(2,  ShuntingYard.Associativity.LEFT) );

        return new ShuntingYard(OPERATORS);
    }

    private ReversePolish<String, Double> MakeEvalInstance() {
        HashMap<String, ReversePolish.PolishOperator<Double>> operators = new HashMap<>();
        operators.put("+", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() + data.pop(); }
        });
        operators.put("*", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() * data.pop(); }
        });
        operators.put("-", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() - data.pop(); }
        });
        operators.put("/", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() / data.pop(); }
        });
        operators.put("^", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return Math.pow(data.pop(),data.pop()); }
        });
        //single operations
        operators.put("sin", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.sin(Math.toRadians(data.pop())); }
        });
        operators.put("cos", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.cos(Math.toRadians(data.pop())); }
        });
        operators.put("tan", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.tan(Math.toRadians(data.pop())); }
        });
        operators.put("root", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.sqrt(data.pop()); }
        });
        //constants
        operators.put("e", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 0; }
            @Override public Double eval(Stack<Double> data) { return Math.E; }
        });



        return new ReversePolish<String, Double>(new ReversePolish.PolishValue<String, Double>() {
            @Override
            public Double eval(String a) {
                double t = Double.parseDouble(a);
                return new Double(t);
            }
        },operators);
    }
    private ShuntingYard MakeMathParser() {
        Map<String, ShuntingYard.OperatorDetails> OPERATORS = new HashMap<>();
        OPERATORS.put("+", new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("-", new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("*", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("/", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("%", new ShuntingYard.OperatorDetails(5,  ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("^", new ShuntingYard.OperatorDetails(10, ShuntingYard.Associativity.RIGHT));
        OPERATORS.put("sin", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("cos", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );
        OPERATORS.put("tan", new ShuntingYard.OperatorDetails(15, ShuntingYard.Associativity.LEFT) );

        return new ShuntingYard(OPERATORS);
    }
    //endregion
}