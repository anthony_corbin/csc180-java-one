package edu.neumont.acorbin.nubay.utils;

import edu.neumont.acorbin.nubay.utils.Predicates.AndPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.NotPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.OrPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class ReversePolishTest extends TestCase {

    public void testStaticEval() throws Exception {
        testMakingClassEval("5 1 2 + 4 * + 3 - ",   14); // 5+((1+2)*4)-3
        testMakingClassEval("1 2 + 4 * 5 + 3 - ",   14); // 5+((1+2)*4)-3
        testMakingClassEval("2 5 * 4 + 3 2 * 1 + /", 2); // (4 + 2 * 5) / (1 + 3 * 2)
        testMakingClassEval("4 2 5 * + 1 3 2 * + /", 2); // (2 * 5 + 4 )/(3 * 2 + 1)
        testMakingClassEval("2 2 ^ ",                4) ; // 2^2
        testMakingClassEval("9 5 3 + 2 4 ^ - +",     1);
        testMakingClassEval("5 2 3 ^ +" ,           13); // 5 + 2 ^3
    }
    private ReversePolish<String, Double> MakeEvalInstance() {
        HashMap<String, ReversePolish.PolishOperator<Double>> operators = new HashMap<>();
        operators.put("+", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() + data.pop(); }
        });
        operators.put("*", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() * data.pop(); }
        });
        operators.put("-", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() - data.pop(); }
        });
        operators.put("/", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return data.pop() / data.pop(); }
        });
        operators.put("^", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 2; }
            @Override public Double eval(Stack<Double> data) { return Math.pow(data.pop(),data.pop()); }
        });
        //single operations
        operators.put("sin", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.sin(Math.toRadians(data.pop())); }
        });
        operators.put("cos", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.cos(Math.toRadians(data.pop())); }
        });
        operators.put("tan", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.tan(Math.toRadians(data.pop())); }
        });
        operators.put("root", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 1; }
            @Override public Double eval(Stack<Double> data) { return Math.sqrt(data.pop()); }
        });
        //constants
        operators.put("e", new ReversePolish.PolishOperator<Double>() {
            @Override public int getOpCount() { return 0; }
            @Override public Double eval(Stack<Double> data) { return Math.E; }
        });



        return new ReversePolish<String, Double>(new ReversePolish.PolishValue<String, Double>() {
            @Override
            public Double eval(String a) {
                double t = Double.parseDouble(a);
                return new Double(t);
            }
        },operators);
    }
    private void testMakingClassEval(String input,double result) throws Exception {
        List<String> inputs = Arrays.asList(input.split(" "));

        double ret = MakeEvalInstance().Eval(inputs);
        Assert.assertEquals(result,ret,.0001);
    }

    public void testPredicateVersion() throws Exception {
        List<String> data = Arrays.asList("a b and c d and or".split(" ")); // a and b or c and d
        Predicate<String> hasFandJ = MakePredicateEval().Eval(data);

        Assert.assertTrue(hasFandJ.Invoke("ab"));
        Assert.assertTrue(hasFandJ.Invoke("cd"));
        Assert.assertTrue(hasFandJ.Invoke("abcd"));
        Assert.assertFalse(hasFandJ.Invoke("ad"));
        Assert.assertFalse(hasFandJ.Invoke("ac"));
        Assert.assertFalse(hasFandJ.Invoke("bd"));
        Assert.assertFalse(hasFandJ.Invoke("wut"));
    }
    private ReversePolish<String,Predicate<String>> MakePredicateEval() {
        HashMap<String, ReversePolish.PolishOperator<Predicate<String>>> operators = new HashMap<>();
        operators.put("and", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new AndPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("or", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 2; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new OrPredicate<>(data.pop(), data.pop()); }
        });
        operators.put("not", new ReversePolish.PolishOperator<Predicate<String>>() {
            @Override public int getOpCount() { return 1; }
            @Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new NotPredicate<>(data.pop()); }
        });



        return new ReversePolish<>(new ReversePolish.PolishValue<String, Predicate<String>>() {
            @Override
            public Predicate<String> eval(final String a) { // from search query
                return new Predicate<String>() {
                    @Override
                    public boolean Invoke(String val) {
                        return val.toLowerCase().contains(a.toLowerCase());
                    }
                };
            }
        }, operators);
    }

    public void testExceptions() throws Exception {
        ReversePolish<String, Double> mather = MakeEvalInstance();

        try{
            mather.Eval(null);
            Assert.fail("should have thrown exception for null argument");
        } catch (IllegalArgumentException e) { }

        try{
            mather.Eval(Arrays.asList("1 2".split(" ")));
            Assert.fail("should have thrown exception for not enough operators");
        } catch (IllegalArgumentException e) { }

        try{
            mather.Eval(Arrays.asList("1 2 + +".split(" ")));
            Assert.fail("should have thrown exception for too many operators");
        } catch (IllegalArgumentException e) { }

    }
}