package edu.neumont.acorbin.nubay.utils;

import junit.framework.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompareTest {

	@Test
	public void testCompare() {
		Assert.assertEquals(false,Compare.Compare(true,  false));
		Assert.assertEquals(true, Compare.Compare(true,  true ));
		Assert.assertEquals(true, Compare.Compare(false, false));

		Assert.assertEquals(false,Compare.Compare('a','b'));
		Assert.assertEquals(false,Compare.Compare('b','a'));
		Assert.assertEquals(true, Compare.Compare('a','a'));
		Assert.assertEquals(true, Compare.Compare('b','b'));

		Assert.assertEquals(false,Compare.Compare((byte) 1,(byte)10));
		Assert.assertEquals(false,Compare.Compare((byte)10,(byte) 1));
		Assert.assertEquals(true, Compare.Compare((byte) 1,(byte) 1));
		Assert.assertEquals(true, Compare.Compare((byte)10,(byte)10));

		Assert.assertEquals(false,Compare.Compare((short) 1,(short)10));
		Assert.assertEquals(false,Compare.Compare((short)10,(short) 1));
		Assert.assertEquals(true, Compare.Compare((short) 1,(short) 1));
		Assert.assertEquals(true, Compare.Compare((short)10,(short)10));

		Assert.assertEquals(false,Compare.Compare( 1,10));
		Assert.assertEquals(false,Compare.Compare(10, 1));
		Assert.assertEquals(true, Compare.Compare( 1, 1));
		Assert.assertEquals(true, Compare.Compare(10,10));

		Assert.assertEquals(false,Compare.Compare( 1L,10L));
		Assert.assertEquals(false,Compare.Compare(10L, 1L));
		Assert.assertEquals(true, Compare.Compare( 1L, 1L));
		Assert.assertEquals(true, Compare.Compare(10L,10L));

		Assert.assertEquals(false,Compare.Compare( 1.0,10.0));
		Assert.assertEquals(false,Compare.Compare(10.0, 1.0));
		Assert.assertEquals(true, Compare.Compare( 1.0, 1.0));
		Assert.assertEquals(true, Compare.Compare(10.0,10.0));

		Assert.assertEquals(false,Compare.Compare( 1.0f,10.0f));
		Assert.assertEquals(false,Compare.Compare(10.0f, 1.0f));
		Assert.assertEquals(true, Compare.Compare( 1.0f, 1.0f));
		Assert.assertEquals(true, Compare.Compare(10.0f,10.0f));

		Assert.assertEquals(false,Compare.Compare( 1.0f,10.0f));
		Assert.assertEquals(false,Compare.Compare(10.0f, 1.0f));
		Assert.assertEquals(true, Compare.Compare( 1.0f, 1.0f));
		Assert.assertEquals(true, Compare.Compare(10.0f,10.0f));
	}

}