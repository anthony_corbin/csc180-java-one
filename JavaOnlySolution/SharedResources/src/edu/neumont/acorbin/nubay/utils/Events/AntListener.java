package edu.neumont.acorbin.nubay.utils.Events;

/**
 * Created by Anthony on 1/28/2015.
 */
public interface AntListener<T> {
    void OnAntChange(T obj);
}
