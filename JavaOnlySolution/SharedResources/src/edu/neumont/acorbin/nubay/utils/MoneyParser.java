package edu.neumont.acorbin.nubay.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.regex.Pattern;

/**
 * Created by Anthony on 1/29/2015.
 */
public class MoneyParser {
	static private MoneyParser CodeCoverageHack = new MoneyParser();
    private MoneyParser(){} // no one should create a static class
	public static String moneyPatternRegex = "\\$?(?:(?:(?:\\d+,?)*\\d+.?\\d+)|(?:.?\\d+))";
    private static Pattern moneyPattern = Pattern.compile(moneyPatternRegex);
    public  static BigDecimal parse(String toParse) {
        if(toParse == null || toParse.equals("")) toParse = "$.01";
        if(moneyPattern.matcher(toParse).find()) {
            toParse = toParse.replaceAll("[,$ ]", "");
            return new BigDecimal(toParse);
        } else {
            throw new NumberFormatException(toParse + " is Invalid Money Format");
        }
    }
    //private static String format(String amount) {
    //	String ret = amount;
    //	int periodIndex = amount.indexOf('.');
    //	if(periodIndex >= 0) {
    //		String frontPart = amount.substring(0, periodIndex);
    //		String Decimals = amount.substring(periodIndex, amount.length()).substring(0, 3);
    //		ret =  frontPart + Decimals;
    //	} else {
    //		ret = "0."+ret;
    //	}
    //	return "$" + ret;
    //}
    public  static String format(BigDecimal toFormat) {
        //return format(toFormat.toString());
        return format(toFormat.doubleValue());
    }
    public  static String format(double toFormat) {
        //return format(""+toFormat);
        return NumberFormat.getCurrencyInstance().format(toFormat);
    }
	/* Double Parser from C++
bool EngineParser::tryParseDouble(std::string& src, double& out)
	if(src.size() ==0 ) return false;
	double ret = 0;
	double power = 1;
	bool afterDot = false;

	bool negitive = src[0] == '-';

	unsigned int i = negitive ? 1 : 0;
	for (; i < src.size(); i++) {
		if('0' <= src[i] && src[i] <= '9') {
			if(!afterDot) {
				ret *= 10; // move numbers to the left as new numbers are found
				ret += src[i] - '0';
			} else {
				power /= 10.0;
				double tmp = (src[i] - '0');
				ret += tmp * power;
			}
		} else {
			if(!afterDot && src[i] == '.') {
				afterDot = true;
			} else {
				return false;
			}
		}
	}
	out = ret * (negitive ? -1 : 1);
	return true;
	//*/
}
