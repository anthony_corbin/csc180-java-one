package edu.neumont.acorbin.nubay.utils.Events;

import java.util.ArrayList;

/**
 * Created by Anthony on 1/14/2015.
 */
public class SimpleObservable<T> implements AntObservable<T> {
    ArrayList<AntListener<T>> listeners = new ArrayList<>();
    public synchronized void Subscribe(AntListener<T> toAdd) {
        listeners.add(toAdd);
    }
    public synchronized void UnSubscribe(AntListener<T> toRemove) {
        listeners.remove(toRemove);
    }
    public synchronized void NotifyObservers(T obj) {
        for (AntListener<T> l : listeners) {
            l.OnAntChange(obj);
        }
    }
}
