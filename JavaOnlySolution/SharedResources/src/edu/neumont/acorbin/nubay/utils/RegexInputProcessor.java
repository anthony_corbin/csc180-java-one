package edu.neumont.acorbin.nubay.utils;

import edu.neumont.acorbin.nubay.utils.FunctionInterfaces.Functions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anthony on 3/11/2015.
 * Validates converter based off regex and transforms into return type
 */
public class RegexInputProcessor<T> {
	public static class Command<T> {
		private Pattern acceptedInputRegex;
		private Functions.Function2<T, String, Matcher> executeFunction;
		public Command(Pattern acceptedInputRegex, Functions.Function2<T, String, Matcher> execute) {
			this.acceptedInputRegex = acceptedInputRegex;
			this.executeFunction = execute;
		}
		public boolean matches(String s) {
			return acceptedInputRegex.matcher(s).matches();
		}
		public T Execute(String input) {
			return executeFunction.Invoke(input,acceptedInputRegex.matcher(input));
		}
	}


	private Collection<Command<T>> commands;

	public RegexInputProcessor(Command<T> ... commands) {
		this.commands = new ArrayList<>();
		Collections.addAll(this.commands, commands);
	}

	public RegexInputProcessor(Collection<Command<T>> commands) {
		this.commands = commands;
	}

	public T processInput(String input) {
		input = input.toLowerCase();
		for(Command<T> command : commands) {
			if (command.matches(input)) {
				return command.Execute(input);
			}
		}
		return null;
	}
}
