package edu.neumont.acorbin.nubay.utils.Threading;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anthony on 3/12/2015.
 */
public class LocksBasedOffID {
	Map<Long,Thread> locks = new HashMap<>();
	public synchronized boolean TryApplyLock(Long idToLock) {
		if(locks.containsKey(idToLock)) {
			return false;
		}
		locks.put(idToLock, Thread.currentThread());
		return true;
	}
	public synchronized boolean ReleaseLock(Long idToRelease) {
		if(locks.containsKey(idToRelease)) {
			if(locks.get(idToRelease) != Thread.currentThread()) {
				throw new IllegalStateException("Trying to unlock from different thread");
			}
			locks.remove(idToRelease);
			return true;
		}
		return false;
	}
}
