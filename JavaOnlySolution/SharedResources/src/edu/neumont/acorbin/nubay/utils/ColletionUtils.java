package edu.neumont.acorbin.nubay.utils;

import java.util.*;

import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;

/**
 * Created by Anthony on 1/20/2015.
 */
public class ColletionUtils {
    public interface Selector<IN,OUT> {
        OUT Select(IN obj);
    }
    public static <T> List<T> filter(List<T> elements, Predicate<T> theTest) {
        return filter(elements,theTest, obj -> obj);
    }
    public static <DATA,PREDICATE> List<DATA> filter(List<DATA> elements, Predicate<PREDICATE> theTest, Selector<DATA,PREDICATE> selector) {
        List<DATA> filtered = new ArrayList<>();
        for(DATA element : elements)
        {
            if(theTest.Invoke(selector.Select(element)))
            {
                filtered.add(element);
            }
        }
        return filtered;
    }
    public static <T> Set<T> filter(Collection<T> elements, Predicate<T> theTest) {
        return filter(elements,theTest, obj -> obj);
    }
    public static <DATA,PREDICATE> Set<DATA> filter(Collection<DATA> elements, Predicate<PREDICATE> theTest, Selector<DATA,PREDICATE> selector) {
        Set<DATA> filtered = new HashSet<>();
        for(DATA element : elements)
        {
            if(theTest.Invoke(selector.Select(element)))
            {
                filtered.add(element);
            }
        }
        return filtered;
    }
	public static <T> int Count(Iterable<T> collection) {
		int ret = 0;
		for(T ignored : collection) ret++;
		return ret;
	}
	public static <T> boolean isEmpty(Iterable<T> collection) {
		for(T ignored : collection) return false;
		return true;
	}
	public static <T> T FirstOrDefault(Iterable<T> collection) {
		for(T t : collection) return t;
		return null;
	}
	public static <T> List<T> ToList(Iterable<T> collection) {
		List<T> ret = new ArrayList<>();
		for(T t : collection) ret.add(t);
		return ret;
	}
}
