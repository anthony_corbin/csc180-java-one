package edu.neumont.acorbin.nubay.utils;

/**
 * Created by Anthony on 2/5/2015.
 */
public class Tuple<T1,T2> {
    public T1 First;
    public T2 Second;

    public Tuple(T1 first, T2 second) {
        First = first;
        Second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple tuple = (Tuple) o;

        if (First != null ? !First.equals(tuple.First) : tuple.First != null) return false;
        if (Second != null ? !Second.equals(tuple.Second) : tuple.Second != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = First != null ? First.hashCode() : 0;
        result = 31 * result + (Second != null ? Second.hashCode() : 0);
        return result;
    }
}
