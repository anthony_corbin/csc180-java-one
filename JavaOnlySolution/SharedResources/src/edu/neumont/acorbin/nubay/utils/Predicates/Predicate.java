package edu.neumont.acorbin.nubay.utils.Predicates;

import edu.neumont.acorbin.nubay.utils.FunctionInterfaces.Functions;

/**
 * Created by Anthony on 1/22/2015.
 */
public interface Predicate<T> {
    boolean Invoke(T val);
}
