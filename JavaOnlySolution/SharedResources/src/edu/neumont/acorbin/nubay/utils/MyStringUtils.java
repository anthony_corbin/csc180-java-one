package edu.neumont.acorbin.nubay.utils;

import java.util.HashMap;

/**
 * Created by Anthony on 1/13/2015.
 *
 */
public class MyStringUtils {
	private MyStringUtils() {} // no one should be calling this
    private static final HashMap<Character, String> exceptedChars = new HashMap<Character, String>(){
        {
            put('\n', "\\n");
            put('\r', "\\r");
            put('"' , "\\\"");
            put('\\', "\\\\");
        }
    };
    public static String javaStringLiteral(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<str.length(); i++)
        {
            char c = str.charAt(i);
            if(exceptedChars.containsKey(c)) {
                sb.append(exceptedChars.get(c));
            } else if (c < 0x20) {
                sb.append(String.format("\\%03o", (int)c));
            } else if (c >= 0x80) {
                sb.append(String.format("\\u%04x", (int)c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
