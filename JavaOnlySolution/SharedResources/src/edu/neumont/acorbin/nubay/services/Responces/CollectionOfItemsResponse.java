package edu.neumont.acorbin.nubay.services.Responces;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ServerResponse;

import java.util.Set;

/**
 * Created by Anthony on 2/28/2015.
 */
public class CollectionOfItemsResponse extends ServerResponse {
	private Set<ItemDetailModel> items;

	public CollectionOfItemsResponse(int responseCode, Set<ItemDetailModel> items, String additionalInfo) {
		super(responseCode, additionalInfo);
		this.items = items;
	}

	public Set<ItemDetailModel> getItems() {
		return items;
	}
}
