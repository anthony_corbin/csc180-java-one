package edu.neumont.acorbin.nubay.services;

import java.io.Serializable;

/**
 * Created by Anthony on 2/28/2015.
 */
public class ServerResponse implements Serializable {
	public static final int SUCCESS = 1;
	public static final int MODEL_DOES_NOT_EXIST = 2;
	public static final int CLIENT_DATA_INVALID  = 3;
	public static final int SERVER_ENCOUNTERED_ERROR = 4;

	private int responseCode;
	private String additionalInfo;

	public ServerResponse(int responseCode, String additionalInfo) {
		this.responseCode = responseCode;
		this.additionalInfo = additionalInfo;
	}
	public int getResponseCode() {
		return responseCode;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}
}
