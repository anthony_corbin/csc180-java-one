package edu.neumont.acorbin.nubay.services.Communicatons;

import edu.neumont.acorbin.nubay.services.ServerCommunication;

/**
 * Created by Anthony on 2/28/2015.
 */
public class GetItemCommunitaion extends ServerCommunication {
	public static final int GET_RANDOM_ITEM         = 4;
	public static final int GET_RANDOM_ITEM_VALID   = 5;
	public static final int GET_RANDOM_ITEM_INVALID = 6;

	private int actionCode;

	public GetItemCommunitaion(int actionCode) {
		this.actionCode = actionCode;
	}

	public int getActionCode() {
		return actionCode;
	}
}
