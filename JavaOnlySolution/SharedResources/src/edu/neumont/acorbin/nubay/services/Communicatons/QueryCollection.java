package edu.neumont.acorbin.nubay.services.Communicatons;

import edu.neumont.acorbin.nubay.services.ServerCommunication;

/**
 * Created by Anthony on 2/28/2015.
 */
public class QueryCollection extends ServerCommunication {
	private String query;

	public QueryCollection(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}
}
