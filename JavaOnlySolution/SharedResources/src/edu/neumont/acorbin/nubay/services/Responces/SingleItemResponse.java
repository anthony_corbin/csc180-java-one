package edu.neumont.acorbin.nubay.services.Responces;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ServerResponse;

/**
 * Created by Anthony on 2/28/2015.
 */
public class SingleItemResponse extends ServerResponse {
	public static final int CREATE_ITEM = 1;
	public static final int EDIT_ITEM   = 2;
	public static final int DELETE_ITEM = 3;

	private ItemDetailModel modelData;
	private String additionalInfo; // mainly exception data

	public SingleItemResponse(int responseCode, ItemDetailModel modelData, String additionalInfo) {
		super(responseCode, additionalInfo);
		this.modelData = modelData;
		this.additionalInfo = additionalInfo;
	}

	public ItemDetailModel getModelData() {
		return modelData;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}
}
