package edu.neumont.acorbin.nubay.services;

import java.math.BigDecimal;
import java.util.*;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;

/**
 * Created by Anthony on 1/21/2015.
 */
public interface ItemService {
    public static class ItemClientException   extends RuntimeException {
        public ItemClientException(String message) {
            super(message);
        }
        public ItemClientException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemClientException(Throwable cause) {
            super(cause);
        }
    }
    public static class ItemServiceException  extends RuntimeException {
        public ItemServiceException(String message) {
            super(message);
        }
        public ItemServiceException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemServiceException(Throwable cause) {
            super(cause);
        }
    }
    public static class ItemNotFoundException extends RuntimeException {
        public ItemNotFoundException(String message) {
            super(message);
        }
        public ItemNotFoundException(String message, Throwable cause) {
            super(message, cause);
        }
        public ItemNotFoundException(Throwable cause) {
            super(cause);
        }
    }

     Collection<ItemDetailModel> GetAll();

    Set<ItemDetailModel> search(String query);
    default ItemDetailModel incrementBid(Long id, BigDecimal bidIncrease) {
		ItemDetailModel tmp = getItemByID(id);
		if(tmp != null) {
			return bid(id,tmp.getCurrentBid().add(bidIncrease));
		}
		return bid(id,bidIncrease); // should throw exception
	}
    default ItemDetailModel incrementBid(Long id, double bidIncrease) {
		return incrementBid(id,new BigDecimal(bidIncrease));
	}
	ItemDetailModel bid(Long id, BigDecimal newAmount);
	default ItemDetailModel bid(Long id, double newAmount) {
		return bid(id,new BigDecimal(newAmount));
	}
    ItemDetailModel getItemByID(long id);
    ItemDetailModel getRandomValidModel();
    ItemDetailModel getRandomInValidModel();
	ItemDetailModel getRandomModel();

    ItemDetailModel update(ItemDetailModel model);

    boolean delete(long id);

    //returns ID of item created, null if item failed to create
    Long AddNewObject(ItemDetailModel item);
    Long AddNewObject(ItemDetailStringBuilder item);
}
