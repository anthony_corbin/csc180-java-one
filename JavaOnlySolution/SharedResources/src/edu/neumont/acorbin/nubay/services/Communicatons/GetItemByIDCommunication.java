package edu.neumont.acorbin.nubay.services.Communicatons;

import edu.neumont.acorbin.nubay.services.ServerCommunication;

/**
 * Created by Anthony on 2/28/2015.
 */
public class GetItemByIDCommunication extends ServerCommunication {

	private Long ID;

	public GetItemByIDCommunication(Long id) {
		ID = id;
	}

	public Long getID() {
		return ID;
	}
}
