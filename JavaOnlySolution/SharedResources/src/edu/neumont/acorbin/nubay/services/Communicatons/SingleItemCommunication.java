package edu.neumont.acorbin.nubay.services.Communicatons;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ServerCommunication;

/**
 * Created by Anthony on 2/28/2015.
 */
public class SingleItemCommunication extends ServerCommunication {
	public static final int CREATE_ITEM = 1;
	public static final int EDIT_ITEM   = 2;
	public static final int DELETE_ITEM = 3;

	private int actionID;
	private ItemDetailModel modelData;

	public SingleItemCommunication(int actionID, ItemDetailModel modelData) {
		this.actionID = actionID;
		this.modelData = modelData;
	}

	public int getActionID() {
		return actionID;
	}

	public ItemDetailModel getModelData() {
		return modelData;
	}
}
