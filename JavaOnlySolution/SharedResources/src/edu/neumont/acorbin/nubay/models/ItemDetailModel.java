package edu.neumont.acorbin.nubay.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import edu.neumont.acorbin.nubay.utils.Compare;
import edu.neumont.acorbin.nubay.utils.Events.SimpleObservable;

/**
 * Created by Anthony on 1/12/2015.
 */
public class ItemDetailModel extends SimpleObservable<ItemDetailModel> implements Serializable {
    private static final long serialVersionUID = 0L;
    private long id;
    private String name;
    private String shortDescription;
    private String longDescription;
    private BigDecimal currentBid;
    private Date StartDate;
    private Date EndDate;
    private String imageUrl;

	public ItemDetailModel(long id) {
		this.id = id;
		this.currentBid = BigDecimal.ZERO;
		StartDate = EndDate = new Date();
	}
	public ItemDetailModel(long id, String name, String shortDescription, String longDescription, BigDecimal currentBid, Date startDate, Date endDate, String imageUrl) {
        this.id = id;
        this.name = name;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.currentBid = currentBid;
        StartDate = startDate;
        EndDate = endDate;
        this.imageUrl = imageUrl;
    }

    public boolean valid() {
        Date currentTime = Calendar.getInstance().getTime();
        boolean ret = (StartDate.getTime() <= currentTime.getTime() && currentTime.getTime() <= EndDate.getTime());
        return ret;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public BigDecimal getCurrentBid() {
        return currentBid;
    }

    public void setShortDescription(String shortDescription) {
        if (!Compare.Compare(this.shortDescription, shortDescription)) {
            this.shortDescription = shortDescription;
            NotifyObservers(this);
        }
    }

    public String getLongDescription() {
        return longDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setName(String name) {
        if (!Compare.Compare(this.name, name)) {
            this.name = name;
            NotifyObservers(this);
        }
    }

    public void setLongDescription(String longDescription) {
        if (!Compare.Compare(this.longDescription, longDescription)) {
            this.longDescription = longDescription;
            NotifyObservers(this);
        }
    }

    public void setCurrentBid(double currentBid) {
		setCurrentBid(new BigDecimal(currentBid));
	}
    public void setCurrentBid(BigDecimal currentBid) {
        if (!Compare.Compare(this.currentBid, currentBid)) {
            this.currentBid = currentBid;
            NotifyObservers(this);
        }
    }

    public void setImageUrl(String imageUrl) {
        if (!Compare.Compare(this.imageUrl, imageUrl)) {
            this.imageUrl = imageUrl;
            NotifyObservers(this);
        }
    }


    public void increaseBid(BigDecimal amount) {
        currentBid = currentBid.add(amount);
        NotifyObservers(this);
    }

    @Override
    public String toString() {
        return "#" + id + " " + name + " (" + currentBid + ")";
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        if (!Compare.Compare(this.EndDate, endDate)) {
            EndDate = endDate;
            NotifyObservers(this);
        }
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        if (!Compare.Compare(this.StartDate, startDate)) {
            StartDate = startDate;
            NotifyObservers(this);
        }
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ItemDetailModel)) return false;

		ItemDetailModel that = (ItemDetailModel) o;

		if (id != that.id) return false;
		if (!EndDate.equals(that.EndDate)) return false;
		if (!StartDate.equals(that.StartDate)) return false;
		if (!currentBid.equals(that.currentBid)) return false;
		if (!imageUrl.equals(that.imageUrl)) return false;
		if (!longDescription.equals(that.longDescription)) return false;
		if (!name.equals(that.name)) return false;
		if (!shortDescription.equals(that.shortDescription)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + name.hashCode();
		result = 31 * result + shortDescription.hashCode();
		result = 31 * result + longDescription.hashCode();
		result = 31 * result + currentBid.hashCode();
		result = 31 * result + StartDate.hashCode();
		result = 31 * result + EndDate.hashCode();
		result = 31 * result + imageUrl.hashCode();
		return result;
	}
}