package edu.neumont.acorbin.nubay;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemByIDCommunication;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemCommunitaion;
import edu.neumont.acorbin.nubay.services.Communicatons.QueryCollection;
import edu.neumont.acorbin.nubay.services.Communicatons.SingleItemCommunication;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.Responces.CollectionOfItemsResponse;
import edu.neumont.acorbin.nubay.services.Responces.SingleItemResponse;
import edu.neumont.acorbin.nubay.services.ServerResponse;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.services.ServiceLoggers.ModelLogger;
import edu.neumont.acorbin.nubay.utils.ColletionUtils;
import edu.neumont.acorbin.nubay.utils.DateParser;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.management.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TranslatorTest {

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	//region validTest

	@Test
	public void testGetRandom() throws Exception {
		ItemDetailModel theOnlyModel = new ItemDetailModel(1, "name_a", "short_a", "long_a", BigDecimal.ONE, DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0), DateParser.getNowPlusDuration(0, 0, 0, 0, 0, 0, 1), "");
		ItemService service = new ServerService(new ModelLogger(
				theOnlyModel
		));
		Translator trans = new Translator(service);
		SingleItemResponse ret = (SingleItemResponse) trans.getResponse(new GetItemCommunitaion(GetItemCommunitaion.GET_RANDOM_ITEM_VALID));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(theOnlyModel, ret.getModelData());

		ret = (SingleItemResponse) trans.getResponse(new GetItemCommunitaion(GetItemCommunitaion.GET_RANDOM_ITEM));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(theOnlyModel, ret.getModelData());

		ret = (SingleItemResponse) trans.getResponse(new GetItemCommunitaion(GetItemCommunitaion.GET_RANDOM_ITEM_INVALID));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(null, ret.getModelData());
	}

	@Test
	public void testGetItem() throws Exception {
		ItemDetailModel theOnlyModel = new ItemDetailModel(1, "name_a", "short_a", "long_a", BigDecimal.ONE, DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0), DateParser.getNowPlusDuration(0, 0, 0, 0, 0, 0, 1), "");
		ItemService service = new ServerService(new ModelLogger(
				theOnlyModel
		));
		Translator trans = new Translator(service);
		SingleItemResponse ret = (SingleItemResponse) trans.getResponse(new GetItemByIDCommunication(1L));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(theOnlyModel, ret.getModelData());

		ret = (SingleItemResponse) trans.getResponse(new GetItemByIDCommunication(2L));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(null, ret.getModelData());

		CollectionOfItemsResponse retCollection = (CollectionOfItemsResponse) trans.getResponse(new QueryCollection("name"));
		Assert.assertEquals(ServerResponse.SUCCESS, retCollection.getResponseCode());
		Assert.assertEquals(theOnlyModel, ColletionUtils.FirstOrDefault(retCollection.getItems()));
	}

	@Test
	public void testCRUD() throws Exception {
		ItemDetailModel theOnlyModel = new ItemDetailModel(1, "name_a", "short_a", "long_a", BigDecimal.ONE, DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0), DateParser.getNowPlusDuration(0, 0, 0, 0, 0, 0, 1), "");
		ItemService service = new ServerService(new ModelLogger()); // empty server
		Translator trans = new Translator(service);

		SingleItemResponse ret = (SingleItemResponse) trans.getResponse(new SingleItemCommunication(SingleItemCommunication.CREATE_ITEM, theOnlyModel));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());

		//update from server
		theOnlyModel = ((SingleItemResponse) trans.getResponse(new GetItemByIDCommunication(ret.getModelData().getId()))).getModelData();

		theOnlyModel.setImageUrl("sup");

		Assert.assertNotSame(ret.getModelData(), ret);

		//preform update
		ret = (SingleItemResponse) trans.getResponse(new SingleItemCommunication(SingleItemCommunication.EDIT_ITEM, theOnlyModel));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());
		Assert.assertEquals(theOnlyModel, ret.getModelData());

		//delete item
		ret = (SingleItemResponse) trans.getResponse(new SingleItemCommunication(SingleItemCommunication.DELETE_ITEM, theOnlyModel));
		Assert.assertEquals(ServerResponse.SUCCESS, ret.getResponseCode());

		//edit deleted item!
		ret = (SingleItemResponse) trans.getResponse(new SingleItemCommunication(SingleItemCommunication.EDIT_ITEM, theOnlyModel));
		Assert.assertEquals(ServerResponse.MODEL_DOES_NOT_EXIST, ret.getResponseCode());
	}

	@Test
	public void testServerException() throws Exception {
		ItemService service = new ServerService(new ServerService.Logger() {
			@Override public void CreateEvent(ItemDetailModel model) { throw new RuntimeException(); }
			@Override public void DeleteEvent(long id) { throw new RuntimeException(); }
			@Override public void EditEvent(ItemDetailModel model) { throw new RuntimeException(); }
			@Override public Iterable<ItemDetailModel> GetAll() { return Arrays.asList(new ItemDetailModel(1,"","","",BigDecimal.ONE,null,null,"")); }
		}); // bad logger

		Translator trans = new Translator(service);
		SingleItemResponse ret = (SingleItemResponse) trans.getResponse(new SingleItemCommunication(SingleItemCommunication.DELETE_ITEM, new ItemDetailModel(1)));
		Assert.assertEquals(ServerResponse.SERVER_ENCOUNTERED_ERROR, ret.getResponseCode());
	}

	//endregion
}