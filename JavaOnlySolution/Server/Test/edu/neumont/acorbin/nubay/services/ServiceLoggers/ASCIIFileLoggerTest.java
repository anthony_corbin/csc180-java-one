package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ASCIIFileLoggerTest {

	private static final String filePath = "filename.txt";

	@Before
	public void setUp() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
	}

	@After
	public void tearDown() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
	}


	//region test valid files

	@Test
	public void testFromFileEvent() throws Exception {
		//setup default file
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		try (PrintStream out = new PrintStream(new FileOutputStream(meFile))) {
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	1	name	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	2	name num 2	shorty desc	long desc	$50.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	3	name num 3	shorty desc	long desc	$100.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	4	name num 4	shorty desc	long desc	$52.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	5	name num 5	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	6	name num 5	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	7	name num 5	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.edit.toString()   + "	6	edited Name	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.delete.toString() + "	7				$41.52	1/1/12	1/1/15	");
		}


		ItemService service = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));
		//service.GetAll()
		List<ItemDetailModel> backBone = new ArrayList<>();
		backBone.add(new ItemDetailModel(1,"name",        "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(2,"name num 2",  "shorty desc", "long desc", MoneyParser.parse("$50.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(3,"name num 3",  "shorty desc", "long desc", MoneyParser.parse("$100.52"), DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(4,"name num 4",  "shorty desc", "long desc", MoneyParser.parse("$52.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(5,"name num 5",  "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(6,"edited Name", "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));

		Assert.assertEquals(backBone.size(),service.GetAll().size());
		for(ItemDetailModel m : service.GetAll()) {
			ItemDetailModel toCheck = backBone.get((int)m.getId()-1);
			Assert.assertEquals(toCheck, m);
		}
	}

	//this proves that it is actually logging to file
	@Test
	public void testPersistenceBetweenLoggers() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		List<ItemDetailModel> backBone = new ArrayList<>();
		backBone.add(new ItemDetailModel(1,"name",        "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(2,"name\nnum 2",  "shorty desc", "long desc", MoneyParser.parse("$50.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(3,"name num 3",  "shorty desc", "long desc", MoneyParser.parse("$100.52"), DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(4,"name num 4",  "shorty desc", "long desc", MoneyParser.parse("$52.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(5,"name num 5",  "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(6,"Name", "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));


		ItemService service1 = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));

		backBone.forEach(service1::AddNewObject);

		backBone.set(5, new ItemDetailModel(6,"Name edited", "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		service1.update(backBone.get(5));
		service1.delete(4);

		ItemService service2 = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));
		Assert.assertEquals(backBone.size()-1,service2.GetAll().size());
		for(ItemDetailModel m : service2.GetAll()) {
			ItemDetailModel toCheck = backBone.get((int)m.getId()-1);
			Assert.assertEquals(toCheck, m);
		}
	}

	//endregion

	//region test file exceptions

	@Test
	public void testNoFile() throws Exception {
		try {
			new ServerService(new ASCIIFileLogger("?watFile"));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) {}
	}

	@Test
	public void testInvalidFile_multipleCreate() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		try (PrintStream out = new PrintStream(new FileOutputStream(meFile))) {
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	1	name	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	2	name num 2	shorty desc	long desc	$50.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	3	name num 3	shorty desc	long desc	$100.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	3	name num 3	shorty desc	long desc	$100.52	1/1/15	1/1/16	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	4	name num 4	shorty desc	long desc	$52.52	1/1/15	1/1/16	www.google.com\n");
		}
		try {
			ItemService service = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) { }
	}

	@Test
	public void testInvalidFile_InvalidEdit() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		try (PrintStream out = new PrintStream(new FileOutputStream(meFile))) {
			out.print(ASCIIFileLogger.FileCommands.create.toString() + "	1	name	short desc	long desc	$41.52	1/1/12	1/1/15	www.google.com\n");
			out.print(ASCIIFileLogger.FileCommands.edit.toString()   + "	2	name num 2	shorty desc	long desc	$50.52	1/1/15	1/1/16	www.google.com\n");
		}
		try {
			ItemService service = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) { }
	}

	@Test
	public void testInvalidFile_CorruptFile() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		try (PrintStream out = new PrintStream(new FileOutputStream(meFile))) {
			out.print("Sup\n");
		}
		try {
			ItemService service = new ServerService(new ASCIIFileLogger(meFile.getAbsolutePath()));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) { }
	}

	//endregion
}