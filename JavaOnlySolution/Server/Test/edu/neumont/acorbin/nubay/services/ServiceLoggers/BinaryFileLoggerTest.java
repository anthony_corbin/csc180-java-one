package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BinaryFileLoggerTest {
	public static String filePath = "filename.bin";


	@Before
	public void setUp() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
	}

	@After
	public void tearDown() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
	}

	//this proves that it is actually logging to file
	@Test
	public void testPersistenceBetweenLoggers() throws Exception {
		File meFile = new File(filePath);
		if(meFile.exists()) meFile.delete();
		meFile.createNewFile();
		List<ItemDetailModel> backBone = new ArrayList<>();
		backBone.add(new ItemDetailModel(1,"name",        "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(2,"name\nnum 2", "shorty desc", "long desc", MoneyParser.parse("$50.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(3,"name num 3",  "shorty desc", "long desc", MoneyParser.parse("$100.52"), DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(4,"name num 4",  "shorty desc", "long desc", MoneyParser.parse("$52.52"),  DateParser.parse("1/1/15"), DateParser.parse("1/1/16"), "www.google.com"));
		backBone.add(new ItemDetailModel(5,"name num 5",  "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		backBone.add(new ItemDetailModel(6,"Name",        "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));


		ItemService service1 = new ServerService(new BinaryFileLogger(meFile.getAbsolutePath()));

		backBone.forEach(service1::AddNewObject);

		backBone.set(5, new ItemDetailModel(6,"Name edited", "short desc",  "long desc", MoneyParser.parse("$41.52"),  DateParser.parse("1/1/12"), DateParser.parse("1/1/15"), "www.google.com"));
		service1.update(backBone.get(5));
		service1.delete(5);
		backBone.set(4,null);

		ItemService service2 = new ServerService(new BinaryFileLogger(meFile.getAbsolutePath()));
		Assert.assertEquals(backBone.size()-1,service2.GetAll().size());
		for(ItemDetailModel m : service2.GetAll()) {
			ItemDetailModel toCheck = backBone.get((int)m.getId()-1);
			Assert.assertEquals(toCheck, m);
		}
	}

	@Test
	public void testInvalidFile() throws Exception {
		try {
			new ServerService(new BinaryFileLogger("?watFile"));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) {}
	}
}