package edu.neumont.acorbin.nubay.services;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.services.ServiceLoggers.ModelLogger;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ServerServiceTest {

	//region basic tests ========================================================================================================

	@Test
	public void testSearch() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0);
		Date endDate = DateParser.getNowPlusDuration(0,30,0,0,0,0,0);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster",         "Very Brave",            "The Brave Toaster has killed 50 foes",                                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Potato",          "Very Starchy",          "The Brave Potato has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.lib.umn.edu/huber195/psy1001spring12/imagespotato-face.jpg")),
				new ItemDetailModel(  2 ,"Panda",           "He will Kill you",      "The Scared Panda has killed 50 foes",                                               new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://blog.boostability.com/wp-content/uploads/2014/09/Panda-Update.jpg")),
				new ItemDetailModel(  3 ,"Singer",          "Person, sounds decent", "Mr Greg has a long career of hurting friends and family's ears with his \"music\"", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://4vector.com/i/free-vector-man-with-a-microphone_099808_Man_with_a_microphone.png")),
				new ItemDetailModel(  4 ,"Desktop Mic",     "Kinda works",           "If you smack it a few times, it starts working",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.altoedge.com/microphones/images/pc-microphone-gn3_hi.jpg")),
				new ItemDetailModel(  5 ,"Mic",             "Unlock your singer",    "Please let the singer you have caged up go, take this mic instead",                 new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://fc09.deviantart.net/fs14/f/2007/004/2/3/Microphone_by_gregVent.jpg")),
				new ItemDetailModel(  6 ,"iPhone",          "$.$",                   "Do you like having money? I didn't think so",                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.telegraph.co.uk/multimedia/archive/02424/iphone-4s_2424784k.jpg")),
				new ItemDetailModel(  7 ,"Hose",            "not a gun",             "really it \"shoots\" water, but it isn't a gun",                                    new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://www.deere.com/common/media/images/product/home_and_workshop_products/air_tools/0076249_air_tools_942x458.jpg")),
				new ItemDetailModel(  8 ,"Hammer",          "hit things",            "Smack anything or anyone right into place with this shiny hammer",                  new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.popularmechanics.com/cm/popularmechanics/images/se/10-tools-for-kids-02-0513-lgn.jpg")),
				new ItemDetailModel(  9 ,"Toe Tail Clipper","For Men",               "For Toe nails made of steel",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.globalspec.com/ImageRepository/LearnMore/20129/ASTM_PEX_Crimp_Tool_3Ca64f4633735f4d9c93933bdd4bcdea53.png")),
				new ItemDetailModel( 10 ,"Plumber's crack", "for pipes and stuff",   "have a good excuse to not pull up your pants",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.johnnichollstrade.co.uk/assets/Rothenberger-tool-32.jpg")),
				new ItemDetailModel( 11 ,"Speaker",         "Makes noise",           "this one makes a lot of noise",                                                     new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://s3-us-west-1.amazonaws.com/static.brit.co/wp-content/uploads/2012/10/Wireless-Pill.jpg")),
				new ItemDetailModel( 12 ,"Alienware",       "Phone",                 "It doesn't really exist yet",                                                       new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://www.blogcdn.com/www.engadget.com/media/2008/02/alienware-concept-phone.jpg")),
				new ItemDetailModel( 13 ,"Samsung Phone",   "Shiny and new",         "this one includes an app that can make calls",                                      new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://3.bp.blogspot.com/-jtiTpw22KZE/T8yDeavLDWI/AAAAAAAAAJg/Aoiq1uHEODw/s1600/best+new+samsung+android+smart+phone+with+3G+technology+at+MarketA2Z.com++%25286%2529.jpg")),
				new ItemDetailModel( 14 ,"Android",         "Android",               "Android",                                                                           new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));

		Assert.assertEquals(1, service.search("very and (brave or phone) and not starchy").size());
		Assert.assertEquals(2,service.search("very and (brave or phone)").size());
		Assert.assertEquals(5,service.search("very and brave or phone").size());
		Assert.assertEquals(2,service.search("very and brave").size());
		Assert.assertEquals(0,service.search("very and brave and").size()); // invalid
	}

	@Test
	public void testDelete() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0,  0,  0,  0,  0,  1);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")),
				new ItemDetailModel(  2 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")),
				new ItemDetailModel(  3 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")),
				new ItemDetailModel(  4 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")),
				new ItemDetailModel(  5 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		for (int i = service.GetAll().size(); i > 0; i--) {
			service.getRandomModel().toString(); // coverage hack
			service.delete(service.getRandomModel().getId());
			Assert.assertEquals(i-1,service.GetAll().size());
		}
		Assert.assertEquals(null,service.getRandomModel());
	}

	@Test
	public void testBid() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0,  0,  0,  0,  0,  1);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		Assert.assertEquals(null,service.getRandomInValidModel());
		ItemDetailModel m = service.getRandomValidModel();
		BigDecimal beforeBid = m.getCurrentBid();
		service.incrementBid(m.getId(), 10);
		BigDecimal after = m.getCurrentBid();
		Assert.assertEquals(10,after.subtract(beforeBid).doubleValue(),.001);
	}

	@Test
	public void testExpiredBit() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0, -1,  0,  0,  0,  0);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		Assert.assertEquals(null,service.getRandomValidModel());
		ItemDetailModel m = service.getRandomInValidModel();
		try {
			service.incrementBid(m.getId(), 10);
			Assert.fail();
		} catch (Exception ignored) { }
	}

	@Test
	public void testBidInvalid() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0, -1,  0,  0,  0,  0);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		Random rand = new Random();
		long invalidId = -1;
		while(service.getItemByID(invalidId) != null) {
			invalidId = rand.nextInt();
		}
		Assert.assertEquals(null,service.incrementBid(invalidId, 100));
	}

	@Test
	public void testGetItemByID() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, -2,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0, -1,  0,  0,  0,  0);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		Collection<ItemDetailModel> all = service.GetAll();
		for(ItemDetailModel m : all) {
			Assert.assertEquals(m,service.getItemByID(m.getId()));
		}
	}

	@Test
	public void testGetRandomModel() throws Exception {
		Date startDate = DateParser.getNowPlusDuration(0,  0, 0,  0,  0,  0,  0);
		Date endDate   = DateParser.getNowPlusDuration(0,  0, 0,  0,  0,  0,  1);
		ItemService service = new ServerService(new ModelLogger(
				new ItemDetailModel(  0 ,"Toaster", "Very Brave", "The Brave Toaster has killed 50 foes", new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")),
				new ItemDetailModel(  1 ,"Android", "Android",    "Android",                              new BigDecimal(10), startDate, endDate, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png"))
		));
		for (int i = 0; i < 10; i++) {
			Assert.assertTrue(service.GetAll().contains(service.getRandomValidModel()));
		}
	}

	@Test
	public void testCreateModelErr() throws Exception {
		ItemService service = new ServerService(new ModelLogger());
		ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
		stringy.setStartDate("Hello");
		Assert.assertTrue(stringy.getErrs().size() > 0);
		Assert.assertFalse(stringy.isValid());
		Assert.assertEquals(stringy.getModel(100),null);
		try {
			service.AddNewObject(stringy);
			Assert.fail();
		} catch (ItemService.ItemClientException ignored) { }
	}

	@Test
	public void testCreateModel() throws Exception {
		ItemService service = new ServerService(new ModelLogger());
		ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
		stringy.setStartDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0)));
		stringy.setImageUrl("");
		stringy.setName("Valid");
		stringy.setShortDescription("Good");
		stringy.setLongDescription("Goooooooooooooooooooooooooooooood");
		stringy.setCurrentBid("not good");
		Assert.assertFalse(stringy.isValid());
		Assert.assertTrue(stringy.getErrs().size() > 0);
		stringy.setCurrentBid(MoneyParser.format(100));
		stringy.setEndDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, 0, 100, 0, 0, 0)));
		Assert.assertTrue(stringy.isValid());
		service.AddNewObject(stringy);
	}

	//endregion

	//region testing exceptions =================================================================================================

	@Test
	public void testCoreExceptions() throws Exception {
		RuntimeException master = new RuntimeException();
		try { throw new ItemService.ItemServiceException ("message"       ); } catch (ItemService.ItemServiceException  e)       { Assert.assertEquals("message",e.getMessage()); }
		try { throw new ItemService.ItemServiceException (          master); } catch (ItemService.ItemServiceException  ignored) { }
		try { throw new ItemService.ItemServiceException ("message",master); } catch (ItemService.ItemServiceException  e)       { Assert.assertEquals("message",e.getMessage()); }
		try { throw new ItemService.ItemClientException  ("message"       ); } catch (ItemService.ItemClientException   e)       { Assert.assertEquals("message",e.getMessage()); }
		try { throw new ItemService.ItemClientException  (          master); } catch (ItemService.ItemClientException   ignored) { }
		try { throw new ItemService.ItemClientException  ("message",master); } catch (ItemService.ItemClientException   e)       { Assert.assertEquals("message",e.getMessage()); }
		try { throw new ItemService.ItemNotFoundException("message"       ); } catch (ItemService.ItemNotFoundException e)       { Assert.assertEquals("message",e.getMessage()); }
		try { throw new ItemService.ItemNotFoundException(          master); } catch (ItemService.ItemNotFoundException ignored) { }
		try { throw new ItemService.ItemNotFoundException("message",master); } catch (ItemService.ItemNotFoundException e)       { Assert.assertEquals("message",e.getMessage()); }
	}

	@Test
	public void testUpdateException() throws Exception {
		try {
			new ServerService(new ModelLogger()).update(new ItemDetailModel(41, "", "", "", null, null, null, ""));
			Assert.fail();
		} catch (ItemService.ItemNotFoundException ignored) { }
	}

	@Test
	public void testLoggerThrowsExceptions_CREATE() throws Exception {
		ItemService invalidCreate = new ServerService(new ServerService.Logger() {
			@Override public void CreateEvent(ItemDetailModel model) { throw new RuntimeException("AHHHH"); }
			@Override public void DeleteEvent(long id)               { }
			@Override public void EditEvent(ItemDetailModel model)   { }
			@Override public Iterable<ItemDetailModel> GetAll() { return new ArrayList<>(); }
		});
		try {
			invalidCreate.AddNewObject(new ItemDetailModel(42,"","","",null,null,null,""));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) {}
	}
	@Test
	public void testLoggerThrowsExceptions_DELETE() throws Exception {
		ItemService invalidDelete = new ServerService(new ServerService.Logger() {
			@Override public void CreateEvent(ItemDetailModel model) { }
			@Override public void DeleteEvent(long id)               { throw new RuntimeException("AHHHH"); }
			@Override public void EditEvent(ItemDetailModel model)   { }
			@Override public Iterable<ItemDetailModel> GetAll() { return new ArrayList<>(); }
		});
		try {
			invalidDelete.AddNewObject(new ItemDetailModel(42,"","","",null,null,null,""));
			invalidDelete.delete(42);
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) {}
	}
	@Test
	public void testLoggerThrowsExceptions_EDIT() throws Exception {
		ItemService invalidEdit = new ServerService(new ServerService.Logger() {
			@Override public void CreateEvent(ItemDetailModel model) { }
			@Override public void DeleteEvent(long id)               { }
			@Override public void EditEvent(ItemDetailModel model)   { throw new RuntimeException("AHHHH"); }
			@Override public Iterable<ItemDetailModel> GetAll() { return new ArrayList<>(); }
		});
		try {
			invalidEdit.AddNewObject(new ItemDetailModel(42,"","","",null,null,null,""));
			invalidEdit.update(new ItemDetailModel(42, "sup", "", "", null, null, null, ""));
			Assert.fail();
		} catch (ItemService.ItemServiceException ignored) {}
	}

	//endregion

	//region multi threaded =====================================================================================================


	private List<Thread> runMultipleThreads(int count, Runnable toRun) {
		List<Thread> threads = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			threads.add(new Thread(toRun));
		}
		threads.forEach(java.lang.Thread::start);
		return threads;
	}

	@Test
	public void MultipleBid() {
		ItemService service = new ServerService(new ModelLogger(Arrays.asList(
				new ItemDetailModel(1,"name1","short1","long1",BigDecimal.ONE,DateParser.getNowPlusDuration(0,0,-1,0,0,0,0),DateParser.getNowPlusDuration(0,0,0,0,0,0,1),""),
				new ItemDetailModel(2,"name2","short2","long2",BigDecimal.ONE,DateParser.getNowPlusDuration(0,0,-1,0,0,0,0),DateParser.getNowPlusDuration(0,0,0,0,0,0,1),"")
		)));

		final boolean[] letsGo = {false};
		final int[] runCount = {0};
		List<Thread> threads = runMultipleThreads(2,() -> {
			boolean working = !letsGo[0];
			while(working) { // wait here
				working = !letsGo[0];
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			try {
				service.bid(1L,10.0);
				runCount[0]++;
			} catch(ItemService.ItemServiceException e) {
			}
		});
		letsGo[0] = true; // RUN!!!!
		for(Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Assert.assertEquals(10,service.getItemByID(1l).getCurrentBid().intValue());
		Assert.assertEquals(2, runCount[0]);
	}

	@Test
	public void MultipleUpdate() throws InterruptedException {
		ItemService service = new ServerService(new ModelLogger(Arrays.asList(
				new ItemDetailModel(1,"name1","short1","long1",BigDecimal.ONE,DateParser.getNowPlusDuration(0,0,-1,0,0,0,0),DateParser.getNowPlusDuration(0,0,0,0,0,0,1),""),
				new ItemDetailModel(2,"name2","short2","long2",BigDecimal.ONE,DateParser.getNowPlusDuration(0,0,-1,0,0,0,0),DateParser.getNowPlusDuration(0,0,0,0,0,0,1),"")
		)));

		ItemDetailModel model = new ItemDetailModel(1);
		final boolean[] letsGo = {false};
		final boolean[] exceptionWasThrown = {false};
		List<Thread> threads = runMultipleThreads(10,() -> {
			boolean working = !letsGo[0];
			while(working) { // wait here
				working = !letsGo[0];
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			try {
				service.update(model);
			} catch(ItemService.ItemServiceException e) {
				exceptionWasThrown[0] = true;
			}
		});
		letsGo[0] = true; // RUN!!!!
		for(Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Assert.assertTrue(exceptionWasThrown[0]);
	}

	//endregion
}