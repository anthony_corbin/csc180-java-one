package edu.neumont.acorbin.nubay;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemByIDCommunication;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemCommunitaion;
import edu.neumont.acorbin.nubay.services.Communicatons.QueryCollection;
import edu.neumont.acorbin.nubay.services.Communicatons.SingleItemCommunication;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.Responces.CollectionOfItemsResponse;
import edu.neumont.acorbin.nubay.services.Responces.SingleItemResponse;
import edu.neumont.acorbin.nubay.services.ServerResponse;
import edu.neumont.acorbin.nubay.utils.FunctionInterfaces.Functions;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Anthony on 2/28/2015.
 */
public class Translator {
	@SuppressWarnings("unchecked")
	static class ActionCRTP<T> implements Functions.Function1<ServerResponse, Object> {

		private Functions.Function1<ServerResponse, T> backbone;

		ActionCRTP(Functions.Function1<ServerResponse, T> backbone) {
			this.backbone = backbone;
		}

		@Override
		public ServerResponse Invoke(Object a) {
			return backbone.Invoke((T) a);
		}
	}

	static class ItemServiceResponse<T> {
		int ErrCode;
		String errMsg = "";
		T data;
		ItemServiceResponse(Functions.Function0<T> getter) {
			ErrCode = ServerResponse.SUCCESS;
			try {
				data = getter.Invoke();
			} catch (ItemService.ItemClientException e) { // only add item from string model throws this error, which isn't available through network
				ErrCode = ServerResponse.CLIENT_DATA_INVALID;
				errMsg = e.getMessage();
				data = null;
			} catch (ItemService.ItemNotFoundException e) {
				ErrCode = ServerResponse.MODEL_DOES_NOT_EXIST;
				errMsg = e.getMessage();
				data = null;
			} catch (ItemService.ItemServiceException e) {
				ErrCode = ServerResponse.SERVER_ENCOUNTERED_ERROR;
				errMsg = e.getMessage();
				data = null;
			}
		}
	}


	static Map<Type, Functions.Function1<ServerResponse, Object>> supportedActions = new HashMap<>();

	public Translator(ItemService service) {
		supportedActions.put(GetItemCommunitaion.class, new ActionCRTP<GetItemCommunitaion>(com -> {
			ItemServiceResponse<ItemDetailModel> ret = null;
			switch (com.getActionCode()) {
				case GetItemCommunitaion.GET_RANDOM_ITEM:         ret = new ItemServiceResponse<>(service::getRandomModel);        break;
				case GetItemCommunitaion.GET_RANDOM_ITEM_INVALID: ret = new ItemServiceResponse<>(service::getRandomInValidModel); break;
				case GetItemCommunitaion.GET_RANDOM_ITEM_VALID:   ret = new ItemServiceResponse<>(service::getRandomValidModel);   break;
			}
			return ret == null ? null : new SingleItemResponse(ret.ErrCode,ret.data,ret.errMsg);
		}));

		supportedActions.put(GetItemByIDCommunication.class, new ActionCRTP<GetItemByIDCommunication>(com -> {
			ItemServiceResponse<ItemDetailModel> ret = new ItemServiceResponse<>(() -> service.getItemByID(com.getID()));
			return new SingleItemResponse(ret.ErrCode,ret.data,ret.errMsg);
		}));

		supportedActions.put(QueryCollection.class, new ActionCRTP<QueryCollection>(com -> {
			ItemServiceResponse<Set<ItemDetailModel>> ret = new ItemServiceResponse<>(() -> service.search(com.getQuery()));
			return new CollectionOfItemsResponse(ret.ErrCode,ret.data,ret.errMsg);
		}));

		supportedActions.put(SingleItemCommunication.class, new ActionCRTP<SingleItemCommunication>(com -> {
			ItemServiceResponse<ItemDetailModel> ret = null;
			switch (com.getActionID()) {
				case SingleItemCommunication.CREATE_ITEM: ret = new ItemServiceResponse<>(()-> new ItemDetailModel(service.AddNewObject(com.getModelData()))); break;
				case SingleItemCommunication.DELETE_ITEM: ret = new ItemServiceResponse<>(()-> service.delete(com.getModelData().getId()) ? new ItemDetailModel(-1) : new ItemDetailModel(-1)); break;
				case SingleItemCommunication.EDIT_ITEM:   ret = new ItemServiceResponse<>(()-> service.update(com.getModelData())); break;
			}
			return ret == null ? null : new SingleItemResponse(ret.ErrCode,ret.data,ret.errMsg);
		}));

	}
	public ServerResponse getResponse(Object clientObj) {
		ServerResponse ret = new ServerResponse(ServerResponse.SERVER_ENCOUNTERED_ERROR, "Given Communication not found, result was unable to be calculated");
		try {
			if (supportedActions.containsKey(clientObj.getClass())) {
				ret = supportedActions.get(clientObj.getClass()).Invoke(clientObj);
			}
		} catch (Exception e) {
			ret = new ServerResponse(ServerResponse.SERVER_ENCOUNTERED_ERROR, e.getMessage()); // should never be hit
		}
		return ret;
	}
}
