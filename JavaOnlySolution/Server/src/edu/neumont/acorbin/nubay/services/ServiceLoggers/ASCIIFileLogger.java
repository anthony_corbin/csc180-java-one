package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;

import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anthony on 2/19/2015.
 */
public class ASCIIFileLogger implements ServerService.Logger {

	private String filePath;

	public ASCIIFileLogger(String filePath) {
		this.filePath = filePath;
	}

	enum FileCommands {
		create,edit,delete
	}
	private static Pattern lineMatch;

	static
	{
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		String delim = "";
		for(FileCommands c : EnumSet.allOf(FileCommands.class)) {
			sb.append(delim).append(c.toString());
			delim = "|";
		}
		sb.append(")");

		delim = "\\t";

		//(create|edit|delete)\t\d+\t([^\t]+)\t([^\t]+)\t([^\t]+)\t(\$?(?:(?:(?:\d+,?)*\d+.?\d+)|(?:.?\d+)))\t((\w+|\d{2}|\d)[\-\.\s/]+(?:(\d{2}|\d)?[\-\.\s/,]+)?(\d{2,4}))\t((\w+|\d{2}|\d)[\-\.\s/]+(?:(\d{2}|\d)?[\-\.\s/,]+)?(\d{2,4}))\t.*

		String dateRegex = DateParser.datePatternRegex.replace("\\((?!\\?)", "(:?"); // (ignoring all groups)

		//System.out.println(dateRegex);

		/*  1 command    */
		/*  2 id         */ sb.append(delim).append("(\\d+)");
		/*  3 name       */ sb.append(delim).append("([^\\t]+)?");
		/*  4 short desc */ sb.append(delim).append("([^\\t]+)?");
		/*  5 long desc  */ sb.append(delim).append("([^\\t]+)?");
		/*  6 money      */ sb.append(delim).append('(').append(MoneyParser.moneyPatternRegex).append(')');
		/*  7 start date */ sb.append(delim).append('(').append(dateRegex).append(')');
		/* 11 end date   */ sb.append(delim).append('(').append(dateRegex).append(')');
		/* 15 URL        */ sb.append(delim).append("(.*)");

		lineMatch = Pattern.compile(sb.toString());
	}

	private void ensureFileCreation() {
		try {
			File file = new File(filePath);
			if(!file.exists())
				file.createNewFile();
		} catch (IOException e) {
			throw new ItemService.ItemServiceException(e); // move it on up
		}
	}

	private void LogEvent(FileCommands command, ItemDetailModel model) {
		ensureFileCreation();

		String delim = "\t";
		try(BufferedWriter file = new BufferedWriter(new FileWriter(filePath,true))) {
			String sb = new StringBuilder()
			.append(command.toString())
			.append(delim).append(model.getId())
			.append(delim).append(model.getName())
			.append(delim).append(model.getShortDescription())
			.append(delim).append(model.getLongDescription())
			.append(delim).append(MoneyParser.format(model.getCurrentBid()))
			.append(delim).append(DateParser.format(model.getStartDate()))
			.append(delim).append(DateParser.format(model.getEndDate()))
			.append(delim).append(model.getImageUrl())
			.append('\n').toString();

			file.write(sb);
		} catch (IOException e) {
			//should never be hit because ensure file creations should throw errors for invalid files
			throw new ItemService.ItemServiceException(e);
		}
	}

	@Override
	public void CreateEvent(ItemDetailModel model) {
		LogEvent(FileCommands.create,model);
	}

	@Override
	public void DeleteEvent(long id) {
		//i'm lazy and don't want to make a special case for delete
		//creating dummy object with correct id
		LogEvent(FileCommands.delete,new ItemDetailModel(id));
	}

	@Override
	public void EditEvent(ItemDetailModel model) {
		LogEvent(FileCommands.edit,model);
	}

	@Override
	public Iterable<ItemDetailModel> GetAll() {
		ensureFileCreation();
		HashMap<Long, ItemDetailModel> ret = new HashMap<>();
		try(BufferedReader file = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))) {
			StringBuilder line = new StringBuilder();
			String toAdd;
			while((toAdd = file.readLine()) != null) {
				line.append(toAdd);
				Matcher m = lineMatch.matcher(line);
				if(m.matches()) {
					//process

					Long id = Long.parseLong(m.group(2));

					if(m.group(1).equals(FileCommands.delete.toString())) {
						if(ret.containsKey(id))
							ret.remove(id);
					} else {
						String name = m.group(3);
						String sDesc = m.group(4);
						String lDesc = m.group(5);
						BigDecimal money = MoneyParser.parse(m.group(6));
						Date sDate = DateParser.parse(m.group(7));
						Date eDate = DateParser.parse(m.group(11));
						String url = m.group(15);

						ItemDetailModel model = new ItemDetailModel(id,name,sDesc,lDesc,money,sDate,eDate,url);


						if (m.group(1).equals(FileCommands.create.toString())) {
							if (ret.containsKey(id)) {
								throw new ItemService.ItemServiceException("Invalid file, create event called with existing item");
							}
							ret.put(id,model);
						} else if (m.group(1).equals(FileCommands.edit.toString())) {
							if(!ret.containsKey(id)) {
								throw new ItemService.ItemServiceException("Invalid file, editing item that does not exist");
							}
							ret.remove(id);
							ret.put(id,model);
						} else {
							// following line should never be hit because of regex
							throw new ItemService.ItemServiceException("Programmer screwed up and didn't implement command");
						}
					}
					line = new StringBuilder();
				} else {
					line.append('\n');//preserve line return if building across lines
				}
			}
			if(line.length() > 0)
				throw new ItemService.ItemServiceException("invalid file, data left not parsed");
			return ret.values();
		} catch (IOException e) {
			throw new ItemService.ItemServiceException(e);
		}
	}
}
















