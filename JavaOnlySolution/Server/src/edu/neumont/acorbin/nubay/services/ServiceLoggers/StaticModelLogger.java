package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Anthony on 2/24/2015.
 */
public class StaticModelLogger implements ServerService.Logger {
	private static HashMap<Long,ItemDetailModel> items;
	static {
		long ItemID = 0;
		items = new HashMap<>();
		Date startDate = DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0);
		Date endDate = DateParser.getNowPlusDuration(0,30,0,0,0,0,1);
		Date endDate2 = DateParser.getNowPlusDuration(0,0,0,0,0,0,0);
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Toaster",         "Very Brave",            "The Brave Toaster has killed 50 foes",                                              new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://i.imgur.com/GcOsB8D.png")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Potato",          "Very Starchy",          "The Brave Potato has killed 50 foes",                                               new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://blog.lib.umn.edu/huber195/psy1001spring12/imagespotato-face.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Panda",           "He will Kill you",      "The Scared Panda has killed 50 foes",                                               new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://blog.boostability.com/wp-content/uploads/2014/09/Panda-Update.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Singer",          "Person, sounds decent", "Mr Greg has a long career of hurting friends and family's ears with his \"music\"", new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://4vector.com/i/free-vector-man-with-a-microphone_099808_Man_with_a_microphone.png")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Desktop Mic",     "Kinda works",           "If you smack it a few times, it starts working",                                    new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://www.altoedge.com/microphones/images/pc-microphone-gn3_hi.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Mic",             "Unlock your singer",    "Please let the singer you have caged up go, take this mic instead",                 new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://fc09.deviantart.net/fs14/f/2007/004/2/3/Microphone_by_gregVent.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"iPhone",          "$.$",                   "Do you like having money? I didn't think so",                                       new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://i.telegraph.co.uk/multimedia/archive/02424/iphone-4s_2424784k.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Hose",            "not a gun",             "really it \"shoots\" water, but it isn't a gun",                                    new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("https://www.deere.com/common/media/images/product/home_and_workshop_products/air_tools/0076249_air_tools_942x458.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Hammer",          "hit things",            "Smack anything or anyone right into place with this shiny hammer",                  new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://www.popularmechanics.com/cm/popularmechanics/images/se/10-tools-for-kids-02-0513-lgn.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Toe Tail Clipper","For Men",               "For Toe nails made of steel",                                                       new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://www.globalspec.com/ImageRepository/LearnMore/20129/ASTM_PEX_Crimp_Tool_3Ca64f4633735f4d9c93933bdd4bcdea53.png")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Plumber's crack", "for pipes and stuff",   "have a good excuse to not pull up your pants",                                      new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://www.johnnichollstrade.co.uk/assets/Rothenberger-tool-32.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Speaker",         "Makes noise",           "this one makes a lot of noise",                                                     new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://s3-us-west-1.amazonaws.com/static.brit.co/wp-content/uploads/2012/10/Wireless-Pill.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Alienware",       "Phone",                 "It doesn't really exist yet",                                                       new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://www.blogcdn.com/www.engadget.com/media/2008/02/alienware-concept-phone.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Samsung Phone",   "Shiny and new",         "this one includes an app that can make calls",                                      new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("http://3.bp.blogspot.com/-jtiTpw22KZE/T8yDeavLDWI/AAAAAAAAAJg/Aoiq1uHEODw/s1600/best+new+samsung+android+smart+phone+with+3G+technology+at+MarketA2Z.com++%25286%2529.jpg")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"Android",         "Android",               "Android",                                                                           new BigDecimal(10), startDate, endDate,  MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")));
		ItemID++; items.put(ItemID,new ItemDetailModel(ItemID,"invalidTitle",    "Android",               "Android",                                                                           new BigDecimal(10), startDate, endDate2, MyStringUtils.javaStringLiteral("https://cdn4.iconfinder.com/data/icons/smart-phones-technologies/512/android-phone-color.png")));
	}

	@Override
	public void CreateEvent(ItemDetailModel model) {
		items.put(model.getId(), model);
	}

	@Override
	public void DeleteEvent(long id) {
		items.remove(id);
	}

	@Override
	public void EditEvent(ItemDetailModel model) {
		items.remove(model.getId());
		items.put(model.getId(),model);
	}

	@Override
	public Iterable<ItemDetailModel> GetAll() {
		return items.values();
	}
}
