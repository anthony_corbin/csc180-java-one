package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerService;

import java.io.*;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by Anthony on 2/23/2015.
 */
public class BinaryFileLogger implements ServerService.Logger {
	private String filePath;

	public BinaryFileLogger(String filePath) {
		this.filePath = filePath;
	}

	private static final int CREATE_EVENT = 1;
	private static final int EDIT_EVENT = 2;
	private static final int DELETE_EVENT = 3;

	private void ensureFileCreation() {
		try {
			File file = new File(filePath);
			if (!file.exists())
				file.createNewFile();
		} catch (IOException e) {
			throw new ItemService.ItemServiceException(e); // move it on up
		}
	}

	private void LogEvent(int event, ItemDetailModel data) {
		ensureFileCreation();

		try (FileOutputStream output = new FileOutputStream(filePath, true)) {
			output.write(event);
			new java.io.ObjectOutputStream(output).writeObject(data);
		} catch (IOException e) {
			//should never be hit since ensure file creation should throw error
			throw new ItemService.ItemServiceException(e);
		}
	}

	@Override
	public void CreateEvent(ItemDetailModel model) {
		LogEvent(CREATE_EVENT,model);
	}

	@Override
	public void DeleteEvent(long id) {
		LogEvent(DELETE_EVENT, new ItemDetailModel(id));
	}

	@Override
	public void EditEvent(ItemDetailModel model) {
		LogEvent(EDIT_EVENT,model);
	}

	public Iterable<ItemDetailModel> GetAll() {
		ensureFileCreation();
		HashMap<Long, ItemDetailModel> ret = new HashMap<>();
		try (FileInputStream file = new FileInputStream(filePath)) {
			int Command;
			while((Command = file.read()) != -1) {
				java.io.ObjectInputStream tmp = new java.io.ObjectInputStream(file);
				if (Command == CREATE_EVENT) {
					ItemDetailModel model = (ItemDetailModel)tmp.readObject();
					ret.put(model.getId(), model);
				} else if (Command == EDIT_EVENT) {
					ItemDetailModel model = (ItemDetailModel)tmp.readObject();
					ret.remove(model.getId());
					ret.put(model.getId(), model);
				} else if (Command == DELETE_EVENT) {
					long id = ((ItemDetailModel)tmp.readObject()).getId();
					if (ret.containsKey(id)) ret.remove(id);
				}
			}
		} catch (IOException | ClassNotFoundException e) {
			//should never be hit because ensure file creation will throw file errors
			throw new ItemService.ItemServiceException(e);
		}
		return ret.values();
	}
}
