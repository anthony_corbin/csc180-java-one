package edu.neumont.acorbin.nubay.services.ServiceLoggers;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerService;

import java.util.HashMap;

/**
 * Created by Anthony on 2/28/2015.
 */
public class ModelLogger implements ServerService.Logger {
	private HashMap<Long,ItemDetailModel> items = new HashMap<>();

	public ModelLogger(Iterable<ItemDetailModel> models) { for(ItemDetailModel m : models) { items.put(m.getId(),m); } }
	//ModelLogger(HashMap<Long,ItemDetailModel> items) { this.items = items; }
	public ModelLogger(ItemDetailModel... models) {
		for(ItemDetailModel m : models) {
			items.put(m.getId(),m);
		}
	}

	@Override
	public void CreateEvent(ItemDetailModel model) {
		items.put(model.getId(), model);
	}

	@Override
	public void DeleteEvent(long id) {
		items.remove(id);
	}

	@Override
	public void EditEvent(ItemDetailModel model) {
		items.remove(model.getId());
		items.put(model.getId(),model);
	}

	@Override
	public Iterable<ItemDetailModel> GetAll() {
		return items.values();
	}
}