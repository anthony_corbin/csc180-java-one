package edu.neumont.acorbin.nubay.services;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.utils.ColletionUtils;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;
import edu.neumont.acorbin.nubay.utils.Predicates.AndPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.NotPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.OrPredicate;
import edu.neumont.acorbin.nubay.utils.Predicates.Predicate;
import edu.neumont.acorbin.nubay.utils.ReversePolish;
import edu.neumont.acorbin.nubay.utils.ShuntingYard;
import edu.neumont.acorbin.nubay.utils.Threading.LocksBasedOffID;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Anthony on 2/28/2015.
 */
public class ServerService implements ItemService {

	//region static resources and helper classes ================================================================================

	private static ShuntingYard parser; static {
		Map<String, ShuntingYard.OperatorDetails> OPERATORS = new HashMap<>();
		OPERATORS.put("or",  new ShuntingYard.OperatorDetails(0,  ShuntingYard.Associativity.LEFT) );
		OPERATORS.put("and", new ShuntingYard.OperatorDetails(1,  ShuntingYard.Associativity.LEFT) );
		OPERATORS.put("not", new ShuntingYard.OperatorDetails(2,  ShuntingYard.Associativity.LEFT) );
		parser = new ShuntingYard(OPERATORS);
	}
	private static ReversePolish<String,Predicate<String>> eval; static {
		HashMap<String, ReversePolish.PolishOperator<Predicate<String>>> operators = new HashMap<>();
		operators.put("and", new ReversePolish.PolishOperator<Predicate<String>>() {
			@Override public int getOpCount() { return 2; }
			@Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new AndPredicate<>(data.pop(), data.pop()); }
		});
		operators.put("or", new ReversePolish.PolishOperator<Predicate<String>>() {
			@Override public int getOpCount() { return 2; }
			@Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new OrPredicate<>(data.pop(), data.pop()); }
		});
		operators.put("not", new ReversePolish.PolishOperator<Predicate<String>>() {
			@Override public int getOpCount() { return 1; }
			@Override public Predicate<String> eval(Stack<Predicate<String>> data) { return new NotPredicate<>(data.pop()); }
		});



		eval = new ReversePolish<>(a -> { // from search query
			return val -> val.toLowerCase().contains(a.toLowerCase());
		}, operators);
	}
	public static interface Logger {
		void CreateEvent(ItemDetailModel model);
		void DeleteEvent(long id);
		void EditEvent(ItemDetailModel model);
		Iterable<ItemDetailModel> GetAll(); // called when first loaded
	}

	//endregion

	//region local vars =========================================================================================================

	private HashMap<Long,ItemDetailModel> itemsById;
	private LocksBasedOffID locker = new LocksBasedOffID();
	private Logger logger;
	private static long ItemID = -1;

	//endregion

	public ServerService(Logger logger) {
		this.logger = logger;
		itemsById = new HashMap<>();
		for(ItemDetailModel m : logger.GetAll()) {
			ItemID = Math.max(ItemID,m.getId()+1);
			itemsById.put(m.getId(),m);
		}
	}

	//region CRUD Read (Getters) ================================================================================================

	public Collection<ItemDetailModel> GetAll(){
		return itemsById.values();
	}

	public Set<ItemDetailModel> search(String query){
		query = query.toLowerCase();
		if(query.equals("all")) return new HashSet<>(GetAll());
		Predicate<String> condition;
		Set<ItemDetailModel> ret;
		try {
			condition = eval.Eval(parser.infixToRPN(query));
			ret =  ColletionUtils.filter(
					ColletionUtils.filter(itemsById.values(), ItemDetailModel::valid)
					, condition, obj -> obj.getName() + " " + obj.getShortDescription() + " " + obj.getLongDescription());
		} catch(IllegalArgumentException e) {
			ret = new HashSet<>(); // empty set
		}
		return ret;
	}

	public ItemDetailModel getItemByID(long id) {
		if(itemsById.containsKey(id)) {
			return itemsById.get(id);
		}
		return null;
	}
	public ItemDetailModel getRandomValidModel() {
		int randomIndex = new Random().nextInt(itemsById.size());
		ItemDetailModel ret = null;
		int index = 0;
		for(ItemDetailModel i : itemsById.values()) {
			if(i.valid()) ret = i;
			if(ret != null && index >= randomIndex) return ret;
			index++;
		}
		return null;
	}
	public ItemDetailModel getRandomInValidModel() {
		int randomIndex = new Random().nextInt(itemsById.size());
		ItemDetailModel ret = null;
		int index = 0;
		for(ItemDetailModel i : itemsById.values()) {
			if(!i.valid()) ret = i;
			if(ret != null && index >= randomIndex)
				return ret;
			index++;
		}
		return null;
	}
	public ItemDetailModel getRandomModel() {
		int randomIndex = new Random().nextInt(Math.max(1,itemsById.size()));
		int index = 0;
		for(ItemDetailModel i : itemsById.values()) {
			if(index++ == randomIndex) return i;
		}
		return null;
	}

	//endregion

	//region CRUD Update ========================================================================================================

	@Override
	public ItemDetailModel bid(Long id, BigDecimal newAmount) {
		if(itemsById.containsKey(id)) {
			ItemDetailModel model = itemsById.get(id);
			if(model.valid()) {
				synchronized (itemsById.get(id)) {
					if(newAmount.doubleValue() > model.getCurrentBid().doubleValue()) {
						model.setCurrentBid(newAmount);
						update(model);
					}
				}
			} else {
				throw new RuntimeException("Item Has Expired");
			}
			return model;
		}
		return null;
	}

	public ItemDetailModel update(ItemDetailModel model) {
		if(itemsById.containsKey(model.getId())) {
			if(!locker.TryApplyLock(model.getId())) {
				throw new ItemServiceException("Multiple Items being edited at the same time, try again");
			}
			try {
				logger.EditEvent(model);

				itemsById.get(model.getId()).setName(model.getName());
				itemsById.get(model.getId()).setShortDescription(model.getShortDescription());
				itemsById.get(model.getId()).setLongDescription(model.getLongDescription());
				itemsById.get(model.getId()).setCurrentBid(model.getCurrentBid());
				itemsById.get(model.getId()).setStartDate(model.getStartDate());
				itemsById.get(model.getId()).setEndDate(model.getEndDate());
				itemsById.get(model.getId()).setImageUrl(model.getImageUrl());
			} catch (Exception e) {
				locker.ReleaseLock(model.getId());
				throw new ItemServiceException("Something Broke", e);
			}
		} else {
			throw new ItemNotFoundException("update failed: "+model.getId()+" not in collection");
		}
		locker.ReleaseLock(model.getId());
		return getItemByID(model.getId());
	}

	//endregion

	//region CRUD Delete ========================================================================================================

	public boolean delete(long id) {
		if(itemsById.containsKey(id)) {
			if(!locker.TryApplyLock(id)) {
				throw new ItemServiceException("Multiple Items being edited at the same time, try again");
			}
			try {
				logger.DeleteEvent(id);
				itemsById.remove(id);
			} catch (Exception e) {
				locker.ReleaseLock(id);
				throw new ItemServiceException("Something Broke", e);
			}
		}
		locker.ReleaseLock(id);
		return true;
	}

	//endregion

	//region CRUD Create ========================================================================================================

	//returns ID of item created, null if item failed to create
	public Long AddNewObject(ItemDetailModel item) {
		synchronized (this) {
			try {
				logger.CreateEvent(item);
				itemsById.put(item.getId(), item);
			} catch (Exception e) {
				throw new ItemServiceException("Something Broke", e);
			}
			return item.getId();
		}
	}
	public Long AddNewObject(ItemDetailStringBuilder item) {
		item.setImageUrl(MyStringUtils.javaStringLiteral("http://content.mycutegraphics.com/graphics/clothing/green-sock.png"));
		if(!item.isValid()) {
			throw new ItemClientException("Unable to add new model, it still invalid");
		}
		return AddNewObject(item.getModel(ItemID++));
	}

	//endregion
}
