package edu.neumont.acorbin.nubay.driver;

import edu.neumont.acorbin.nubay.Translator;
import edu.neumont.acorbin.nubay.services.ItemService;
import edu.neumont.acorbin.nubay.services.ServerResponse;
import edu.neumont.acorbin.nubay.services.ServerService;
import edu.neumont.acorbin.nubay.services.ServiceLoggers.StaticModelLogger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Anthony on 2/28/2015.
 */
public class Driver {

	public static void runFromPool(Socket s, ItemService service, ExecutorService threadPool) throws IOException, ClassNotFoundException {


	}
	private static void runOnCurrentThread(Socket s, ItemService service) throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(s.getInputStream());
		ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());

		ServerResponse ret = new Translator(service).getResponse(in.readObject());
		out.writeObject(ret);
		s.close();
	}

	public static void main(String[] args) {
		ItemService service = new ServerService(new StaticModelLogger());
		ExecutorService threadPool = Executors.newFixedThreadPool(100);

		try (ServerSocket ss = new ServerSocket(8080)) {
			//noinspection InfiniteLoopStatement
			while (true) {
				Socket s = ss.accept(); // represents 1 connection from 1 computer
				threadPool.submit(() -> {
					try {
						ObjectInputStream in = new ObjectInputStream(s.getInputStream());
						ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
						ServerResponse ret = new Translator(service).getResponse(in.readObject());
						out.writeObject(ret);
						s.close();
					} catch (Exception ignored) {}
				});


			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
