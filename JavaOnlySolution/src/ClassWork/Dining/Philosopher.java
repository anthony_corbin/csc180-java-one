package ClassWork.Dining;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Anthony on 3/9/2015.
 */
public class Philosopher implements Runnable {
	private static List<String> logs = new ArrayList<>();


	private int bites = 0;

	private final int left;
	private final int right;
	private String name;
	private ChopStick[] myChopsticks;
	private Table table;

	public static synchronized void Log(String msg) {
		logs.add(msg);
		Thread.currentThread().setName(msg);
		System.out.println(msg);
	}
	public static void printLog() {
		/*
		for(String log : logs) {
			System.out.print(log);
			System.out.print('\t');
		}
		//*/
	}

	public Philosopher(int left, int right, String name, ChopStick[] myChopsticks, Table table) {
		this.left = left;
		this.right = right;
		this.name = name;
		this.myChopsticks = myChopsticks;
		this.table = table;
	}

	public void eat(int secondsToEat) {
		long start = System.currentTimeMillis();
		long endTime = start + secondsToEat * 1000;
		while(System.currentTimeMillis() < endTime) {
			//singleEatWithDrops();
			//singleEatSyncLock();
			singleEatWithTable();
		}
	}
	private void singleEatWithDrops() {
		String prependTmp = "";
		if(getChopStick(left).tryGet(1)) {
			Log(name + " Got Left");
			if(getChopStick(right).tryGet(1)) {
				Log(name + " Got Right");
				Log(name + " Eat some food");
				takeABite();
				getChopStick(right).drop();
				Log(name + " Dropped right");
			} else {
				prependTmp = "Unable to grab right so ";
			}
			getChopStick(left).drop();
			Log(prependTmp + name + " Dropped left");
		} else {
			try {Thread.sleep(new Random().nextInt(10));} catch (InterruptedException ignored) {}
		}
	}
	private void singleEatSyncLock() {
		synchronized (getChopStick(left)) {
			Log(name + " Got Left");
			synchronized (getChopStick(right)) {
				Log(name + " Got Right");
				Log(name + " Eat some food");
				takeABite();
			}
		}
	}
	private void singleEatWithTable() {
		ChopStick[] chopSticks = table.pickUpChopsticks(this,left,right);

		table.takeBite(this,left,right);

		table.dropChopsticks(this,right,left);
	}

	ChopStick getChopStick(int chopstickID) {
		return myChopsticks[chopstickID];
	}

	@Override
	public void run() {
		eat(5);
	}

	public int getBites() {
		return bites;
	}

	public synchronized void takeABite() {
		this.bites++;
		try {Thread.sleep(100);} catch (InterruptedException ignored) {}
	}

	@Override
	public String toString() {
		return "Philosopher{" +
				"name='" + name + '\'' +
				'}';
	}
}
