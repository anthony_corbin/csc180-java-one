package ClassWork.Dining;

import sun.awt.Mutex;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anthony on 3/9/2015.
 */
public class ChopStick {
	private final int id;
	private final Semaphore mu = new Semaphore(1);

	public ChopStick(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public boolean tryGet(int timeout) {
		try {
			return mu.tryAcquire(timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			return false;
		}
	}
	public boolean tryGet() {
		return mu.tryAcquire();
	}
	private long timePassedSince(long startTime) {
		long ret = System.currentTimeMillis() - startTime;
		return ret;
	}
	public void drop() {
		mu.release();
	}
}
