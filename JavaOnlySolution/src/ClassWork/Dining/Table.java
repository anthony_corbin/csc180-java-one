package ClassWork.Dining;

/**
 * Created by Anthony on 3/10/2015.
 */
public class Table {
	private Philosopher[] locks;
	private ChopStick[] chopsticks;

	public Table(int size, ChopStick[] chopsticks) {
		this.chopsticks = chopsticks;
		locks = new Philosopher[size];
	}

	public synchronized ChopStick[]  pickUpChopsticks(Philosopher philosopher, int first, int second) {
		waitAndGetChopstick(philosopher,first);
		waitAndGetChopstick(philosopher,second);

		return new ChopStick[] { chopsticks[first], chopsticks[second] };
	}

	public void waitAndGetChopstick(Philosopher philosopher, int chopstickID) {
		while(locks[chopstickID] != null) {
			try { wait(); } catch (InterruptedException ignored) { }
		}
		locks[chopstickID] = philosopher;
	}

	public void takeBite(Philosopher philosopher, int first, int second) {
		if(locks[first] == philosopher && locks[second] == philosopher) {
			philosopher.takeABite();
			return;
		}
		System.out.println("Corrupt Data In Bite");
	}

	public synchronized void dropChopsticks(Philosopher philosopher, int first, int second) {
		if(locks[first] == philosopher && locks[second] == philosopher) {
			locks[first] = null;
			locks[second] = null;
			notify();
		} else {
			System.out.println("Corrupt Data In Drop");
		}
	}
}
