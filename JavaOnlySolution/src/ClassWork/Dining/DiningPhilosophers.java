package ClassWork.Dining;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Anthony on 3/9/2015.
 */
public class DiningPhilosophers {
	private ChopStick[] chopSticks;
	private Philosopher[] philosophers;
	private Table table;

	public DiningPhilosophers(int size) {
		chopSticks = new ChopStick[size];
		philosophers = new Philosopher[size];
		table = new Table(size, chopSticks);
		for (int i = 0; i < size; i++) {
			chopSticks[i] = new ChopStick(i);
		}
		for (int i = 0; i < size; i++) {
			int left  = i;
			int right = (i+1) % size;
			philosophers[i] = new Philosopher(left, right, ""+i,chopSticks,table);
		}
	}

	public void go() {
		List<Thread> toJoinOn = new ArrayList<>();
		for (Philosopher philosopher : philosophers) {
			Thread tmp = new Thread(philosopher);
			toJoinOn.add(tmp);
			tmp.start();
		}
		for (Thread thread : toJoinOn) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private Philosopher get(int index) {
		return philosophers[index];
	}


	public static void main(String[] args) {
		System.out.println("Enter number of dudes: ");
		int size = new Scanner(System.in).nextInt();
		DiningPhilosophers program = new DiningPhilosophers(size);
		program.go();

		Philosopher.printLog();

		System.out.println("\n" +
				"Eating Summary\n" +
				"--------------\n" +
				"\n");
		for (int i = 0; i < size; i++) {
			System.out.printf("dude %s ate %s bites\n", i, program.get(i).getBites());
		}
	}
}
