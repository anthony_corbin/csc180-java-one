package ClassWork;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anthony on 2/19/2015.
 */
public class DummyItemService {
	private static Pattern lineMatch = Pattern.compile("(\\d+)alligator(.*)");
	private String filePath;

	public DummyItemService(String filePath) {
		this.filePath = filePath;
	}

	public String getDummy(Long id) {
		try(BufferedReader file = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))) {
			String line;
			while((line = file.readLine()) != null) {
				Matcher m = lineMatch.matcher(line);
				if(m.matches() && Long.parseLong(m.group(1)) == id)
					return m.group(2);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[]args) {
		DummyItemService service = new DummyItemService("C:\\Users\\Anthony\\Documents\\Repos\\JavaOne\\JavaOnlySolution\\src\\ClassWork\\DummyItemList.txt");
		long id = -1;
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
		id++; System.out.printf("%s: %s\n",id,service.getDummy(id));
	}
}
