package ClassWork.Sockets;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Anthony on 2/25/2015.
 */
public class LineCounterInputStream extends InputStream {
	private InputStream is;
	private int numOfLines = 1;

	public LineCounterInputStream(InputStream is) {
		this.is = is;
	}

	@Override
	public int read() throws IOException {
		int b = is.read();

		if( b == (int)'\n') {
			numOfLines++;
		}

		return b;
	}

	public int getNumOfLines() {
		return numOfLines;
	}
}
