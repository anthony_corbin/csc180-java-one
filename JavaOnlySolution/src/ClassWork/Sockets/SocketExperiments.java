package ClassWork.Sockets;

import ClassWork.XML_JSON.Crocodile;
import ClassWork.XML_JSON.EncryptingInputStream;
import ClassWork.XML_JSON.EncryptingOutputStream;

import java.io.*;
import java.net.*;

/**
 * Created by Anthony on 2/25/2015.
 */
public class SocketExperiments {

	public static void main(String[]args) throws Exception {
		//URL();
		//encrypt();
		ports();
	}
	private static void URL() throws Exception {
		URL url = new URL("http://www.google.com");
		URLConnection c = url.openConnection();
		InputStream is = c.getInputStream();

		LineCounterInputStream lcis = new LineCounterInputStream(is);

		BufferedReader br = new BufferedReader(new InputStreamReader(lcis));
		String line;
		int tmp = 1;
		while((line = br.readLine()) != null)
			System.out.printf("%s - %s\n", tmp++, line);
		System.out.println();
		System.out.printf("Total Num of lines: %s\n", lcis.getNumOfLines());
	}
	private static void encrypt() throws Exception {

		String filepath = "testFile.bin";
		boolean verbose = false;

		Crocodile c = new Crocodile(1L,"Charles",120,34,120*34);

		ObjectOutputStream encrypted = new ObjectOutputStream(new EncryptingOutputStream(new FileOutputStream(filepath),verbose));
		encrypted.writeObject(c);
		System.out.println();

		ObjectInputStream in = new ObjectInputStream(new EncryptingInputStream(new FileInputStream(filepath),verbose));

		Crocodile out = (Crocodile)in.readObject();

		System.out.println();

		if(c.getId().equals(out.getId())) {
			System.out.println("YAY");
		} else {
			System.out.println("fail");
		}
	}
	private static void ports() {
		new Thread(Server::main).start();
		new Thread(Client::main).start();
		while(true); // hehe lets stop the main thread from existing
	}


	public static class Client {
		public static void main() {
			Socket s = null;
			try {
				s = new Socket("localhost",8080);

				PrintWriter pw = new PrintWriter(s.getOutputStream(),true);
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));

				while(!s.isClosed()) {
					pw.println("ping"); // ping
					//System.out.print("ping - ");
					System.out.println(br.readLine()); // pong
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(s != null) {
					try {
						s.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	public static class Server {
		public static void main() {
			try(ServerSocket ss = new ServerSocket(8080)) {
				Socket s = ss.accept(); // represents 1 connection from 1 computer

				PrintWriter pw = new PrintWriter(s.getOutputStream(),true);
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));

				while(!s.isClosed()) {
					System.out.println(br.readLine());
					//System.out.println(" - pong");
					pw.println("pong");
				}

				System.out.println("Server Done");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

