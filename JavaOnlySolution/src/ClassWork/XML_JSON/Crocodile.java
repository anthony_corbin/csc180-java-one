package ClassWork.XML_JSON;

import java.io.Serializable;

/**
 * Created by Anthony on 2/24/2015.
 */
public class Crocodile implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Integer lengthInInches;

	private Integer widthInInches;
	private Integer area;

	public Crocodile() { }
	public Crocodile(Long id, String name, Integer lengthInInches, Integer widthInInches, Integer area) {
		this.id = id;
		this.name = name;
		this.lengthInInches = lengthInInches;
		this.widthInInches = widthInInches;
		this.area = area;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLengthInInches() {
		return lengthInInches;
	}

	public void setLengthInInches(Integer lengthInInches) {
		this.lengthInInches = lengthInInches;
	}

	public Integer getWidthInInches() {
		return widthInInches;
	}

	public void setWidthInInches(Integer widthInInches) {
		this.widthInInches = widthInInches;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}



	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Crocodile)) return false;

		Crocodile crocodile = (Crocodile) o;

		if (area != null ? !area.equals(crocodile.area) : crocodile.area != null) return false;
		if (id != null ? !id.equals(crocodile.id) : crocodile.id != null) return false;
		if (lengthInInches != null ? !lengthInInches.equals(crocodile.lengthInInches) : crocodile.lengthInInches != null)
			return false;
		if (name != null ? !name.equals(crocodile.name) : crocodile.name != null) return false;
		if (widthInInches != null ? !widthInInches.equals(crocodile.widthInInches) : crocodile.widthInInches != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (lengthInInches != null ? lengthInInches.hashCode() : 0);
		result = 31 * result + (widthInInches != null ? widthInInches.hashCode() : 0);
		result = 31 * result + (area != null ? area.hashCode() : 0);
		return result;
	}
}
