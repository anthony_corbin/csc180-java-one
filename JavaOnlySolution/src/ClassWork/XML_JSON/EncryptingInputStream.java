package ClassWork.XML_JSON;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Anthony on 2/25/2015.
 */
public class EncryptingInputStream extends InputStream {
	private InputStream in;
	boolean verbose = false;

	public EncryptingInputStream(InputStream in) {
		this.in = in;
	}
	public EncryptingInputStream(InputStream in, boolean verbose) {
		this.in = in;
		this.verbose = verbose;
	}

	@Override
	public int read() throws IOException {
		byte input = (byte)in.read();
		int ret = 1 - input;
		if(verbose) System.out.printf("{%s => %s} ",input,ret);
		return ret;
	}
}
