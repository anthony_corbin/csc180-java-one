package ClassWork.XML_JSON;

import java.io.*;

/**
 * Created by Anthony on 2/24/2015.
 */
public class Cereal {

	public static void main(String[]args) throws IOException, ClassNotFoundException {
		Crocodile c = new Crocodile();
		c.setId(2l);
		c.setLengthInInches(120);
		c.setWidthInInches(34);
		c.setName("Charlie");
		c.setArea(120 * 34);

		String filename = "testFile.bin";

		ObjectOutputStream oos = new ObjectOutputStream((System.out));
		oos.writeObject(c);
		System.out.println();
		oos = new ObjectOutputStream(new EncryptingOutputStream(new FileOutputStream(filename)));
		oos.writeObject(c);
		System.out.println();

		ObjectInputStream ois = new ObjectInputStream(new EncryptingInputStream(new FileInputStream(filename)));
		Crocodile in = (Crocodile)ois.readObject();
		if(in.getId().equals(c.getId())) {
			System.out.println("yeaa");
		}
	}
}
