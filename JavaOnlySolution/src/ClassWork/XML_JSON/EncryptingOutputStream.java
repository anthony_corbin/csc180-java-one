package ClassWork.XML_JSON;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Anthony on 2/25/2015.
 */
public class EncryptingOutputStream extends OutputStream {
	private OutputStream os;
	boolean verbose = false;

	public EncryptingOutputStream(OutputStream os) {
		this.os = os;
	}
	public EncryptingOutputStream(OutputStream os, boolean verbose) {
		this.os = os;
		this.verbose = verbose;
	}

	@Override
	public void write(int b) throws IOException {
		byte input = (byte)b;
		int ret = -(input - 1);
		if(verbose) System.out.printf("{%s => %s} ",input,ret);
		os.write(ret);
	}
}
