package edu.neumont.acorbin.nubay.services;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemByIDCommunication;
import edu.neumont.acorbin.nubay.services.Communicatons.GetItemCommunitaion;
import edu.neumont.acorbin.nubay.services.Communicatons.QueryCollection;
import edu.neumont.acorbin.nubay.services.Communicatons.SingleItemCommunication;
import edu.neumont.acorbin.nubay.services.Responces.CollectionOfItemsResponse;
import edu.neumont.acorbin.nubay.services.Responces.SingleItemResponse;
import edu.neumont.acorbin.nubay.utils.MyStringUtils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.*;

/**
 * Created by Anthony on 2/27/2015.
 */
public class LocalItemService implements ItemService {

	Socket serverConnection = null;

	String serverAddress = "localhost";
	int serverPort = 8080;

	public LocalItemService() { }

	private void ensureConnection() {
		//if(serverConnection == null || serverConnection.isClosed() || !serverConnection.isConnected()) {
			try {
				serverConnection = new Socket(serverAddress,serverPort);
			} catch (IOException e) {
				throw new ItemServiceException(e);
			}
		//}
	}

	private <T extends ServerResponse> T getServerResponse(ServerCommunication communication) {
		ensureConnection();
		ServerResponse ret_tmp;
		try {
			new ObjectOutputStream(serverConnection.getOutputStream()).writeObject(communication);
			ret_tmp = (ServerResponse)(new ObjectInputStream(serverConnection.getInputStream()).readObject());
		} catch (Exception e) {
			throw new ItemServiceException(e);
		}
		tryThrowServerErrors(ret_tmp);
		try {
			return (T) ret_tmp;
		} catch (Exception e) {
			throw new ItemServiceException(e);
		}
	}

	private void tryThrowServerErrors(ServerResponse response) {
		if(response == null) throw new ItemServiceException ("Sever Err");
		switch(response.getResponseCode()) {
			case ServerResponse.CLIENT_DATA_INVALID:      throw new ItemClientException  (response.getAdditionalInfo());
			case ServerResponse.MODEL_DOES_NOT_EXIST:     throw new ItemNotFoundException(response.getAdditionalInfo());
			case ServerResponse.SERVER_ENCOUNTERED_ERROR: throw new ItemServiceException (response.getAdditionalInfo());
		}
		if(response.getResponseCode() != ServerResponse.SUCCESS) throw new ItemServiceException (response.getAdditionalInfo());
	}

	@Override
	public Collection<ItemDetailModel> GetAll() {
		CollectionOfItemsResponse ret = getServerResponse(new QueryCollection("all"));
		return ret.getItems();
	}

	@Override
	public Set<ItemDetailModel> search(String query) {
		CollectionOfItemsResponse ret = getServerResponse(new QueryCollection(query));
		return ret.getItems();
	}

	@Override
	public ItemDetailModel bid(Long id, BigDecimal newAmount) {
		ItemDetailModel item = this.getItemByID(id);
		if(!item.valid()) {
			throw new ItemServiceException("Item Expired, unable to complete bid");
		}
		item.setCurrentBid(newAmount);
		return update(item);
	}

	@Override
	public ItemDetailModel getItemByID(long id) {
		SingleItemResponse ret = getServerResponse(new GetItemByIDCommunication(id));
		return ret.getModelData();
	}

	private ItemDetailModel getRandom(int id) {
		SingleItemResponse ret = getServerResponse(new GetItemCommunitaion(id));
		return ret.getModelData();
	}

	@Override
	public ItemDetailModel getRandomValidModel() {
		return getRandom(GetItemCommunitaion.GET_RANDOM_ITEM_VALID);
	}

	@Override
	public ItemDetailModel getRandomInValidModel() {
		return getRandom(GetItemCommunitaion.GET_RANDOM_ITEM_INVALID);
	}

	@Override
	public ItemDetailModel getRandomModel() {
		return getRandom(GetItemCommunitaion.GET_RANDOM_ITEM);
	}

	private ItemDetailModel singleModelCommand(ItemDetailModel modelData, int commandID) {
		SingleItemResponse ret = getServerResponse(new SingleItemCommunication(commandID,modelData));
		return ret.getModelData();
	}

	@Override
	public ItemDetailModel update(ItemDetailModel model) {
		return singleModelCommand(model, SingleItemCommunication.EDIT_ITEM);
	}

	@Override
	public boolean delete(long id) {
		singleModelCommand(new ItemDetailModel(id), SingleItemCommunication.DELETE_ITEM);
		return true;
	}

	@Override
	public Long AddNewObject(ItemDetailModel item) {
		ItemDetailModel ret = singleModelCommand(item, SingleItemCommunication.CREATE_ITEM);
		if(ret != null) {
			return ret.getId();
		}
		throw new ItemServiceException("Server Err");
	}

	@Override
	public Long AddNewObject(ItemDetailStringBuilder item) {
		if(item.getImageUrl() == null || item.getImageUrl().isEmpty()) {
			item.setImageUrl(MyStringUtils.javaStringLiteral("http://content.mycutegraphics.com/graphics/clothing/green-sock.png"));
		}
		if(!item.isValid()) {
			throw new ItemClientException("Unable to add new model, it still invalid");
		}
		return AddNewObject(item.getModel(-1));
	}
}
