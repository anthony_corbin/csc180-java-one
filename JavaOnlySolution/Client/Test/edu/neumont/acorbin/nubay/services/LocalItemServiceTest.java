package edu.neumont.acorbin.nubay.services;

import edu.neumont.acorbin.nubay.models.ItemDetailModel;
import edu.neumont.acorbin.nubay.models.ItemDetailStringBuilder;
import edu.neumont.acorbin.nubay.utils.DateParser;
import edu.neumont.acorbin.nubay.utils.MoneyParser;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collection;

public class LocalItemServiceTest {
	@Test
	public void testSearch() throws Exception {
		ItemService service = new LocalItemService();

		Assert.assertEquals(1, service.search("very and (brave or phone) and not starchy").size());
		Assert.assertEquals(2, service.search("very and (brave or phone)").size());
		Assert.assertEquals(5, service.search("very and brave or phone").size());
		Assert.assertEquals(2, service.search("very and brave").size());
		Assert.assertEquals(0, service.search("very and brave and").size()); // invalid
	}

	@Test
	public void testDelete() throws Exception {
		//ItemService service = new LocalItemService();
		//for (int i = service.GetAll().size(); i > 0; i--) {
		//	service.getRandomModel().toString(); // coverage hack
		//	service.delete(service.getRandomModel().getId());
		//	Assert.assertEquals(i-1,service.GetAll().size());
		//}
		//Assert.assertEquals(null,service.getRandomModel());
	}

	@Test
	public void testBid() throws Exception {
		ItemService service = new LocalItemService();

		ItemDetailModel m = service.getRandomValidModel();
		BigDecimal beforeBid = m.getCurrentBid();
		service.incrementBid(m.getId(), 10);
		BigDecimal after = service.getItemByID(m.getId()).getCurrentBid();
		Assert.assertEquals(10,after.subtract(beforeBid).doubleValue(),.001);
	}

	@Test
	public void testExpiredBit() throws Exception {
		ItemService service = new LocalItemService();

		ItemDetailModel m = service.getRandomInValidModel();
		try {
			service.incrementBid(m.getId(), 10);
			Assert.fail();
		} catch (Exception ignored) { }
	}

	@Test
	public void testGetItemByID() throws Exception {
		ItemService service = new LocalItemService();
		Collection<ItemDetailModel> all = service.GetAll();
		for(ItemDetailModel m : all) {
			Assert.assertEquals(m,service.getItemByID(m.getId()));
		}
	}

	@Test
	public void testGetRandomModel() throws Exception {
		ItemService service = new LocalItemService();
		for (int i = 0; i < 10; i++) {
			Assert.assertTrue(service.GetAll().contains(service.getRandomValidModel()));
		}
	}

	@Test
	public void testCreateModelErr() throws Exception {
		ItemService service = new LocalItemService();
		ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
		stringy.setStartDate("Hello");
		Assert.assertTrue(stringy.getErrs().size() > 0);
		Assert.assertFalse(stringy.isValid());
		Assert.assertEquals(stringy.getModel(100),null);
		try {
			service.AddNewObject(stringy);
			Assert.fail();
		} catch (ItemService.ItemClientException ignored) { }
	}

	@Test
	public void testCreateModel() throws Exception {
		ItemService service = new LocalItemService();
		ItemDetailStringBuilder stringy = new ItemDetailStringBuilder();
		stringy.setStartDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, -1, 0, 0, 0, 0)));
		stringy.setImageUrl("");
		stringy.setName("Valid");
		stringy.setShortDescription("Good");
		stringy.setLongDescription("Goooooooooooooooooooooooooooooood");
		stringy.setCurrentBid("not good");
		Assert.assertFalse(stringy.isValid());
		Assert.assertTrue(stringy.getErrs().size() > 0);
		stringy.setCurrentBid(MoneyParser.format(100));
		stringy.setEndDate(DateParser.format(DateParser.getNowPlusDuration(0, 0, 0, 100, 0, 0, 0)));
		Assert.assertTrue(stringy.isValid());
		service.AddNewObject(stringy);
	}
}